package empire.backend.database;

import empire.backend.helper.CustomLogger;
import empire.backend.model.internal.RoleWithId;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class RoleQueries {
    @Inject
    DbSetup db;
    @Inject
    private CustomLogger logger;

    public RoleWithId userHasRoleForTable(String table, List<RoleWithId> roles) {
        if (roles == null)
            return null;
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT `neededRole` FROM `RolesForTable` WHERE `tableName` = ?");
            statement.setString(1, table);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int neededRole = resultSet.getInt("neededRole");
                return roles.stream().filter(roleWithId -> neededRole == roleWithId.getId()).findAny().orElse(new RoleWithId());
            }
            return new RoleWithId();
        } catch (Exception e) {
            logger.log("Error on querying role for user and table", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public List<RoleWithId> getRolesForUser(String imperialId) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT `roleId`, `read`, `write`, `create`, `delete` " +
                    "FROM `UserWithRole` r JOIN `User` u ON r.`userId` = u.`id` WHERE `imperialId` = ?");
            statement.setString(1, imperialId);
            resultSet = statement.executeQuery();
            List<RoleWithId> roles = new LinkedList<>();
            while (resultSet.next()) {
                roles.add(new RoleWithId(resultSet.getInt("roleId"), resultSet.getBoolean("read"),
                        resultSet.getBoolean("write"), resultSet.getBoolean("create"), resultSet.getBoolean("delete")));
            }
            return roles;
        } catch (Exception e) {
            logger.log("Error on querying roles for user", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }
}

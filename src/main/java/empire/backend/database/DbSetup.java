/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.database;

import empire.backend.helper.CustomLogger;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbSetup {
    @Inject
    ConnectionHelper helper;
    @Inject
    private CustomLogger logger;

    Connection getConnection() {
        try {
            return helper.loadConnection();
        } catch (ClassNotFoundException classNotFound) {
            logger.log("JDBC Driver class not found", classNotFound);
            return null;
        } catch (SQLException sql) {
            logger.log("Error on getting database connection", sql);
            return null;
        }
    }

    void close(ResultSet resultSet, PreparedStatement statement, Connection connect) {
        try {
            if (resultSet != null)
                resultSet.close();
            if (statement != null)
                statement.close();
            if (connect != null)
                connect.close();
        } catch (SQLException sql) {
            logger.log("Error on closing result set", sql);
        }
    }
}

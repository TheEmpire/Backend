/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.database;

import empire.backend.helper.CustomLogger;
import empire.backend.model.schemas.info.InfoType;
import empire.backend.model.schemas.info.InfoTypeArray;
import empire.backend.model.schemas.info.InfoTypeCreate;

import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.LinkedList;
import java.util.List;

public class InfoTypeQueries {
    @Inject
    DbSetup db;
    @Inject
    private CustomLogger logger;

    public InfoTypeArray getTypes() {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT id, name, neededRole FROM InfoType");
            resultSet = statement.executeQuery();
            List<InfoType> typeList = new LinkedList<>();
            while (resultSet.next()) {
                typeList.add(new InfoType(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("neededRole")));
            }
            return new InfoTypeArray(typeList.toArray(new InfoType[0]));
        } catch (Exception e) {
                logger.log("Error on querying information types", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public Boolean insertType(InfoTypeCreate type) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("INSERT INTO `InfoType` (`name`, `neededRole`) VALUES (?, ?)");
            statement.setString(1, type.getType());
            statement.setInt(2, type.getRole());
            statement.executeUpdate();
            return true;
        } catch (SQLIntegrityConstraintViolationException e) {
            return false;
        } catch (Exception e) {
                logger.log("Error on adding information type", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean updateType(InfoTypeCreate type, int id) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("UPDATE `InfoType` SET `name` = ?, `neededRole` = ? WHERE `id` = ?");
            statement.setString(1, type.getType());
            statement.setInt(2, type.getRole());
            statement.setInt(3, id);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on updating information type", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean deleteType(int id) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("DELETE FROM `InfoType` WHERE `id` = ?");
            statement.setInt(1, id);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on deleting information type", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.database;

import empire.backend.helper.CustomLogger;
import empire.backend.model.enums.UserState;
import empire.backend.model.internal.UserAuthentication;
import empire.backend.model.internal.UserShare;
import empire.backend.model.internal.UserWithId;
import empire.backend.model.schemas.user.Registration;
import empire.backend.model.schemas.user.UserUpdate;

import javax.inject.Inject;
import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserQueries {
    @Inject
    DbSetup db;
    @Inject
    private CustomLogger logger;

    public UserWithId getUser(String imperialId) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT id, name, mail, timezone, dateOfBirth, dateOfAccess, state, language FROM User u " +
                    "LEFT JOIN Language l on u.id = l.userId WHERE imperialId = ?");
            statement.setString(1, imperialId);

            resultSet = statement.executeQuery();
            if (!resultSet.isBeforeFirst())
                return new UserWithId();
            int id = -1;
            String name = null;
            String mail = null;
            String timezone = null;
            LocalDate dateOfBirth = null;
            LocalDate dateOfAccess = null;
            UserState state = null;
            List<String> language = new ArrayList<>();
            while (resultSet.next()) {
                // can be overriden on each iteration as the sql query returns same values (only language can have multiple values)
                id = resultSet.getInt("id");
                name = resultSet.getString("name");
                mail = resultSet.getString("mail");
                timezone = resultSet.getString("timezone");
                String newLang = resultSet.getString("language");
                if (newLang != null)
                    language.add(newLang);
                dateOfAccess = resultSet.getObject("dateOfAccess", LocalDate.class);
                dateOfBirth = resultSet.getObject("dateOfBirth", LocalDate.class);
                state = UserState.valueOf(resultSet.getString("state"));
            }
            return new UserWithId(id, imperialId, name, mail, timezone, language.toArray(new String[0]), dateOfAccess, dateOfBirth, state);
        } catch (Exception e) {
                logger.log("Error on querying user", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public UserShare getUserShare(String imperialId) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT share.name, share.mail, share.dateOfBirth FROM UserShare share JOIN User user on share.id = user.id WHERE user.imperialId = ?");
            statement.setString(1, imperialId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return new UserShare(resultSet.getBoolean("share.mail"), resultSet.getBoolean("share.dateOfBirth"), resultSet.getBoolean("share.name"));
            }
            return new UserShare(false, false, false);
        } catch (Exception e) {
                logger.log("Error on querying user share properties", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public Boolean isVerified(String imperialId) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT auth.verified FROM UserAuth auth JOIN User user on auth.id = user.id WHERE user.imperialId = ?");
            statement.setString(1, imperialId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getBoolean("verified");
            }
            return null;
        } catch (Exception e) {
                logger.log("Error on querying user verified state", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public Boolean setVerified(int userId, boolean value) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("UPDATE UserAuth SET verified = ? WHERE id = ?");
            statement.setBoolean(1, value);
            statement.setInt(2, userId);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on setting user verified state", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean userExists(String user) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT id FROM User WHERE imperialId = ?");
            statement.setString(1, user);
            resultSet = statement.executeQuery();
            return resultSet.isBeforeFirst();
        } catch (Exception e) {
                logger.log("Error on querying whether the user exists", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public UserAuthentication getUserCredentials(String imperialId) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT auth.password, auth.salt, user.mail, auth.verified " +
                    "FROM User user JOIN UserAuth auth ON auth.id = user.id WHERE user.imperialId=?");
            statement.setString(1, imperialId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return new UserAuthentication(resultSet.getString("password"), resultSet.getString("mail"), resultSet.getString("salt"), resultSet.getBoolean("verified"));
            }
            return new UserAuthentication();
        } catch (Exception e) {
                logger.log("Error on querying user credentials", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public String getIdForMail(String mail) {
        Connection connect = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("SELECT imperialId FROM User WHERE mail = ?");
            statement.setString(1, mail);
            resultSet = statement.executeQuery();
            String id = null;
            if (resultSet.next()) {
                id = resultSet.getString("imperialId");
            }
            return id;
        } catch (Exception e) {
                logger.log("Error on querying user id with mail", e);
            return null;
        } finally {
            db.close(resultSet, statement, connect);
        }
    }

    public Boolean createUser(Registration data, String hashedPassword, String salt) {
        Connection connect = null;
        try {
            connect = db.getConnection();
            connect.setAutoCommit(false);
            createUserTableUser(connect, data);

            if (data.getLanguages() != null) {
                for (String lang : data.getLanguages()) {
                    createUserAddLanguage(connect, lang);
                }
            }

            createUserTableAuth(connect, hashedPassword, salt);

            try (PreparedStatement statement = connect.prepareStatement("INSERT INTO UserShare (id, name, mail, dateOfBirth) VALUES (last_insert_id(), false, false, false)")) {
                statement.execute();
            }

            connect.commit();
            return true;
        } catch (Exception e) {
            try {
                if (connect != null) {
                    connect.rollback();
                    if (e.getMessage() != null && e.getMessage().contains("Duplicate entry")) {
                        return false;
                    }
                        logger.log("Error on creating user", e);
                }
            } catch (SQLException e2) {
                    logger.log("Error on creating user rollback", e2);
            }
            return null;
        } finally {
            db.close(null, null, connect);
        }
    }

    public Boolean updatePassword(int id, String password) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("UPDATE UserAuth SET password = ? WHERE id = ?");
            statement.setString(1, password);
            statement.setInt(2, id);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on setting password", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean deleteUser(String user) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("DELETE FROM User WHERE imperialId = ?");
            statement.setString(1, user);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on deleting user", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean updateUser(UserUpdate user) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            Map<String, Object> map = user.getSetAttributes();
            if (map.size() == 0)
                return false;
            if (map.containsKey("mail")) {
                try (PreparedStatement statementAuth = connect.prepareStatement("UPDATE UserAuth SET verified = false WHERE id = (SELECT id FROM User WHERE imperialId = ?)")) {
                    statementAuth.setString(1, user.getImperialId());
                    if (statementAuth.executeUpdate() != 1) {
                        return false;
                    }
                }
            }
            StringBuilder attributes = new StringBuilder();
            map.keySet().forEach(key -> attributes.append(key).append("=?, "));
            attributes.deleteCharAt(attributes.length() - 2);
            statement = connect.prepareStatement("UPDATE User SET " + attributes.toString() + " WHERE imperialId = ?");
            int i = 1;
            for (Object o : map.values()) {
                if (o instanceof UserState)
                    statement.setString(i, ((UserState) o).name());
                else if (o instanceof LocalDate)
                    statement.setObject(i, java.util.Date.from(((LocalDate) o).atStartOfDay().toInstant(ZoneOffset.UTC)));
                else
                    statement.setObject(i, o);
                i++;
            }
            statement.setString(map.size() + 1, user.getImperialId());
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on updating user", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public int createLanguage(String userId, String language) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("INSERT INTO Language (userId, language) VALUES ((SELECT id FROM User WHERE imperialId = ?), ?) ");
            statement.setString(1, userId);
            statement.setString(2, language);
            statement.execute();
            return 0;
        } catch (Exception e) {
            if (e.getMessage() != null) {
                if (e.getMessage().contains("Column 'userId' cannot be null")) {
                    return 1;
                } else if (e.getMessage().contains("Duplicate entry")) {
                    return 2;
                }
            }
                logger.log("Error on adding language to user", e);
            return -1;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean updateLanguage(String userId, String language, String newValue) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("UPDATE Language SET language = ? WHERE userId = (SELECT id FROM User WHERE imperialId = ?)  AND language = ?");

            statement.setString(1, newValue);
            statement.setString(2, userId);
            statement.setString(3, language);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on updating language for user", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    public Boolean deleteLanguage(String userId, String language) {
        Connection connect = null;
        PreparedStatement statement = null;
        try {
            connect = db.getConnection();
            statement = connect.prepareStatement("DELETE FROM Language WHERE userId = (SELECT id FROM User WHERE imperialId = ?)  AND language = ?");
            statement.setString(1, userId);
            statement.setString(2, language);
            return statement.executeUpdate() == 1;
        } catch (Exception e) {
                logger.log("Error on deleting language for user", e);
            return null;
        } finally {
            db.close(null, statement, connect);
        }
    }

    private void createUserTableAuth(Connection connect, String hashedPassword, String salt) throws SQLException {
        try (PreparedStatement statement = connect.prepareStatement("INSERT INTO UserAuth (id, password, salt) VALUES (last_insert_id(), ?, ?) ")) {
            statement.setString(1, hashedPassword);
            statement.setString(2, salt);
            statement.execute();
        }
    }

    private void createUserAddLanguage(Connection connect, String language) throws SQLException {
        try (PreparedStatement statement = connect.prepareStatement("INSERT INTO Language (userId, language) VALUES (last_insert_id(), ?)")) {
            statement.setString(1, language);
            statement.execute();
        }
    }

    private void createUserTableUser(Connection connect, Registration data) throws SQLException {
        PreparedStatement statementUser = null;
        try {
            if (data.getName() != null) {
                statementUser = connect.prepareStatement("INSERT INTO User (imperialId, mail, timezone, dateOfBirth, name, rankId) VALUES (?, ?, ?, ?, ?, 0) ");
                statementUser.setString(5, data.getName());
            } else
                statementUser = connect.prepareStatement("INSERT INTO User (imperialId, mail, timezone, dateOfBirth, rankId) VALUES (?, ?, ?, ?, 0) ");
            statementUser.setString(1, data.getImperialId());
            statementUser.setString(2, data.getMail());
            statementUser.setString(3, data.getTimezone());
            if (data.getDateOfBirth() != null)
                statementUser.setObject(4, java.util.Date.from(data.getDateOfBirth().atStartOfDay().toInstant(ZoneOffset.UTC)));
            else
                statementUser.setObject(4, null);
            statementUser.execute();
        } finally {
            if (statementUser != null)
                statementUser.close();
        }
    }
}

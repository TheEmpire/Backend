/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.application;

import empire.backend.database.RoleQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.internal.RoleFilterId;
import empire.backend.model.internal.RoleWithId;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.*;
import java.lang.reflect.Method;
import java.util.stream.Collectors;

@Provider
@WithRole(table = "notSet")
@Priority(Priorities.AUTHORIZATION)
public class RoleFilter implements ContainerRequestFilter {
    @Context
    ResourceInfo resourceInfo;
    @Context
    SecurityContext securityContext;
    @Inject
    ResponseBuilder builder;
    @Inject
    RoleQueries roleQueries;
    @Inject
    CustomGson gson;
    @Inject
    JWTHelper jwtHelper;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Method resourceMethod = resourceInfo.getResourceMethod();
        WithRole annotation = resourceMethod.getAnnotation(WithRole.class);
        String method = requestContext.getMethod();

        if (annotation != null) {
            String name = jwtHelper.getName(securityContext);
            if (annotation.self() && isSameUser(requestContext, name))
                return;
            RoleWithId roleOfUser = roleQueries.userHasRoleForTable(annotation.table(),
                    roleQueries.getRolesForUser(name));
            if (roleOfUser == null) {
                requestContext.abortWith(builder.create(ServerMessage.DATABASE_ERROR));
            } else {
                if (roleOfUser.exists()) {
                    switch (method) {
                        case "PUT": {
                            checkForAbort(roleOfUser.getWrite(), requestContext);
                            break;
                        }
                        case "POST": {
                            checkForAbort(roleOfUser.getCreate(), requestContext);
                            break;
                        }
                        case "DELETE": {
                            checkForAbort(roleOfUser.getDelete(), requestContext);
                            break;
                        }
                        default: { //GET
                            checkForAbort(roleOfUser.getRead(), requestContext);
                            break;
                        }
                    }
                } else {
                    requestContext.abortWith(builder.create(ServerMessage.ROLE_MISSING));
                }
            }
        }
    }

    private boolean isSameUser(ContainerRequestContext requestContext, String name) throws IOException {
        String imperialId = requestContext.getUriInfo().getPathParameters().getFirst("imperialId");
        if (imperialId == null) {
            imperialId = checkBody(requestContext);
        }
        return !imperialId.isEmpty() && imperialId.equals(name);
    }

    private String checkBody(ContainerRequestContext requestContext) throws IOException {
        try (InputStream stream = requestContext.getEntityStream()) {
            if (stream != null) {
                String json = new BufferedReader(new InputStreamReader(stream)).lines().collect(Collectors.joining("\n"));
                RoleFilterId id = gson.fromJson(json, RoleFilterId.class);
                requestContext.setEntityStream(new ByteArrayInputStream(json.getBytes()));
                return id != null ? id.getImperialId() : "";
            }
        }
        return "";
    }

    private void checkForAbort(Boolean shouldNotAbort, ContainerRequestContext requestContext) {
        if (Boolean.FALSE.equals(shouldNotAbort))
            requestContext.abortWith(builder.create(ServerMessage.ROLE_MISSING));
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.enums;

public enum ServerMessage {
    RESET_MAIL_SENT, VALIDATION_MAIL_SENT, PASSWORD_UPDATED, LOGIN_SUCCESS, RENEW_SUCCESS, USER_UPDATED, USER_DELETED,
    LANGUAGE_ADDED, LANGUAGE_UPDATED, LANGUAGE_REMOVED, MAIL_VALIDATED, ALL_INFO_UPDATED, ALL_INFO_DELETED, INFO_ADDED,
    INFO_UPDATED, INFO_DELETED, TYPE_UPDATED, TYPE_DELETED, ONLINE_UPDATED, RANK_UPDATED, RANK_DELETED, RANK_ASSIGNED,
    RANK_RESET, ROLE_UPDATED, ROLE_DELETED, ROLE_ASSIGNED, ROLE_REMOVED, ALL_ROLES_REMOVED, REGISTERED, NOT_REGISTERED,
    MANAGEMENT_ROLE_UPDATED, SQUAD_UPDATED, SQUAD_DELETED, SQUAD_ASSIGNED, SQUAD_REMOVED, SQUAD_POSITION_UPDATED,
    CORPORATION_UPDATED, CORPORATION_DELETED, LEGATE_UPDATED, LEGATE_DELETED,

    USER_CREATED, TYPE_CREATED, RANK_CREATED, ROLE_CREATED, SQUAD_CREATED, CORPORATION_CREATED, LEGATE_CREATED,

    QUERY_PARAM_WRONG, PATH_PARAM_WRONG, REQUEST_BODY_WRONG, INVALID_ENTITY, INVALID_TABLE_ALIAS,

    ACCESS_DENIED, JWT_MISSING, JWT_INVALID,

    ROLE_MISSING, AUTH_INCORRECT,

    IMAGE_NOT_FOUND, USER_NOT_FOUND, TYPE_NOT_FOUND, RANK_NOT_FOUND, ROLE_NOT_FOUND, SQUAD_NOT_FOUND,
    CORPORATION_NOT_FOUND, LEGATE_NOT_FOUND, USER_OR_LANGUAGE_NOT_FOUND,

    INFO_TYPE_ALREADY_EXISTS, USER_ALREADY_EXISTS, RANK_ALREADY_EXISTS, ROLE_ALREADY_EXISTS, SQUAD_ALREADY_EXISTS,
    CORPORATION_ALREADY_EXISTS, USER_ALREADY_LEGATE, LANGUAGE_ALREADY_EXISTS,

    DATABASE_ERROR, MAIL_SEND_ERROR, COLOR_CONVERSION_ERROR, JWT_ERROR;

    public int getCode() {
        switch (this) {
            case RESET_MAIL_SENT:
            case VALIDATION_MAIL_SENT:
            case PASSWORD_UPDATED:
            case LOGIN_SUCCESS:
            case RENEW_SUCCESS:
            case USER_UPDATED:
            case USER_DELETED:
            case LANGUAGE_ADDED:
            case LANGUAGE_UPDATED:
            case LANGUAGE_REMOVED:
            case MAIL_VALIDATED:
            case ALL_INFO_UPDATED:
            case ALL_INFO_DELETED:
            case INFO_ADDED:
            case INFO_UPDATED:
            case INFO_DELETED:
            case TYPE_UPDATED:
            case TYPE_DELETED:
            case ONLINE_UPDATED:
            case RANK_UPDATED:
            case RANK_DELETED:
            case RANK_ASSIGNED:
            case RANK_RESET:
            case ROLE_UPDATED:
            case ROLE_DELETED:
            case ROLE_ASSIGNED:
            case ROLE_REMOVED:
            case ALL_ROLES_REMOVED:
            case REGISTERED:
            case NOT_REGISTERED:
            case MANAGEMENT_ROLE_UPDATED:
            case SQUAD_UPDATED:
            case SQUAD_DELETED:
            case SQUAD_ASSIGNED:
            case SQUAD_REMOVED:
            case SQUAD_POSITION_UPDATED:
            case CORPORATION_UPDATED:
            case CORPORATION_DELETED:
            case LEGATE_UPDATED:
            case LEGATE_DELETED:
                return 200;
            case USER_CREATED:
            case TYPE_CREATED:
            case RANK_CREATED:
            case ROLE_CREATED:
            case SQUAD_CREATED:
            case CORPORATION_CREATED:
            case LEGATE_CREATED:
                return 201;
            case QUERY_PARAM_WRONG:
            case PATH_PARAM_WRONG:
            case REQUEST_BODY_WRONG:
            case INVALID_ENTITY:
            case INVALID_TABLE_ALIAS:
                return 400;
            case ACCESS_DENIED:
            case JWT_MISSING:
            case JWT_INVALID:
                return 401;
            case ROLE_MISSING:
            case AUTH_INCORRECT:
                return 403;
            case IMAGE_NOT_FOUND:
            case USER_NOT_FOUND:
            case TYPE_NOT_FOUND:
            case RANK_NOT_FOUND:
            case ROLE_NOT_FOUND:
            case SQUAD_NOT_FOUND:
            case CORPORATION_NOT_FOUND:
            case LEGATE_NOT_FOUND:
            case USER_OR_LANGUAGE_NOT_FOUND:
                return 404;
            case INFO_TYPE_ALREADY_EXISTS:
            case USER_ALREADY_EXISTS:
            case RANK_ALREADY_EXISTS:
            case ROLE_ALREADY_EXISTS:
            case SQUAD_ALREADY_EXISTS:
            case CORPORATION_ALREADY_EXISTS:
            case USER_ALREADY_LEGATE:
            case LANGUAGE_ALREADY_EXISTS:
                return 409;
            default:
                return 500;
        }
    }
}

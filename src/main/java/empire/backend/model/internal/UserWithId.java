/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.internal;

import empire.backend.model.enums.UserState;
import empire.backend.model.schemas.user.User;

import java.time.LocalDate;

public class UserWithId extends User {
    private Boolean exists;
    private Integer id;

    public UserWithId(int id, String imperialId, String name, String mail, String timezone, String[] language, LocalDate dateOfAccess, LocalDate dateOfBirth, UserState state) {
        super(imperialId, name, mail, timezone, language, dateOfAccess, dateOfBirth, state);
        this.exists = true;
        this.id = id;
    }

    public UserWithId() {
        super(null, null, null, null, null, null, null, null);
        this.exists = false;
    }

    public Boolean exists() {
        return exists;
    }

    public UserWithId prepareForExport() {
        id = null;
        exists = null;
        return this;
    }

    public Integer getId() {
        return id;
    }
}

package empire.backend.model.internal;

import empire.backend.model.schemas.role.Role;

public class RoleWithId extends Role {
    private Integer id;
    private boolean exists;

    public RoleWithId(Integer id, Boolean read, Boolean write, Boolean create, Boolean delete) {
        super(read, write, create, delete);
        this.id = id;
        this.exists = true;
    }

    public RoleWithId() {
        super(null, null, null, null);
        this.exists = false;
    }

    public Integer getId() {
        return id;
    }

    public boolean exists() {
        return exists;
    }
}

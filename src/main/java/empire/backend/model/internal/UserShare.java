package empire.backend.model.internal;

public class UserShare {
    private boolean mail;
    private boolean dateOfBirth;
    private boolean name;

    public UserShare(boolean mail, boolean dateOfBirth, boolean name) {
        this.mail = mail;
        this.dateOfBirth = dateOfBirth;
        this.name = name;
    }

    public boolean isMail() {
        return mail;
    }

    public boolean isDateOfBirth() {
        return dateOfBirth;
    }

    public boolean isName() {
        return name;
    }
}

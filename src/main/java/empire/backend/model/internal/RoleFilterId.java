package empire.backend.model.internal;

public class RoleFilterId {
    private String imperialId;

    public RoleFilterId(String imperialId) {
        this.imperialId = imperialId;
    }

    public String getImperialId() {
        return imperialId;
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.internal;

public class UserAuthentication {
    private String password;
    private String mail;
    private String salt;
    private Boolean verified;
    private boolean empty;

    public UserAuthentication(String password, String mail, String salt, Boolean verified) {
        this.password = password;
        this.mail = mail;
        this.verified = verified;
        this.salt = salt;
        this.empty = false;
    }

    public UserAuthentication() {
        this.empty = true;
    }

    public String getPassword() {
        return password;
    }

    public String getMail() {
        return mail;
    }

    public String getSalt() {
        return salt;
    }

    public Boolean isVerified() {
        return verified;
    }

    public boolean isEmpty() {
        return empty;
    }
}

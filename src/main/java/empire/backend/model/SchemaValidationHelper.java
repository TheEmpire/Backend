/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model;

public class SchemaValidationHelper {
    //Prevent instantiation
    private SchemaValidationHelper() {}

    public static boolean isValidArray(SchemaValidation[] array) {
        boolean result = array != null && array.length > 0;
        int count = 0;
        while(result && count < array.length) {
            if (array[count] != null) {
                result = array[count].isValid();
                count++;
            } else
                result = false;
        }
        return result;
    }

    public static boolean isValidArray(String[] array) {
        boolean result = array != null && array.length > 0;
        int count = 0;
        while(result && count < array.length) {
            if (array[count] != null) {
                result = !array[count].isEmpty();
                count++;
            } else
                result = false;
        }
        return result;
    }

    public static boolean isValidOptionalArray(String[] array) {
        if (array == null)
            return true;
        else
            return isValidArray(array);
    }

    public static boolean isValidRequired(String val) {
        return val != null && !val.isEmpty();
    }

    public static boolean isValidRequired(Integer val) {
        return val != null && val >= 0;
    }

    public static boolean isValidRequired(Boolean val) {
        return val != null;
    }

    public static boolean isValidOptional(String val) {
        return val == null || !val.isEmpty();
    }

    public static boolean isValidOptional(Integer val) {
        return val == null || val >= 0;
    }
}

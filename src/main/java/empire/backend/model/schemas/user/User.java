/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import empire.backend.model.SchemaValidation;
import empire.backend.model.enums.UserState;
import empire.backend.model.internal.UserShare;

import java.time.LocalDate;

import static empire.backend.model.SchemaValidationHelper.*;

public class User implements SchemaValidation {
    private String imperialId;
    private String name;
    private String mail;
    private String timezone;
    private String[] language;
    private LocalDate dateOfAccess;
    private LocalDate dateOfBirth;
    private UserState state;

    public User(String imperialId, String name, String mail, String timezone, String[] language, LocalDate dateOfAccess, LocalDate dateOfBirth, UserState state) {
        this.imperialId = imperialId;
        this.name = name;
        this.mail = mail;
        this.timezone = timezone;
        this.language = language;
        this.dateOfAccess = dateOfAccess;
        this.dateOfBirth = dateOfBirth;
        this.state = state;
    }

    public User resetNotShared(UserShare share) {
        if (!share.isName()) {
            name = null;
        }
        if (!share.isDateOfBirth()) {
            dateOfBirth = null;
        }
        if (!share.isMail()) {
            mail = null;
        }
        return this;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(imperialId) && isValidRequired(timezone) && dateOfAccess != null && state != null &&
                isValidOptional(name) && (mail == null || mail.contains("@")) && isValidOptionalArray(language);
    }

    public String getImperialId() {
        return imperialId;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getTimezone() {
        return timezone;
    }

    public String[] getLanguage() {
        return language;
    }

    public LocalDate getDateOfAccess() {
        return dateOfAccess;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public UserState getState() {
        return state;
    }
}

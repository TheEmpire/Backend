/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import empire.backend.model.SchemaValidation;

import java.time.LocalDate;

import static empire.backend.model.SchemaValidationHelper.*;

public class Registration implements SchemaValidation {
    private String imperialId;
    private String password;
    private String mail;
    private String name;
    private String timezone;
    private String[] languages;
    private LocalDate dateOfBirth;

    public Registration(String imperialId, String password, String mail, String name, String timezone, String[] languages, LocalDate dateOfBirth) {
        this.imperialId = imperialId;
        this.password = password;
        this.mail = mail;
        this.name = name;
        this.timezone = timezone;
        this.languages = languages;
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(imperialId) && isValidRequired(password) && isValidRequired(mail) && mail.contains("@") && !mail.contains("\n") &&
                isValidRequired(timezone) && dateOfBirth != null && isValidOptional(name) && isValidOptionalArray(languages);
    }

    public String getImperialId() {
        return imperialId;
    }

    public String getPassword() {
        return password;
    }

    public String getMail() {
        return mail;
    }

    public String getName() {
        return name;
    }

    public String getTimezone() {
        return timezone;
    }

    public String[] getLanguages() {
        return languages;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
}

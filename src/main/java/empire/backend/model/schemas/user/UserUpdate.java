/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import empire.backend.model.SchemaValidation;
import empire.backend.model.enums.UserState;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static empire.backend.model.SchemaValidationHelper.isValidOptional;
import static empire.backend.model.SchemaValidationHelper.isValidRequired;

public class UserUpdate implements SchemaValidation {
    private String imperialId;
    private String name;
    private String mail;
    private String timezone;
    private LocalDate dateOfBirth;
    private UserState state;

    public UserUpdate(String imperialId, String name, String mail, String timezone, LocalDate dateOfBirth, UserState state) {
        this.imperialId = imperialId;
        this.name = name;
        this.mail = mail;
        this.timezone = timezone;
        this.dateOfBirth = dateOfBirth;
        this.state = state;
    }

    @Override
    public boolean isValid() {
        if (isValidRequired(imperialId) && isValidOptional(name) && isValidOptional(mail) && isValidOptional(timezone))
                return isValidRequired(name) || (isValidRequired(mail) && mail.contains("@")) || isValidRequired(timezone) || dateOfBirth != null || state != null;
        return false;
    }

    public Map<String, Object> getSetAttributes() {
        Map<String, Object> map = new HashMap<>();
        if (name != null)
            map.put("name", name);
        if (mail != null)
            map.put("mail", mail);
        if (timezone != null)
            map.put("timezone", timezone);
        if (dateOfBirth != null)
            map.put("dateOfBirth", dateOfBirth);
        if (state != null)
            map.put("state", state);
        return map;
    }

    public String getImperialId() {
        return imperialId;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getTimezone() {
        return timezone;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public UserState getState() {
        return state;
    }
}

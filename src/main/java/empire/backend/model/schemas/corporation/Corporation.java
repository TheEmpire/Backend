/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import empire.backend.model.SchemaValidation;

import static empire.backend.model.SchemaValidationHelper.isValidArray;
import static empire.backend.model.SchemaValidationHelper.isValidRequired;

public class Corporation implements SchemaValidation {
    private Integer id;
    private String name;
    private String description;
    private String[] legate;

    public Corporation(Integer id, String name, String description, String[] legate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.legate = legate;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(id) && isValidRequired(name) && isValidRequired(description) && isValidArray(legate);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String[] getLegate() {
        return legate;
    }
}

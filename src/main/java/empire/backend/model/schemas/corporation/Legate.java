/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import empire.backend.model.SchemaValidation;

import static empire.backend.model.SchemaValidationHelper.isValidOptional;
import static empire.backend.model.SchemaValidationHelper.isValidRequired;

public class Legate implements SchemaValidation {
    private Integer id;
    private Integer userId;
    private String imperialId;

    public Legate(Integer id, Integer userId, String imperialId) {
        this.id = id;
        this.userId = userId;
        this.imperialId = imperialId;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(id) && isValidRequired(imperialId) && isValidOptional(userId);
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getImperialId() {
        return imperialId;
    }
}

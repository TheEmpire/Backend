/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.info;

import empire.backend.model.SchemaValidation;

import static empire.backend.model.SchemaValidationHelper.isValidRequired;

public class InfoType implements SchemaValidation {
    private Integer id;
    private String name;
    private Integer role;

    public InfoType(Integer id, String name, Integer role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(id) && isValidRequired(name) && isValidRequired(role);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getRole() {
        return role;
    }
}

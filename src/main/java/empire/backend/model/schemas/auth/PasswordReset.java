/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.auth;

import empire.backend.model.SchemaValidation;

import static empire.backend.model.SchemaValidationHelper.isValidRequired;

public class PasswordReset implements SchemaValidation {
    private String hashOne;
    private String hashTwo;
    private String imperialId;
    private String newPassword;

    public PasswordReset(String hashOne, String hashTwo, String imperialId, String newPassword) {
        this.hashOne = hashOne;
        this.hashTwo = hashTwo;
        this.imperialId = imperialId;
        this.newPassword = newPassword;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(hashOne) && isValidRequired(hashTwo) && isValidRequired(imperialId) && isValidRequired(newPassword);
    }

    public String getHashOne() {
        return hashOne;
    }

    public String getHashTwo() {
        return hashTwo;
    }

    public String getImperialId() {
        return imperialId;
    }

    public String getNewPassword() {
        return newPassword;
    }
}

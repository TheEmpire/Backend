/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.auth;

import empire.backend.model.SchemaValidation;

import static empire.backend.model.SchemaValidationHelper.isValidRequired;

public class Login implements SchemaValidation {
    private String imperialId;
    private String password;

    public Login(String imperialId, String password) {
        this.imperialId = imperialId;
        this.password = password;
    }

    @Override
    public boolean isValid() {
        return isValidRequired(imperialId) && isValidRequired(password);
    }

    public String getImperialId() {
        return imperialId;
    }

    public String getPassword() {
        return password;
    }
}

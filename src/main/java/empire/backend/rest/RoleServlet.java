/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Secured
@Path("role")
public class RoleServlet {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRoles() {
        return Response.status(501).build();
    }

    @POST
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRole(@PathParam("name") String roleName) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRole(@PathParam("id") int roleId, String json) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteRole(@PathParam("id") int roleId) {
        return Response.status(501).build();
    }

    @GET
    @Path("user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRolesOfUser(@PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeAllRolesFromUser(@PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}/user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignRoleToUser(@PathParam("id") int roleId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{id}/user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeRoleFromUser(@PathParam("id") int roleId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}/table/{table}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignTableRole(@PathParam("id") int roleId, @PathParam("table") String tableAlias) {
        return Response.status(501).build();
    }
}

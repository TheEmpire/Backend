/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Secured
@Path("info")
public class UserInfoServlet {
    @GET
    @Path("{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllInfo(@PathParam("imperialId") String user) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{imperialId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateMultipleInfo(@PathParam("imperialId") String user, String json) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAllInfo(@PathParam("imperialId") String user) {
        return Response.status(501).build();
    }

    @GET
    @Path("{imperialId}/type/{infoType}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInfo(@PathParam("imperialId") String user, @PathParam("infoType") int infoType) {
        return Response.status(501).build();
    }

    @POST
    @Path("{imperialId}/type/{infoType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addInfoTypeForUser(@PathParam("imperialId") String user, @PathParam("infoType") int infoType, String json) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{imperialId}/type/{infoType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateInfo(@PathParam("imperialId") String user, @PathParam("infoType") int infoType, String json) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{imperialId}/type/{infoType}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteInfoTypeForUser(@PathParam("imperialId") String user, @PathParam("infoType") int infoType) {
        return Response.status(501).build();
    }
}

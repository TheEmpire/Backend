/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.helper.ImageHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Path("image")
public class ImageServlet {
    @Inject
    ImageHelper helper;
    @Inject
    ResponseBuilder builder;

    @GET
    @Path("{fileName}")
    @Produces({MediaType.APPLICATION_JSON, "image/png", "image/svg"})
    public Response getImage(@PathParam("fileName") String fileName, @DefaultValue("255,255,255") @QueryParam("color") String color) {
        String[] file = fileName.split("\\.");
        if (file.length != 2)
            return builder.create(ServerMessage.PATH_PARAM_WRONG);
        if (!file[1].equals("svg") && !file[1].equals("png")) {
            return builder.create(ServerMessage.PATH_PARAM_WRONG);
        }
        InputStream image = helper.readImageFromResource(fileName);
        if (image == null) {
            return builder.create(ServerMessage.IMAGE_NOT_FOUND);
        }
        if (file[1].equals("png") && fileName.contains("logo")) {
            if (!color.equals("255,255,255")) {
                String[] rgb = color.split(",");
                color = color + ",";
                if (rgb.length != 3 || !color.matches("((0|[1-9][0-9]?|1[0-9]{2}|2([0-4][0-9]|5[0-5])),){3}"))
                    return builder.create(ServerMessage.QUERY_PARAM_WRONG);
                image = helper.convertPng(image, Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]), Integer.parseInt(rgb[2]));
            }
            if (image == null) {
                return builder.create(ServerMessage.COLOR_CONVERSION_ERROR);
            }
        }
        return builder.create(image, file[1]);
    }
}

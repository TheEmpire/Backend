/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("online")
public class OnlineServlet {
    @GET
    @Produces("image/png,image/svg")
    public Response wrapperWithImage(@QueryParam("token") String token,
                                     @QueryParam("users") String users,
                                     @DefaultValue("png") @QueryParam("pictureFormat") String format,
                                     @DefaultValue("true") @QueryParam("online") boolean online,
                                     @DefaultValue("true") @QueryParam("reset") boolean reset) {
        return Response.status(501).build();
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response setOnline(@QueryParam("users") String users,
                              @DefaultValue("true") @QueryParam("online") boolean online,
                              @DefaultValue("true") @QueryParam("reset") boolean reset) {
        return Response.status(501).build();
    }
}

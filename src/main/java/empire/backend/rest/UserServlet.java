/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;
import empire.backend.application.WithRole;
import empire.backend.database.UserQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.HashHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.internal.UserShare;
import empire.backend.model.internal.UserWithId;
import empire.backend.model.schemas.auth.ValidationHash;
import empire.backend.model.schemas.UpdateValue;
import empire.backend.model.schemas.user.UserUpdate;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("user")
public class UserServlet {
    @Inject
    CustomGson gson;
    @Inject
    ResponseBuilder builder;
    @Inject
    UserQueries queries;
    @Inject
    JWTHelper jwtHelper;
    @Inject
    HashHelper encryptionHelper;

    @PUT
    @Secured
    @WithRole(table = "user", self = true)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(String json) {
        UserUpdate user = gson.fromJson(json, UserUpdate.class);
        if (user != null && user.isValid()) {
            Boolean result = queries.updateUser(user);
            if (result == null)
                return builder.create(ServerMessage.DATABASE_ERROR);
            else if (result)
                return builder.create(ServerMessage.USER_UPDATED);
            else
                return builder.create(ServerMessage.USER_NOT_FOUND);
        } else
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @GET
    @Secured
    @Path("{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("imperialId") String user, @Context SecurityContext securityContext) {
        String currentUser = jwtHelper.getName(securityContext);
        UserWithId userData = queries.getUser(user);
        if (userData == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (!userData.exists())
            return builder.create(ServerMessage.USER_NOT_FOUND);
        else {
            if (currentUser.equals(user))
                return builder.create(userData.prepareForExport());
            else {
                UserShare share = queries.getUserShare(user);
                return builder.create(userData.prepareForExport().resetNotShared(share));
            }
        }
    }

    @DELETE
    @Secured
    @WithRole(table = "user", self = true)
    @Path("{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("imperialId") String user) {
        Boolean result = queries.deleteUser(user);
        if (result == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (result)
            return builder.create(ServerMessage.USER_DELETED);
        else
            return builder.create(ServerMessage.USER_NOT_FOUND);
    }

    @POST
    @Secured
    @WithRole(table = "user", self = true)
    @Path("{imperialId}/language/{language}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addLanguage(@PathParam("imperialId") String user, @PathParam("language") String lang) {
        int result = queries.createLanguage(user, lang);
        if (result == 0)
            return builder.create(ServerMessage.LANGUAGE_ADDED);
        else if (result == 1)
            return builder.create(ServerMessage.USER_NOT_FOUND);
        else if (result == 2)
            return builder.create(ServerMessage.LANGUAGE_ALREADY_EXISTS);
        else
            return builder.create(ServerMessage.DATABASE_ERROR);
    }

    @PUT
    @Secured
    @WithRole(table = "user", self = true)
    @Path("{imperialId}/language/{language}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateLanguage(@PathParam("imperialId") String user, @PathParam("language") String lang, String json) {
        UpdateValue value = gson.fromJson(json, UpdateValue.class);
        if (value == null || !value.isValid()) {
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        }
        Boolean result = queries.updateLanguage(user, lang, value.getValue());
        if (result == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (result)
            return builder.create(ServerMessage.LANGUAGE_UPDATED);
        else
            return builder.create(ServerMessage.USER_OR_LANGUAGE_NOT_FOUND);
    }

    @DELETE
    @Secured
    @WithRole(table = "user")
    @Path("{imperialId}/language/{language}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteLanguage(@PathParam("imperialId") String user, @PathParam("language") String lang) {
        Boolean result = queries.deleteLanguage(user, lang);
        if (result == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (result)
            return builder.create(ServerMessage.LANGUAGE_REMOVED);
        else
            return builder.create(ServerMessage.USER_OR_LANGUAGE_NOT_FOUND);
    }

    @POST
    @Path("{imperialId}/validate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateEmail(@PathParam("imperialId") String userId, String json) {
        ValidationHash hash = gson.fromJson(json, ValidationHash.class);
        if (hash == null || !hash.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        else {
            return validateEmailValidBody(hash, userId);
        }
    }

    @GET
    @Path("{imperialId}/exists")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userExists(@PathParam("imperialId") String user) {
        Boolean exists = queries.userExists(user);
        if (exists == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (exists)
            return builder.create(ServerMessage.REGISTERED);
        else
            return builder.create(ServerMessage.NOT_REGISTERED);
    }

    private Response validateEmailValidBody(ValidationHash hash, String userId) {
        UserWithId user = queries.getUser(userId);
        if (user == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (!user.exists())
            return builder.create(ServerMessage.USER_NOT_FOUND);
        else {
            return validateRequestHashes(user, hash, userId);
        }
    }

    private Response validateRequestHashes(UserWithId user, ValidationHash hash, String userId) {
        String hashed = encryptionHelper.hash(user.getMail(), userId);
        if (hashed.equals(hash.getHash())) {
            Boolean result = queries.setVerified(user.getId(), true);
            if (result != null && result)
                return builder.create(ServerMessage.MAIL_VALIDATED);
            else
                return builder.create(ServerMessage.DATABASE_ERROR);
        } else
            return builder.create(ServerMessage.AUTH_INCORRECT);
    }
}

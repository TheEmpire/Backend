/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;
import empire.backend.application.WithRole;
import empire.backend.database.InfoTypeQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.schemas.info.InfoTypeArray;
import empire.backend.model.schemas.info.InfoTypeCreate;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Secured
@Path("types")
public class InfoTypeServlet {
    @Inject
    InfoTypeQueries typeQueries;
    @Inject
    ResponseBuilder builder;
    @Inject
    CustomGson gson;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTypes() {
        InfoTypeArray types = typeQueries.getTypes();
        if (types == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else
            return builder.create(types);
    }

    @POST
    @WithRole(table = "InfoType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createType(String json) {
        InfoTypeCreate type = gson.fromJson(json, InfoTypeCreate.class);
        if (type == null || !type.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        Boolean result = typeQueries.insertType(type);
        if (result == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (result)
            return builder.create(ServerMessage.TYPE_CREATED);
        else
            return builder.create(ServerMessage.INFO_TYPE_ALREADY_EXISTS);

    }

    @PUT
    @Path("{id}")
    @WithRole(table = "InfoType")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateType(@PathParam("id") int typeId, String json) {
        if (typeId < 0)
            return builder.create(ServerMessage.PATH_PARAM_WRONG);
        InfoTypeCreate type = gson.fromJson(json, InfoTypeCreate.class);
        if (type == null || !type.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        Boolean result = typeQueries.updateType(type, typeId);
        if (result == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (result)
            return builder.create(ServerMessage.TYPE_UPDATED);
        else
            return builder.create(ServerMessage.TYPE_NOT_FOUND);

    }

    @DELETE
    @Path("{id}")
    @WithRole(table = "InfoType")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteType(@PathParam("id") int type) {
        if (type < 0)
            return builder.create(ServerMessage.PATH_PARAM_WRONG);
        Boolean result = typeQueries.deleteType(type);
        if (result == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (result)
            return builder.create(ServerMessage.TYPE_DELETED);
        else
            return builder.create(ServerMessage.TYPE_NOT_FOUND);
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class RootServlet {
    private static String productionApi = "https://rest.cloud-in.space/empire";
    private static String productionDoc = "https://swagger.cloud-in.space/?urls.primaryName=Empire%20API";
    private static String stagingApi = "https://rest.cloud-in.space/empireStaging";
    private static String stagingDoc = "https://swagger.cloud-in.space/?urls.primaryName=Empire%20API%20Staging";
    private static String html = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "  <title>Empire API Documentation</title>\n" +
            "</head>\n" +
            "<body>\n" +
            "  <h1>Documentation</h1>\n" +
            "  This API was designed using the <a href=\"https://swagger.io/docs/specification/about/\">OpenApi Standard 3.0</a> from Swagger.<br /><br />\n" +
            "  <h4>The API is reachable under these sites</h4>\n" +
            "    <ul>\n" +
            "      <li> <a href=\"" + productionApi + "\">Production</a></li>\n" +
            "      <li> <a href=\"" + stagingApi + "\">Staging (Dev)</a></li>\n" +
            "    </ul>\n" +
            "    <h4>The documentation can be found under the following sites</h4>\n" +
            "    <ul>\n" +
            "      <li><a href=\"" + productionDoc + "\">Production</a></li>\n" +
            "      <li><a href=\"" + stagingDoc + "\">Staging (Dev)</a></li>\n" +
            "    </ul>\n" +
            "  <body>\n" +
            "</html>";

    @GET
    public Response getLinkCollection() {
        return Response.ok().entity(html).build();
    }
}

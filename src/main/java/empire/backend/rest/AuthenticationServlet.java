/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;
import empire.backend.database.UserQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.HashHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.helper.mail.MailHelper;
import empire.backend.model.enums.AvailableLanguage;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.internal.UserAuthentication;
import empire.backend.model.internal.UserWithId;
import empire.backend.model.schemas.auth.Login;
import empire.backend.model.schemas.auth.PasswordReset;
import empire.backend.model.schemas.auth.PasswordUpdate;
import empire.backend.model.schemas.user.Registration;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("authentication")
public class AuthenticationServlet {
    @Inject
    HashHelper encryption;
    @Inject
    ResponseBuilder builder;
    @Inject
    JWTHelper jwtHelper;
    @Inject
    UserQueries user;
    @Inject
    CustomGson gson;
    @Inject
    MailHelper mailHelper;

    @Resource(name = "salt")
    private String saltPwd;

    @POST
    @Path("register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(String json) {
        Registration data = gson.fromJson(json, Registration.class);
        if (data == null || !data.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        Boolean userExists = user.userExists(data.getImperialId());
        if (userExists == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (!userExists) {
            String userSalt = encryption.getSalt(26);
            if (Boolean.TRUE.equals(user.createUser(data, generatePassword(data.getPassword(), userSalt), userSalt))) {
                String jwt = jwtHelper.createJWT(data.getImperialId(), data.getMail(), false);
                return checkJWT(jwt, ServerMessage.USER_CREATED);
            } else
                return builder.create(ServerMessage.DATABASE_ERROR);
        } else {
            return builder.create(ServerMessage.USER_ALREADY_EXISTS);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(String json) {
        Login data = gson.fromJson(json, Login.class);
        if (data == null || !data.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        else {
            UserAuthentication userAuth = user.getUserCredentials(data.getImperialId());
            if (userAuth == null)
                return builder.create(ServerMessage.DATABASE_ERROR);
            else if (userAuth.isEmpty())
                return builder.create(ServerMessage.USER_NOT_FOUND);
            else if (encryption.verifyUserPassword(data.getPassword(), userAuth.getPassword(), saltPwd + userAuth.getSalt())) {
                String jwt = jwtHelper.createJWT(data.getImperialId(), userAuth.getMail(), userAuth.isVerified());
                return checkJWT(jwt, ServerMessage.LOGIN_SUCCESS);
            } else
                return builder.create(ServerMessage.ACCESS_DENIED);
        }
    }

    @PUT
    @Secured
    @Produces(MediaType.APPLICATION_JSON)
    public Response renewToken(@Context SecurityContext securityContext) {
        String jwt = jwtHelper.renewJWT(securityContext.getUserPrincipal().getName());
        return checkJWT(jwt, ServerMessage.RENEW_SUCCESS);
    }

    @POST
    @Secured
    @Path("changePassword")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePassword(String json) {
        PasswordUpdate data = gson.fromJson(json, PasswordUpdate.class);
        if (data == null || !data.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        else {
            UserAuthentication userAuth = user.getUserCredentials(data.getImperialId());
            if (userAuth == null)
                return builder.create(ServerMessage.DATABASE_ERROR);
            else if (userAuth.isEmpty())
                return builder.create(ServerMessage.USER_NOT_FOUND);
            else if (encryption.verifyUserPassword(data.getOldPassword(), userAuth.getPassword(), saltPwd + userAuth.getSalt()))
                return updatePassword(userAuth, data.getImperialId(), data.getNewPassword());
            else
                return builder.create(ServerMessage.AUTH_INCORRECT);
        }
    }

    @POST
    @Path("resetPassword")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetPassword(String json) {
        PasswordReset data = gson.fromJson(json, PasswordReset.class);
        if (data == null || !data.isValid())
            return builder.create(ServerMessage.REQUEST_BODY_WRONG);
        else {
            UserAuthentication userAuth = user.getUserCredentials(data.getImperialId());
            if (userAuth == null)
                return builder.create(ServerMessage.DATABASE_ERROR);
            else if (userAuth.isEmpty())
                return builder.create(ServerMessage.USER_NOT_FOUND);
            else if (encryption.hash(userAuth.getPassword(), saltPwd).equals(data.getHashOne()) && encryption.hash(userAuth.getPassword(), data.getImperialId()).equals(data.getHashTwo()))
                return updatePassword(userAuth, data.getImperialId(), data.getNewPassword());
            else
                return builder.create(ServerMessage.AUTH_INCORRECT);
        }
    }

    @GET
    @Path("requestReset/{language}/{identifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendReset(@PathParam("language") String lang, @PathParam("identifier") String identifier) {
        return sendMail(lang, identifier, false);
    }

    @GET
    @Path("requestValidation/{language}/{identifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendValidation(@PathParam("language") String lang, @PathParam("identifier") String identifier) {
        return sendMail(lang, identifier, true);
    }

    private Response sendMail(String lang, String identifier, boolean validation) {
        AvailableLanguage language;
        try {
            language = AvailableLanguage.valueOf(lang);
        } catch (IllegalArgumentException e) {
            return builder.create(ServerMessage.PATH_PARAM_WRONG);
        }
        if (identifier == null)
            return builder.create(ServerMessage.PATH_PARAM_WRONG);
        String imperialId = identifier;
        if (identifier.contains("@"))
            imperialId = user.getIdForMail(identifier);
        if (imperialId == null)
            return builder.create(ServerMessage.USER_NOT_FOUND);
        UserAuthentication userAuth = user.getUserCredentials(imperialId);
        if (userAuth == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        else if (userAuth.isEmpty())
            return builder.create(ServerMessage.USER_NOT_FOUND);
        else {
            String[] mail;
            ServerMessage success;
            if (validation) {
                mail = mailHelper.getValidationMail(userAuth.getMail(), imperialId, language);
                success = ServerMessage.VALIDATION_MAIL_SENT;
            } else {
                mail = mailHelper.getResetMail(imperialId, encryption.hash(userAuth.getPassword(), saltPwd),
                        encryption.hash(userAuth.getPassword(), imperialId), language);
                success = ServerMessage.RESET_MAIL_SENT;
            }
            if (mail.length == 0)
                return builder.create(ServerMessage.MAIL_SEND_ERROR);
            else
                return mailHelper.sendMail(mail[0], userAuth.getMail(), mail[1], success);
        }
    }

    private Response updatePassword(UserAuthentication userAuth, String imperialId, String password) {
        String hashedPassword = generatePassword(password, userAuth.getSalt());
        UserWithId userData = user.getUser(imperialId);
        if (userData == null)
            return builder.create(ServerMessage.DATABASE_ERROR);
        if (user.updatePassword(userData.getId(), hashedPassword)) {
            String jwt = jwtHelper.createJWT(imperialId, userData.getMail(), userAuth.isVerified());
            return checkJWT(jwt, ServerMessage.PASSWORD_UPDATED);
        } else
            return builder.create(ServerMessage.DATABASE_ERROR);
    }

    private Response checkJWT(String jwt, ServerMessage msg) {
        if (jwt != null)
            return builder.create(msg, jwt);
        else
            return builder.create(ServerMessage.JWT_ERROR);
    }

    private String generatePassword(String pwd, String salt) {
        return encryption.hash(pwd, saltPwd + salt);
    }
}

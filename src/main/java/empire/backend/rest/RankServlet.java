/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Secured
@Path("rank")
public class RankServlet {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRanks() {
        return Response.status(501).build();
    }

    @POST
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRank(@PathParam("name") String rankName) {
        return Response.status(501).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsersForRank(@PathParam("id") int rankId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRank(@PathParam("id") int rankId, String json) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteRank(@PathParam("id") int rankId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}/user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignRankToUser(@PathParam("id") int rankId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetRankOfUser(@PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }
}

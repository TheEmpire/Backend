/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Secured
@Path("squad")
public class SquadServlet {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSquads() {
        return Response.status(501).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createSquad(String json) {
        return Response.status(501).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSquad(@PathParam("id") int squadId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateSquad(@PathParam("id") int squadId, String json) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSquad(@PathParam("id") int squadId) {
        return Response.status(501).build();
    }

    @POST
    @Path("{id}/user/{imperialId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePositionInSquad(@PathParam("id") int squadId, @PathParam("imperialId") String imperialId, String json) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}/user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignSquad(@PathParam("id") int squadId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{id}/user/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSquad(@PathParam("id") int squadId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }
}

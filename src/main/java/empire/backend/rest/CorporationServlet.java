/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.application.Secured;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Secured
@Path("corporation")
public class CorporationServlet {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCorporations() {
        return Response.status(501).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCorporation(String json) {
        return Response.status(501).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCorporation(@PathParam("id") int corpId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCorporation(@PathParam("id") int corpId, String json) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCorporation(@PathParam("id") int corpId) {
        return Response.status(501).build();
    }

    @GET
    @Path("{id}/legate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLegatesOfCorporation(@PathParam("id") int corpId) {
        return Response.status(501).build();
    }

    @POST
    @Path("{id}/legate/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addLegateToCorporation(@PathParam("id") int corpId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @PUT
    @Path("legate/{id}/new/{imperialId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateLegate(@PathParam("id") int legateId, @PathParam("imperialId") String imperialId) {
        return Response.status(501).build();
    }

    @DELETE
    @Path("legate/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteLegate(@PathParam("id") int legateId) {
        return Response.status(501).build();
    }
}

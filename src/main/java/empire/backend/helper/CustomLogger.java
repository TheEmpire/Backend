package empire.backend.helper;

import org.slf4j.Logger;

import javax.inject.Inject;

public class CustomLogger {
    @Inject
    private Logger logger;

    public void log(String message, Exception e) {
        if (logger.isErrorEnabled())
            logger.error(message);
        if (logger.isDebugEnabled())
            logger.debug("Stacktrace", e);
    }
}

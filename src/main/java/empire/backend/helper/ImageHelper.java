/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageHelper {
    public InputStream readImageFromResource(String path) {
        return ImageHelper.class.getClassLoader().getResourceAsStream(path);
    }

    public InputStream convertPng(InputStream stream, int red, int green, int blue) {
        if (red < 0 || red > 255 || green < 0 || green > 255  || blue < 0 || blue > 255)
            return null;
        try {
            BufferedImage image = ImageIO.read(stream);
            int color = (red << 16) | (green << 8) | blue;
            int width = image.getWidth();
            int height = image.getHeight();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    int alpha = (image.getRGB(x, y) >> 24) & 0xFF;
                    if (alpha > 0)
                        image.setRGB(x, y, (alpha << 24) | color);
                }
            }
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "png", os);
            return new ByteArrayInputStream(os.toByteArray());
        } catch (NullPointerException | IOException e) {
            return null;
        }
    }
}

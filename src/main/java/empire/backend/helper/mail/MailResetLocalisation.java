/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.model.enums.AvailableLanguage;

public class MailResetLocalisation implements MailLocalisation {
    public String getSubject(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Deine angefragte Passwortzurücksetzung";
            default:
                return "Your requested password reset";
        }
    }

    public String getTitle(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Passwort zurücksetzen - The Empire";
            default:
                return "Password reset - The Empire";
        }
    }

    public String getButton(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Passwort zurücksetzen";
            default:
                return "Reset password";
        }
    }

    public String getReason(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Passwortzurücksetzung";
            default:
                return "Password reset";
        }
    }

    public String getWelcome(AvailableLanguage language, String imperialId) {
        switch (language) {
            case DE:
                return "Hallo " + imperialId;
            default:
                return "Hello " + imperialId;
        }
    }

    public String getShortDescription(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Du hast eine Passwortzurücksetzung angefordert<br />Wenn du das nicht warst, ignoriere diese Email bitte";
            default:
                return "You requested a password reset<br />If not please ignore this mail";
        }
    }

    public String getFooter(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Du erhälst diese Mail, da du einen Account für das " + MailLocalisation.NETWORK_LINK +
                        " besitzt.";
            default:
                return "You are receiving this mail because you have an account for the "  + MailLocalisation.NETWORK_LINK +
                        ".";
        }
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.model.enums.AvailableLanguage;

public class MailValidationLocalisation implements MailLocalisation {
    public String getSubject(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Bitte verifiziere deine Mail Addresse";
            default:
                return "Please validate your mail address";
        }
    }

    public String getTitle(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Mailverifzierung - The Empire";
            default:
                return "Mail validation - The Empire";
        }
    }

    public String getButton(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Mail verifizieren";
            default:
                return "Validate mail";
        }
    }

    public String getReason(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Mailverifizierung";
            default:
                return "Mail validation";
        }
    }

    public String getWelcome(AvailableLanguage language, String imperialId) {
        switch (language) {
            case DE:
                return "Willkommen " + imperialId;
            default:
                return "Welcome " + imperialId;
        }
    }

    public String getShortDescription(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Nur noch ein Schritt<br />Bitte verifiziere deine Email";
            default:
                return "Only one step left <br />Please verify your mail";
        }
    }

    public String getFooter(AvailableLanguage language) {
        switch (language) {
            case DE:
                return "Du erhälst diese Mail, da du dich für einen Zugang zum " + MailLocalisation.NETWORK_LINK +
                        " registriert hast. Solltest du dies nicht gewesen sein, bitte ignoriere diese Mail.";
            default:
                return "You are receiving this mail because you signed up for access to the "  + MailLocalisation.NETWORK_LINK +
                        ". Please ignore this mail, if it was not you.";
        }
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.model.enums.AvailableLanguage;

public interface MailLocalisation {
    String NETWORK_LINK = "<a href=\"https://empire.cloud-in.space\" style=\"color: #CACACA\">Empire Network</a>";

    String getSubject(AvailableLanguage language);

    String getTitle(AvailableLanguage language);

    String getButton(AvailableLanguage language);

    String getReason(AvailableLanguage language);

    String getWelcome(AvailableLanguage language, String imperialId);

    String getShortDescription(AvailableLanguage language);

    String getFooter(AvailableLanguage language);

    default String getErrorDescription(AvailableLanguage language, String link) {
        switch (language) {
            case DE:
                return "Wenn der obige Link nicht funktioniert, kopiere den folgenden in deinen Browser <a href=\""
                        + link + "\" style=\"color: #CACACA\">" + link + "</a>";
            default:
                return "If the above link does not work, please copy the following link into your browser: <a href=\""
                        + link + "\" style=\"color: #CACACA\">" + link + "</a>";
        }
    }

    default String getCopyright(AvailableLanguage language, int year) {
        switch (language) {
            case DE:
                return year + " Alle Rechte vorbehalten";
            default:
                return year + " All rights reserved";
        }
    }
}

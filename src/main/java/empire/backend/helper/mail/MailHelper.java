/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.helper.HashHelper;
import empire.backend.helper.ReadResource;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.AvailableLanguage;
import empire.backend.model.enums.ServerMessage;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.Response;
import java.time.Year;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class MailHelper {
    @Inject
    HashHelper encryption;
    @Inject
    MailValidationLocalisation validationLocalisation;
    @Inject
    MailResetLocalisation resetLocalisation;
    @Inject
    ResponseBuilder builder;
    @Inject
    ReadResource file;
    @Inject
    MailSessionHelper sessionHelper;

    @Resource(name = "mailUser")
    private String user;
    @Resource(name = "mailPassword")
    private String password;

    String template;
    boolean internalException = false;
    Consumer<Message> messageSender;
    Supplier<Message> messageSupplier;

    @PostConstruct
    public void doPostConstruct() {
        template = file.readResource("mail_template.html");
        Session session = sessionHelper.prepareMail(user, password);
        messageSupplier = () -> {
            Message msg = new MimeMessage(session);
            try {
                msg.setFrom(new InternetAddress(user));
            } catch (MessagingException e) {
                internalException = true;
            }
            return msg;
        };
        messageSender = message -> {
            try {
                Transport.send(message);
            } catch (MessagingException e) {
                internalException = true;
            }
        };
    }

    private String populateTemplate(String title, String buttonAction, String buttonName, String reason, String welcome,
                                    String shortDescription, String errorDescription, String footer, String copyright, String language) {
        String populatedTemplate = this.template;
        if (populatedTemplate != null) {
            populatedTemplate = populatedTemplate.replace("LANG", language);
            populatedTemplate = populatedTemplate.replace("REASON", reason);
            populatedTemplate = populatedTemplate.replace("WELCOME", welcome);
            populatedTemplate = populatedTemplate.replace("TITLE", title);
            populatedTemplate = populatedTemplate.replace("BUTTON_ACTION", buttonAction);
            populatedTemplate = populatedTemplate.replace("BUTTON_NAME", buttonName);
            populatedTemplate = populatedTemplate.replace("SHORT_DESCRIPTION", shortDescription);
            populatedTemplate = populatedTemplate.replace("ERROR_DESCRIPTION", errorDescription);
            populatedTemplate = populatedTemplate.replace("FOOTER", footer);
            populatedTemplate = populatedTemplate.replace("COPYRIGHT", copyright);
        }
        return populatedTemplate;
    }

    public String[] getValidationMail(String mail, String imperialId, AvailableLanguage language) {
        if (mail != null && imperialId != null && language != null) {
            String buttonAction = getValidationLink(mail, imperialId, language.name());
            return new String[]{populateTemplate(validationLocalisation.getTitle(language), buttonAction,
                    validationLocalisation.getButton(language), validationLocalisation.getReason(language),
                    validationLocalisation.getWelcome(language, imperialId), validationLocalisation.getShortDescription(language),
                    validationLocalisation.getErrorDescription(language, buttonAction), validationLocalisation.getFooter(language),
                    validationLocalisation.getCopyright(language, Year.now().getValue()), language.name()), validationLocalisation.getSubject(language)};
        }
        return new String[] {};
    }


    public String[] getResetMail(String imperialId, String hashOne, String hashTwo, AvailableLanguage language) {
        if (hashOne != null && hashTwo != null && imperialId != null && language != null) {
            String buttonAction = getResetLink(imperialId, hashOne, hashTwo, language.name());
            return new String[]{populateTemplate(resetLocalisation.getTitle(language), buttonAction,
                    resetLocalisation.getButton(language), resetLocalisation.getReason(language),
                    resetLocalisation.getWelcome(language, imperialId), resetLocalisation.getShortDescription(language),
                    resetLocalisation.getErrorDescription(language, buttonAction), resetLocalisation.getFooter(language),
                    resetLocalisation.getCopyright(language, Year.now().getValue()), language.name()), resetLocalisation.getSubject(language)};
        }
        return new String[] {};
    }

    public Response sendMail(String content, String mail, String subject, ServerMessage serverMessage) {
        try {
            Message message = messageSupplier.get();
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
            message.setSubject(subject);
            message.setContent(content, "text/html; charset=utf-8");

            messageSender.accept(message);
            if (internalException)
                return builder.create(ServerMessage.MAIL_SEND_ERROR);
            else
                return builder.create(serverMessage);
        } catch (MessagingException e) {
            return builder.create(ServerMessage.MAIL_SEND_ERROR);
        }

    }

    private String getValidationLink(String email, String id, String lang) {
        String hash = encryption.hash(email, id);
        return "https://empire.cloud-in.space/" + lang.toLowerCase() + "/validation?id=" + id + "&hash=" + hash;
    }

    private String getResetLink(String id, String hashOne, String hashTwo, String lang) {
        return "https://empire.cloud-in.space/" + lang.toLowerCase() + "/resetPwd?id=" + id + "&hashOne=" + hashOne + "&hashTwo=" + hashTwo;
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import empire.backend.database.UserQueries;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.ws.rs.core.SecurityContext;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class JWTHelper {
    private static final String VERIFIED = "verified";
    private static final String NAME = "name";
    private static final String MAIL = "mail";
    private static final String ISSUER = "empireREST";
    @Inject
    UserQueries user;

    @Resource(name = "jwtSecret")
    private String secret;

    Algorithm alg;
    JWTVerifier verifier;

    @PostConstruct
    public void postConstructInit() {
        if (alg == null)
            alg = Algorithm.HMAC512(secret.getBytes());
        if (verifier == null)
            verifier = JWT.require(alg).withIssuer(ISSUER).build();
    }

    public String createJWT(String name, String mail) {
        return createJWT(name, mail, user.isVerified(name));
    }

    public String createJWT(String name, String mail, boolean verified) {
        return createJWT(name, mail, verified, Clock.systemUTC());
    }

    public String createJWT(String name, String mail, boolean verified, Clock clock) {
        LocalDateTime time = LocalDateTime.now(clock).plusMinutes(30);
        return JWT.create()
                .withIssuer(ISSUER)
                .withClaim(NAME, name)
                .withClaim(MAIL, mail)
                .withClaim(VERIFIED, verified)
                .withExpiresAt(Date.from(time.toInstant(ZoneOffset.UTC)))
                .sign(alg);
    }

    public String renewJWT(String oldJWT) {
        try {
            DecodedJWT token = JWT.decode(oldJWT);
            return createJWT(token.getClaim(NAME).asString(), token.getClaim(MAIL).asString(),
                    token.getClaim(VERIFIED).asBoolean());
        } catch (JWTDecodeException e) {
            return "";
        }

    }

    public String getName(String token) {
        try {
            return JWT.decode(token).getClaim(NAME).asString();
        } catch (JWTDecodeException e) {
            return "";
        }
    }

    public String getName(SecurityContext context) {
        return getName(context.getUserPrincipal().getName());
    }

    public String getMail(String token) {
        try {
            return JWT.decode(token).getClaim(MAIL).asString();
        } catch (JWTDecodeException e) {
            return "";
        }
    }

    public String getMail(SecurityContext context) {
        return getMail(context.getUserPrincipal().getName());
    }


    public boolean isVerified(String token) {
        try {
            return JWT.decode(token).getClaim(VERIFIED).asBoolean();
        } catch (JWTDecodeException e) {
            return false;
        }
    }

    public boolean isVerified(SecurityContext context) {
        return isVerified(context.getUserPrincipal().getName());
    }


    public boolean verifyJWT(String token) {
        try {
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException e) {
            return false;
        }
    }
}

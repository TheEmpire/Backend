/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import empire.backend.InitMocks;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class ReadResourceTest extends InitMocks {
    @Mock
    IOWrapper wrapper;
    @InjectMocks
    ReadResource file;

    @Test
    void testReadResourceInvalidResource() {
        when(wrapper.getResource("invalid")).thenReturn(null);
        assertEquals("", file.readResource("invalid"));
    }

    @Test
    void testReadResourceEmptyFile() {
        when(wrapper.getResource("valid")).thenReturn(new ByteArrayInputStream(new byte[]{}));
        assertEquals("", file.readResource("valid"));
    }

    @Test
    void testReadResource() {
        when(wrapper.getResource("valid")).thenReturn(new ByteArrayInputStream("test".getBytes()));
        assertEquals("test", file.readResource("valid"));
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class ImageHelperTest {
    private  ImageHelper helper;

    @BeforeEach
    void setupHelper() {
        helper = new ImageHelper();
    }

    @Test
    void testReadImageFromResourceExistent() {
        assertNotNull(helper.readImageFromResource("logo.png"));
    }

    @Test
    void testReadImageFromResourceNonExistent() {
        assertNull(helper.readImageFromResource("logo2.png"));
    }

    @Test
    void testConvertPngValidInputStreamValidColorInput() {
        assertNotNull(helper.convertPng(helper.readImageFromResource("logo.png"), 255, 0, 0));
    }

    @Test
    void testConvertPngRedTooHigh() {
        assertNull(helper.convertPng(helper.readImageFromResource("logo.png"), 256, 0, 0));
    }

    @Test
    void testConvertPngRedNegative() {
        assertNull(helper.convertPng(helper.readImageFromResource("logo.png"), -1, 0, 0));
    }

    @Test
    void testConvertPngGreenTooHigh() {
        assertNull(helper.convertPng(helper.readImageFromResource("logo.png"), 0, 256, 0));
    }

    @Test
    void testConvertPngGreenNegative() {
        assertNull(helper.convertPng(helper.readImageFromResource("logo.png"), 0, -1, 0));
    }

    @Test
    void testConvertPngBlueTooHigh() {
        assertNull(helper.convertPng(helper.readImageFromResource("logo.png"), 0, 0, 256));
    }

    @Test
    void testConvertPngBlueNegative() {
        assertNull(helper.convertPng(helper.readImageFromResource("logo.png"), 0, 0, -1));
    }

    @Test
    void testConvertPngInputStreamNoImage() {
        assertNull(helper.convertPng(new ByteArrayInputStream("test".getBytes()), 255, 0, 0));
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;
import empire.backend.InitMocks;
import empire.backend.database.UserQueries;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.core.SecurityContext;
import java.nio.file.attribute.UserPrincipal;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class JWTHelperTest extends InitMocks {
    @Mock
    UserQueries user;
    @InjectMocks
    JWTHelper helper;

    @BeforeEach
    void doPostConstruct() {
        setPrivateVar("test", "secret", helper);
        helper.postConstructInit();
    }

    @Test
    void testPostConstructInit() {
        assertNotNull(helper.alg);
        assertNotNull(helper.verifier);
    }

    @Test
    void testPostConstructInitTwice() {
        String expectedId = helper.alg.getSigningKeyId();
        JWTVerifier verifier = helper.verifier;
        setPrivateVar("test2", "secret", helper);
        helper.postConstructInit();
        assertNotNull(helper.alg);
        assertEquals(expectedId, helper.alg.getSigningKeyId());
        assertNotNull(helper.verifier);
        assertEquals(verifier, helper.verifier);
    }

    @Test
    void testCreateJWTUserVerifiedWithQuery() {
        LocalDateTime time = LocalDateTime.now(Clock.systemUTC()).plusMinutes(30).withNano(0);
        when(user.isVerified("user")).thenReturn(true);
        String jwt = helper.createJWT("user", "mail@mail.com");
        DecodedJWT decodedJWT = JWT.decode(jwt);
        assertEquals("user", decodedJWT.getClaim("name").asString());
        assertEquals("mail@mail.com", decodedJWT.getClaim("mail").asString());
        assertEquals(true, decodedJWT.getClaim("verified").asBoolean());
        assertEquals("HS512", decodedJWT.getAlgorithm());
        assertEquals("empireREST", decodedJWT.getIssuer());
        assertTrue(Duration.between(time.toInstant(ZoneOffset.UTC), decodedJWT.getExpiresAt().toInstant()).getSeconds() < 15);
    }

    @Test
    void testCreateJWTUserNotVerifiedWithQuery() {
        LocalDateTime time = LocalDateTime.now(Clock.systemUTC()).plusMinutes(30).withNano(0);
        when(user.isVerified("user")).thenReturn(false);
        String jwt = helper.createJWT("user", "mail@mail.com");
        DecodedJWT decodedJWT = JWT.decode(jwt);
        assertEquals("user", decodedJWT.getClaim("name").asString());
        assertEquals("mail@mail.com", decodedJWT.getClaim("mail").asString());
        assertEquals(false, decodedJWT.getClaim("verified").asBoolean());
        assertEquals("HS512", decodedJWT.getAlgorithm());
        assertEquals("empireREST", decodedJWT.getIssuer());
        assertTrue(Duration.between(time.toInstant(ZoneOffset.UTC), decodedJWT.getExpiresAt().toInstant()).getSeconds() < 15);
    }

    @Test
    void testCreateJWT() {
        LocalDateTime time = LocalDateTime.now(Clock.systemUTC()).plusMinutes(30).withNano(0);
        String jwt = helper.createJWT("user", "mail@mail.com", true);
        DecodedJWT decodedJWT = JWT.decode(jwt);
        assertEquals("user", decodedJWT.getClaim("name").asString());
        assertEquals("mail@mail.com", decodedJWT.getClaim("mail").asString());
        assertEquals(true, decodedJWT.getClaim("verified").asBoolean());
        assertEquals("HS512", decodedJWT.getAlgorithm());
        assertEquals("empireREST", decodedJWT.getIssuer());
        assertTrue(Duration.between(time.toInstant(ZoneOffset.UTC), decodedJWT.getExpiresAt().toInstant()).getSeconds() < 15);
    }

    @Test
    void testRenewJWT() {
        LocalDateTime time = LocalDateTime.now(Clock.systemUTC()).plusMinutes(30).withNano(0);
        LocalDateTime timeBefore = LocalDateTime.now(Clock.systemUTC()).withNano(0);
        String jwt = helper.createJWT("user", "mail@mail.com", true, Clock.offset(Clock.systemUTC(), Duration.ofMinutes(-30)));
        DecodedJWT oldJWT = JWT.decode(jwt);
        String renewedJWT = helper.renewJWT(jwt);
        DecodedJWT decodedJWT = JWT.decode(renewedJWT);
        assertEquals("user", decodedJWT.getClaim("name").asString());
        assertEquals("mail@mail.com", decodedJWT.getClaim("mail").asString());
        assertEquals(true, decodedJWT.getClaim("verified").asBoolean());
        assertEquals("HS512", decodedJWT.getAlgorithm());
        assertEquals("empireREST", decodedJWT.getIssuer());
        assertTrue(Duration.between(time.toInstant(ZoneOffset.UTC), decodedJWT.getExpiresAt().toInstant()).getSeconds() < 15);
        assertEquals("user", oldJWT.getClaim("name").asString());
        assertEquals("mail@mail.com", oldJWT.getClaim("mail").asString());
        assertEquals(true, oldJWT.getClaim("verified").asBoolean());
        assertEquals("HS512", oldJWT.getAlgorithm());
        assertEquals("empireREST", oldJWT.getIssuer());
        assertTrue(Duration.between(timeBefore.toInstant(ZoneOffset.UTC), oldJWT.getExpiresAt().toInstant()).getSeconds() < 15);
    }

    @Test
    void testRenewJWTInvalidJWT() {
        String jwt = "invalid";
        String renewedJWT = helper.renewJWT(jwt);
        assertEquals("", renewedJWT);
    }

    @Test
    void testGetName() {
        String expected = "user";
        String jwt = helper.createJWT(expected, "mail@mail.com", true);
        assertEquals(expected, helper.getName(jwt));
    }

    @Test
    void testGetNameViaSecurityContext() {
        String expected = "user";
        SecurityContext context = mock(SecurityContext.class);
        UserPrincipal principal = mock(UserPrincipal.class);
        when(context.getUserPrincipal()).thenReturn(principal);
        when(principal.getName()).thenReturn(helper.createJWT(expected, "mail@mail.com", true));

        assertEquals(expected, helper.getName(context));
    }

    @Test
    void testGetNameInvalidJWT() {
        String jwt = "invalid";
        assertEquals("", helper.getName(jwt));
    }

    @Test
    void testGetMail() {
        String expected = "mail@mail.com";
        String jwt = helper.createJWT("user", expected, true);
        assertEquals(expected, helper.getMail(jwt));
    }

    @Test
    void testGetMailViaSecurityContext() {
        String expected = "mail@mail.com";
        SecurityContext context = mock(SecurityContext.class);
        UserPrincipal principal = mock(UserPrincipal.class);
        when(context.getUserPrincipal()).thenReturn(principal);
        when(principal.getName()).thenReturn(helper.createJWT("user", expected, true));

        assertEquals(expected, helper.getMail(context));
    }

    @Test
    void testGetMailInvalidJWT() {
        String jwt = "invalid";
        assertEquals("", helper.getMail(jwt));
    }

    @Test
    void testIsVerified() {
        String jwt = helper.createJWT("user", "mail@mail.com", true);
        assertTrue(helper.isVerified(jwt));
    }

    @Test
    void testIsVerifiedViaSecurityContext() {
        SecurityContext context = mock(SecurityContext.class);
        UserPrincipal principal = mock(UserPrincipal.class);
        when(context.getUserPrincipal()).thenReturn(principal);
        when(principal.getName()).thenReturn(helper.createJWT("user", "mail@mail.com", true));

        assertTrue(helper.isVerified(context));
    }

    @Test
    void testIsVerifiedInvalidJWT() {
        String jwt = "invalid";
        assertFalse(helper.isVerified(jwt));
    }

    @Test
    void testVerifyJWT() {
        String jwt = helper.createJWT("user", "mail@mail.com", true);
        assertTrue(helper.verifyJWT(jwt));
    }

    @Test
    void testVerifyJWTTooOld() {
        String jwt = helper.createJWT("user", "mail@mail.com", true, Clock.offset(Clock.systemUTC(), Duration.ofHours(-2)));
        assertFalse(helper.verifyJWT(jwt));
    }

    @Test
    void testVerifyJWTInvalidJWT() {
        String jwt = "invalid";
        assertFalse(helper.verifyJWT(jwt));
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HashHelperTest {
    private HashHelper encryption;

    @BeforeEach
    void setupEncryption() {
        encryption = new HashHelper();
    }

    @Test
    void testGetSalt() {
        String salt1 = encryption.getSalt(256);
        String salt2 = encryption.getSalt(256);
        String salt3 = encryption.getSalt(128);
        String salt4 = encryption.getSalt(128);
        String salt5 = encryption.getSalt(49);

        assertEquals(256, salt1.length());
        assertEquals(256, salt2.length());
        assertNotEquals(salt1, salt2);
        assertEquals(128, salt3.length());
        assertEquals(128, salt4.length());
        assertNotEquals(salt3, salt4);
        assertEquals(49, salt5.length());
    }

    @Test
    void testHash() {
        String hash1 = encryption.hash("pwd", "salt");
        String hash2 = encryption.hash("pwd", "salt");
        String hash3 = encryption.hash("pwd2", "salt");
        String hash4 = encryption.hash("pwd", "salt2");

        assertEquals(hash1, hash2);
        assertNotEquals(hash1, hash3);
        assertNotEquals(hash1, hash4);
    }

    @Test
    void testHashWrongAlgorithm() {
        encryption.setup("test", 1, 1, "invalidAlg");
        Throwable error = assertThrows(AssertionError.class, () -> encryption.hash("test", "salt"));
        assertEquals("Error while hashing a password: invalidAlg SecretKeyFactory not available", error.getMessage());
    }

    @Test
    void testVerifyUserPassword() {
        String hash = "cfAS6x2llsBuUgB6Kr8SkVJznQ4LCOQG0mJ0Z+hk2/xqXD5GNWdfbAtOCFQGokHA0FwREBnqoI1NzZeWs4xSwg==";
        String wrongHash = "bfAS6x2llsBuUgB6Kr8SkVJznQ4LCOQG0mJ0Z+hk2/xqXD5GNWdfbAtOCFQGokHA0FwREBnqoI1NzZeWs4xSwg==";
        assertTrue(encryption.verifyUserPassword("pwd", hash, "salt"));
        assertFalse(encryption.verifyUserPassword("pwd", wrongHash, "salt"));
        assertFalse(encryption.verifyUserPassword("pwd", hash, "salt2"));
        assertFalse(encryption.verifyUserPassword("pwd2", hash, "salt"));
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomGsonTest {
    private CustomGson gson;
    private TestObject expectedObject;
    private TestObject actualObject;
    private String expectedString;

    @BeforeEach
    void setupObject() {
        gson = new CustomGson();
        expectedObject = new TestObject(LocalDate.of(1900, 5, 20));
        actualObject = gson.fromJson("{\"date\":\"1900-05-20\"}", TestObject.class);
        expectedString = "{\"date\":\"1900-05-20\"}";
    }

    @Test
    void testToJsonLocalDate() {
        assertEquals(expectedString, gson.toJson(expectedObject));
    }

    @Test
    void testFromJsonLocalDate() {
        assertEquals(expectedObject.date, actualObject.date);
    }

    private static class TestObject {
        LocalDate date;

        TestObject(LocalDate date) {
            this.date = date;
        }
    }
}

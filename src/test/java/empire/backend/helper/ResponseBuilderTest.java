/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper;

import empire.backend.InitMocks;
import empire.backend.model.enums.ServerMessage;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;

import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResponseBuilderTest extends InitMocks {
    @Spy
    CustomGson gson;
    @InjectMocks
    ResponseBuilder builder;

    @Test
    void testCreateServerMessage() {
        ServerMessage[] message = ServerMessage.values();
        for (ServerMessage msg : message) {
            int expectedStatus = msg.getCode();
            String expectedBody = "{\"msg\":\"" + msg.name() + "\"}";
            Response actualResponse = builder.create(msg);

            assertEquals(expectedStatus, actualResponse.getStatus());
            assertEquals(expectedBody, actualResponse.getEntity().toString());
        }
    }

    @Test
    void testCreateServerMessageWithJWT() {
        String expectedJWT = "superSecret";
        ServerMessage[] messages = ServerMessage.values();
        for (ServerMessage msg : messages) {
            int expectedStatus = msg.getCode();
            String expectedBody = "{\"msg\":\"" + msg.name() + "\",\"token\":\"" + expectedJWT + "\"}";
            Response actualResponse = builder.create(msg, expectedJWT);

            assertEquals(expectedStatus, actualResponse.getStatus());
            assertEquals(expectedBody, actualResponse.getEntity().toString());
        }
    }

    @Test
    void testCreateImage() {
        int expectedStatus = 200;
        InputStream expectedBody = new ByteArrayInputStream("test".getBytes());
        String expectedContentType = "image/bla";
        Response actualResponse = builder.create(expectedBody, "bla");

        assertEquals(expectedStatus, actualResponse.getStatus());
        assertEquals(expectedBody, actualResponse.getEntity());
        assertEquals(expectedContentType, actualResponse.getHeaderString("Content-Type"));
    }

    @Test
    void testCreateArray() {
        int expectedStatus = 200;
        String[] array = {"test1", "test2", "testing"};
        String expectedBody = "[\"test1\",\"test2\",\"testing\"]";
        Response actualResponse = builder.create(array);

        assertEquals(expectedStatus, actualResponse.getStatus());
        assertEquals(expectedBody, actualResponse.getEntity());
    }
}

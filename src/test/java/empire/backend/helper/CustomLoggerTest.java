package empire.backend.helper;

import empire.backend.InitMocks;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class CustomLoggerTest extends InitMocks {
    @Mock
    Logger logger;
    @InjectMocks
    CustomLogger customLogger;

    @Test
    void testNoLogging() {
        customLogger.log("test", new Exception());
        verify(logger, times(0)).error(any());
        verify(logger, times(0)).debug(any(), any(Throwable.class));
    }

    @Test
    void testDebugLogging() {
        when(logger.isDebugEnabled()).thenReturn(true);
        customLogger.log("test", new Exception());
        verify(logger, times(0)).error(any());
        verify(logger).debug(eq("Stacktrace"), any(Throwable.class));
    }

    @Test
    void testErrorLogging() {
        when(logger.isErrorEnabled()).thenReturn(true);
        customLogger.log("test", new Exception());
        verify(logger).error("test");
        verify(logger, times(0)).debug(any(), any(Throwable.class));
    }

    @Test
    void testErrorAndDebugLogging() {
        when(logger.isDebugEnabled()).thenReturn(true);
        when(logger.isErrorEnabled()).thenReturn(true);
        customLogger.log("test", new Exception());
        verify(logger).error("test");
        verify(logger).debug(eq("Stacktrace"), any(Throwable.class));
    }
}

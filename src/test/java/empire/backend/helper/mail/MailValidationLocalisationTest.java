/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.InitMocks;
import empire.backend.model.enums.AvailableLanguage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class MailValidationLocalisationTest extends InitMocks {
    @Mock
    MailValidationLocalisation localisation;

    @BeforeEach
    void initMock() {
        when(localisation.getSubject(any())).thenCallRealMethod();
        when(localisation.getTitle(any())).thenCallRealMethod();
        when(localisation.getButton(any())).thenCallRealMethod();
        when(localisation.getReason(any())).thenCallRealMethod();
        when(localisation.getWelcome(any(), any())).thenCallRealMethod();
        when(localisation.getShortDescription(any())).thenCallRealMethod();
        when(localisation.getFooter(any())).thenCallRealMethod();
    }

    @Test
    void testCreateInstance() {
        MailValidationLocalisation actual = new MailValidationLocalisation();
        assertNotNull(actual);
    }

    @Test
    void testGetSubjectDe() {
        String expectedSubject = "Bitte verifiziere deine Mail Addresse";
        String actual = localisation.getSubject(AvailableLanguage.DE);
        assertEquals(expectedSubject, actual);
    }

    @Test
    void testGetSubjectEn() {
        String expectedSubject = "Please validate your mail address";
        String actual = localisation.getSubject(AvailableLanguage.EN);
        assertEquals(expectedSubject, actual);
    }

    @Test
    void testGetTitleDe() {
        String expectedTitle = "Mailverifzierung - The Empire";
        String actual = localisation.getTitle(AvailableLanguage.DE);
        assertEquals(expectedTitle, actual);
    }

    @Test
    void testGetTitleEn() {
        String expectedTitle = "Mail validation - The Empire";
        String actual = localisation.getTitle(AvailableLanguage.EN);
        assertEquals(expectedTitle, actual);
    }

    @Test
    void testGetButtonDe() {
        String expectedButton = "Mail verifizieren";
        String actual = localisation.getButton(AvailableLanguage.DE);
        assertEquals(expectedButton, actual);
    }

    @Test
    void testGetButtonEn() {
        String expectedButton = "Validate mail";
        String actual = localisation.getButton(AvailableLanguage.EN);
        assertEquals(expectedButton, actual);
    }

    @Test
    void testGetReasonDe() {
        String expectedReason = "Mailverifizierung";
        String actual = localisation.getReason(AvailableLanguage.DE);
        assertEquals(expectedReason, actual);
    }

    @Test
    void testGetReasonEn() {
        String expectedReason = "Mail validation";
        String actual = localisation.getReason(AvailableLanguage.EN);
        assertEquals(expectedReason, actual);
    }

    @Test
    void testGetWelcomeDe() {
        String expectedWelcome = "Willkommen UserId";
        String actual = localisation.getWelcome(AvailableLanguage.DE, "UserId");
        assertEquals(expectedWelcome, actual);
    }

    @Test
    void testGetWelcomeEn() {
        String expectedWelcome = "Welcome UserId";
        String actual = localisation.getWelcome(AvailableLanguage.EN, "UserId");
        assertEquals(expectedWelcome, actual);
    }

    @Test
    void testGetShortDescriptionDe() {
        String expectedShortDescription = "Nur noch ein Schritt<br />Bitte verifiziere deine Email";
        String actual = localisation.getShortDescription(AvailableLanguage.DE);
        assertEquals(expectedShortDescription, actual);
    }

    @Test
    void testGetShortDescriptionEn() {
        String expectedShortDescription = "Only one step left <br />Please verify your mail";
        String actual = localisation.getShortDescription(AvailableLanguage.EN);
        assertEquals(expectedShortDescription, actual);
    }

    @Test
    void testGetFooterDe() {
        String expectedFooter = "Du erhälst diese Mail, da du dich für einen Zugang zum " + MailLocalisation.NETWORK_LINK + " registriert hast. " +
                "Solltest du dies nicht gewesen sein, bitte ignoriere diese Mail.";
        String actual = localisation.getFooter(AvailableLanguage.DE);
        assertEquals(expectedFooter, actual);
    }

    @Test
    void testGetFooterEn() {
        String expectedFooter = "You are receiving this mail because you signed up for access to the " + MailLocalisation.NETWORK_LINK + ". " +
                "Please ignore this mail, if it was not you.";
        String actual = localisation.getFooter(AvailableLanguage.EN);
        assertEquals(expectedFooter, actual);
    }


}

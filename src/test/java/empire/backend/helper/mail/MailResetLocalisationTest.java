/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.InitMocks;
import empire.backend.model.enums.AvailableLanguage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class MailResetLocalisationTest extends InitMocks {
    @Mock
    MailResetLocalisation localisation;

    @BeforeEach
    void initMock() {
        when(localisation.getSubject(any())).thenCallRealMethod();
        when(localisation.getTitle(any())).thenCallRealMethod();
        when(localisation.getButton(any())).thenCallRealMethod();
        when(localisation.getReason(any())).thenCallRealMethod();
        when(localisation.getWelcome(any(), any())).thenCallRealMethod();
        when(localisation.getShortDescription(any())).thenCallRealMethod();
        when(localisation.getFooter(any())).thenCallRealMethod();
    }

    @Test
    void testCreateInstance() {
        MailResetLocalisation actual = new MailResetLocalisation();
        assertNotNull(actual);
    }

    @Test
    void testGetSubjectDe() {
        String expectedSubject = "Deine angefragte Passwortzurücksetzung";
        String actual = localisation.getSubject(AvailableLanguage.DE);
        assertEquals(expectedSubject, actual);
    }

    @Test
    void testGetSubjectEn() {
        String expectedSubject = "Your requested password reset";
        String actual = localisation.getSubject(AvailableLanguage.EN);
        assertEquals(expectedSubject, actual);
    }

    @Test
    void testGetTitleDe() {
        String expectedTitle = "Passwort zurücksetzen - The Empire";
        String actual = localisation.getTitle(AvailableLanguage.DE);
        assertEquals(expectedTitle, actual);
    }

    @Test
    void testGetTitleEn() {
        String expectedTitle = "Password reset - The Empire";
        String actual = localisation.getTitle(AvailableLanguage.EN);
        assertEquals(expectedTitle, actual);
    }

    @Test
    void testGetButtonDe() {
        String expectedButton = "Passwort zurücksetzen";
        String actual = localisation.getButton(AvailableLanguage.DE);
        assertEquals(expectedButton, actual);
    }

    @Test
    void testGetButtonEn() {
        String expectedButton = "Reset password";
        String actual = localisation.getButton(AvailableLanguage.EN);
        assertEquals(expectedButton, actual);
    }

    @Test
    void testGetReasonDe() {
        String expectedReason = "Passwortzurücksetzung";
        String actual = localisation.getReason(AvailableLanguage.DE);
        assertEquals(expectedReason, actual);
    }

    @Test
    void testGetReasonEn() {
        String expectedReason = "Password reset";
        String actual = localisation.getReason(AvailableLanguage.EN);
        assertEquals(expectedReason, actual);
    }

    @Test
    void testGetWelcomeDe() {
        String expectedWelcome = "Hallo UserId";
        String actual = localisation.getWelcome(AvailableLanguage.DE, "UserId");
        assertEquals(expectedWelcome, actual);
    }

    @Test
    void testGetWelcomeEn() {
        String expectedWelcome = "Hello UserId";
        String actual = localisation.getWelcome(AvailableLanguage.EN, "UserId");
        assertEquals(expectedWelcome, actual);
    }

    @Test
    void testGetShortDescriptionDe() {
        String expectedShortDescription = "Du hast eine Passwortzurücksetzung angefordert<br />Wenn du das nicht warst, ignoriere diese Email bitte";
        String actual = localisation.getShortDescription(AvailableLanguage.DE);
        assertEquals(expectedShortDescription, actual);
    }

    @Test
    void testGetShortDescriptionEn() {
        String expectedShortDescription = "You requested a password reset<br />If not please ignore this mail";
        String actual = localisation.getShortDescription(AvailableLanguage.EN);
        assertEquals(expectedShortDescription, actual);
    }

    @Test
    void testGetFooterDe() {
        String expectedFooter = "Du erhälst diese Mail, da du einen Account für das " + MailLocalisation.NETWORK_LINK + " besitzt.";
        String actual = localisation.getFooter(AvailableLanguage.DE);
        assertEquals(expectedFooter, actual);
    }

    @Test
    void testGetFooterEn() {
        String expectedFooter = "You are receiving this mail because you have an account for the " + MailLocalisation.NETWORK_LINK + ".";
        String actual = localisation.getFooter(AvailableLanguage.EN);
        assertEquals(expectedFooter, actual);
    }


}

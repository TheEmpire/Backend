package empire.backend.helper.mail;

import org.junit.jupiter.api.Test;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MailSessionHelperTest {
    @Test
    void testPrepareMail() {
        Session actual = new MailSessionHelper().prepareMail("test", "password");
        PasswordAuthentication auth = actual.requestPasswordAuthentication(null, 0, null, null, null);
        assertEquals("true", actual.getProperty("mail.smtp.auth"));
        assertEquals("true", actual.getProperty("mail.smtp.starttls.enable"));
        assertEquals("cloud-in.space", actual.getProperty("mail.smtp.host"));
        assertEquals("25", actual.getProperty("mail.smtp.port"));
        assertEquals("test", auth.getUserName());
        assertEquals("password", auth.getPassword());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.InitMocks;
import empire.backend.model.enums.AvailableLanguage;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

class MailLocalisationTest extends InitMocks {
    @Mock
    MailLocalisation localisation;

    @Test
    void testGetErrorDescriptionDe() {
        when(localisation.getErrorDescription(any(), any())).thenCallRealMethod();
        String expectedLink = "testLink";
        String expectedResult = "Wenn der obige Link nicht funktioniert, kopiere den folgenden in deinen Browser <a href=\"testLink\" " +
                "style=\"color: #CACACA\">testLink</a>";
        String actual = localisation.getErrorDescription(AvailableLanguage.DE, expectedLink);
        assertTrue(actual.contains(expectedLink));
        assertEquals(expectedResult, actual);
    }

    @Test
    void testGetErrorDescriptionEn() {
        when(localisation.getErrorDescription(any(), any())).thenCallRealMethod();
        String expectedLink = "testLink";
        String expectedResult = "If the above link does not work, please copy the following link into your browser: <a href=\"testLink\" " +
                "style=\"color: #CACACA\">testLink</a>";
        String actual = localisation.getErrorDescription(AvailableLanguage.EN, expectedLink);
        assertTrue(actual.contains(expectedLink));
        assertEquals(expectedResult, actual);
    }

    @Test
    void testGetCopyrightDe() {
        when(localisation.getCopyright(any(), anyInt())).thenCallRealMethod();
        String expectedResult = "2019 Alle Rechte vorbehalten";
        String actual = localisation.getCopyright(AvailableLanguage.DE, 2019);
        assertEquals(expectedResult, actual);
    }

    @Test
    void testGetCopyrightEn() {
        when(localisation.getCopyright(any(), anyInt())).thenCallRealMethod();
        String expectedResult = "2019 All rights reserved";
        String actual = localisation.getCopyright(AvailableLanguage.EN, 2019);
        assertEquals(expectedResult, actual);
    }

    @Test
    void testGetLinkForNetwork() {
        String expectedResult = "<a href=\"https://empire.cloud-in.space\" style=\"color: #CACACA\">Empire Network</a>";

        assertEquals(expectedResult, MailLocalisation.NETWORK_LINK);
    }


}

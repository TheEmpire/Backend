/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.helper.mail;

import empire.backend.InitMocks;
import empire.backend.helper.HashHelper;
import empire.backend.helper.ReadResource;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.AvailableLanguage;
import empire.backend.model.enums.ServerMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class MailHelperTest extends InitMocks {
    @Mock
    HashHelper encryption;
    @Mock
    MailValidationLocalisation validationLocalisation;
    @Mock
    MailResetLocalisation resetLocalisation;
    @Mock
    ResponseBuilder builder;
    @Mock
    ReadResource file;
    @Mock
    MailSessionHelper sessionHelper;
    @InjectMocks
    MailHelper helper;

    @BeforeEach
    void doPostConstruct() {
        setPrivateVar("user", "user", helper);
        setPrivateVar("pwd", "password", helper);
        when(encryption.hash("mail@mail.com", "userId")).thenReturn("hashed");
        when(file.readResource("mail_template.html")).thenReturn("lang LANG, reason REASON, welcome WELCOME, " +
                "title TITLE, button_action BUTTON_ACTION, button_name BUTTON_NAME, short_description SHORT_DESCRIPTION, " +
                "error_description ERROR_DESCRIPTION, footer FOOTER, copyright COPYRIGHT");
        helper.doPostConstruct();
        setupValidationLocalisation(validationLocalisation);
        setupResetLocalisation(resetLocalisation);
    }

    private void setupValidationLocalisation(MailValidationLocalisation validationLocalisation) {
        when(validationLocalisation.getButton(any())).thenReturn("buttonValidation");
        when(validationLocalisation.getFooter(any())).thenReturn("footerValidation");
        when(validationLocalisation.getReason(any())).thenReturn("reasonValidation");
        when(validationLocalisation.getShortDescription(any())).thenReturn("shortValidation");
        when(validationLocalisation.getSubject(any())).thenReturn("subjectValidation");
        when(validationLocalisation.getTitle(any())).thenReturn("titleValidation");
        when(validationLocalisation.getWelcome(any(), eq("userId"))).thenReturn("welcomeValidation");
        when(validationLocalisation.getCopyright(any(), anyInt())).thenReturn("copyrightValidation");
        when(validationLocalisation.getErrorDescription(any(), anyString())).thenReturn("errorValidation");
    }

    private void setupResetLocalisation(MailResetLocalisation resetLocalisation) {
        when(resetLocalisation.getButton(any())).thenReturn("buttonReset");
        when(resetLocalisation.getFooter(any())).thenReturn("footerReset");
        when(resetLocalisation.getReason(any())).thenReturn("reasonReset");
        when(resetLocalisation.getShortDescription(any())).thenReturn("shortReset");
        when(resetLocalisation.getSubject(any())).thenReturn("subjectReset");
        when(resetLocalisation.getTitle(any())).thenReturn("titleReset");
        when(resetLocalisation.getWelcome(any(), eq("userId"))).thenReturn("welcomeReset");
        when(resetLocalisation.getCopyright(any(), anyInt())).thenReturn("copyrightReset");
        when(resetLocalisation.getErrorDescription(any(), anyString())).thenReturn("errorReset");
    }

    @Test
    void testPostConstruct() {
        verify(file).readResource("mail_template.html");
        verify(sessionHelper).prepareMail("user", "pwd");
    }

    @Test
    void testGetValidationMail() {
        String expectedMail = "mail@mail.com";
        String expectedImperialId = "userId";
        AvailableLanguage language = AvailableLanguage.EN;
        String[] actual = helper.getValidationMail(expectedMail, expectedImperialId, language);
        String expectedTemplate = "lang EN, reason reasonValidation, welcome welcomeValidation, title titleValidation, button_action https://empire.cloud-in.space/en/validation?id=userId&hash=hashed, button_name buttonValidation, short_description shortValidation, error_description errorValidation, footer footerValidation, copyright copyrightValidation";
        String expectedSubject = "subjectValidation";
        assertEquals(2, actual.length);
        assertEquals(expectedTemplate, actual[0]);
        assertEquals(expectedSubject, actual[1]);
    }

    @Test
    void testGetValidationMailMailNull() {
        String expectedImperialId = "userId";
        AvailableLanguage language = AvailableLanguage.EN;
        assertEquals(0, helper.getValidationMail(null, expectedImperialId, language).length);
    }

    @Test
    void testGetValidationMailImperialIdNull() {
        String expectedMail = "mail@mail.com";
        AvailableLanguage language = AvailableLanguage.EN;
        assertEquals(0, helper.getValidationMail(expectedMail, null, language).length);
    }

    @Test
    void testGetValidationMailAvailableLanguageNull() {
        String expectedMail = "mail@mail.com";
        String expectedImperialId = "userId";
        assertEquals(0, helper.getValidationMail(expectedMail, expectedImperialId, null).length);
    }

    @Test
    void testGetValidationMailTemplateNull() {
        helper.template = null;
        String expectedMail = "mail@mail.com";
        String expectedImperialId = "userId";
        AvailableLanguage language = AvailableLanguage.EN;
        String[] actual = helper.getValidationMail(expectedMail, expectedImperialId, language);
        String expectedSubject = "subjectValidation";
        assertEquals(2, actual.length);
        assertNull(actual[0]);
        assertEquals(expectedSubject, actual[1]);
    }

    @Test
    void testGetResetMail() {
        String expectedImperialId = "userId";
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        AvailableLanguage language = AvailableLanguage.EN;
        String[] actual = helper.getResetMail(expectedImperialId, expectedHashOne, expectedHashTwo, language);
        String expectedTemplate = "lang EN, reason reasonReset, welcome welcomeReset, title titleReset, button_action https://empire.cloud-in.space/en/resetPwd?id=userId&hashOne=hash1&hashTwo=hash2, button_name buttonReset, short_description shortReset, error_description errorReset, footer footerReset, copyright copyrightReset";
        String expectedSubject = "subjectReset";
        assertEquals(2, actual.length);
        assertEquals(expectedTemplate, actual[0]);
        assertEquals(expectedSubject, actual[1]);
    }

    @Test
    void testGetResetMailImperialIdNull() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        AvailableLanguage language = AvailableLanguage.EN;
        assertEquals(0, helper.getResetMail(null, expectedHashOne, expectedHashTwo, language).length);
    }

    @Test
    void testGetResetMailHashOneNull() {
        String expectedImperialId = "userId";
        String expectedHashTwo = "hash2";
        AvailableLanguage language = AvailableLanguage.EN;
        assertEquals(0, helper.getResetMail(expectedImperialId, null, expectedHashTwo, language).length);
    }

    @Test
    void testGetResetMailHashTwoNull() {
        String expectedImperialId = "userId";
        String expectedHashOne = "hash1";
        AvailableLanguage language = AvailableLanguage.EN;
        assertEquals(0, helper.getResetMail(expectedImperialId, expectedHashOne, null, language).length);
    }

    @Test
    void testGetResetMailAvailableLanguageNull() {
        String expectedImperialId = "userId";
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        assertEquals(0, helper.getResetMail(expectedImperialId, expectedHashOne, expectedHashTwo, null).length);
    }

    @Test
    void testGetResetMailTemplateNull() {
        helper.template = null;
        String expectedImperialId = "userId";
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        AvailableLanguage language = AvailableLanguage.EN;
        String[] actual = helper.getResetMail(expectedImperialId, expectedHashOne, expectedHashTwo, language);
        String expectedSubject = "subjectReset";
        assertEquals(2, actual.length);
        assertNull(actual[0]);
        assertEquals(expectedSubject, actual[1]);
    }

    @Test
    void testSendMailWithExceptionThrown() throws MessagingException {
        final boolean[] sendCalled = {false};
        Message msg = mock(Message.class);
        helper.messageSender = message -> sendCalled[0] = true;
        helper.messageSupplier = () -> msg;
        doThrow(MessagingException.class).when(msg).setRecipients(eq(Message.RecipientType.TO), any());
        helper.sendMail("content", "mail@mail.com", "subject", ServerMessage.MAIL_VALIDATED);

        verify(builder).create(ServerMessage.MAIL_SEND_ERROR);
        assertFalse(sendCalled[0]);
    }

    @Test
    void testSendMailWithExcpetionInsideLambda() {
        final boolean[] sendCalled = {false};
        Message msg = mock(Message.class);
        helper.messageSender = message -> sendCalled[0] = true;
        helper.messageSupplier = () -> msg;
        helper.internalException = true;
        helper.sendMail("content", "mail@mail.com", "subject", ServerMessage.MAIL_VALIDATED);

        verify(builder).create(ServerMessage.MAIL_SEND_ERROR);
        assertTrue(sendCalled[0]);
    }

    @Test
    void testSendMail() throws MessagingException {
        final boolean[] sendCalled = {false};
        Message msg = mock(Message.class);
        helper.messageSender = message -> sendCalled[0] = true;
        helper.messageSupplier = () -> msg;
        helper.sendMail("content", "mail@mail.com", "subject", ServerMessage.MAIL_VALIDATED);

        verify(msg).setContent(eq("content"), anyString());
        verify(msg).setSubject("subject");
        verify(msg).setRecipients(Message.RecipientType.TO, InternetAddress.parse("mail@mail.com"));
        verify(builder).create(ServerMessage.MAIL_VALIDATED);
        assertTrue(sendCalled[0]);
    }

    @Test
    void testSupplierLambda() {
        assertNotNull(helper.messageSupplier.get());
        assertFalse(helper.internalException);
    }

    @Test
    void testSupplierLambdaException() {
        setPrivateVar(";", "user", helper);
        helper.doPostConstruct();

        assertNotNull(helper.messageSupplier.get());
        assertTrue(helper.internalException);
    }

    @Test
    void testSenderLambdaException() {
        Message message = mock(Message.class);

        helper.messageSender.accept(message);

        assertTrue(helper.internalException);
    }

}

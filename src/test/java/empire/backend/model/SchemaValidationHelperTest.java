/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SchemaValidationHelperTest {
    @Test
    void testIsValidArrayInterfaceNull() {
        MockInterface[] mock = null;
        assertFalse(SchemaValidationHelper.isValidArray(mock));
    }

    @Test
    void testIsValidArrayInterfaceEmpty() {
        MockInterface[] mock = new MockInterface[] {};
        assertFalse(SchemaValidationHelper.isValidArray(mock));
    }


    @Test
    void testIsValidArrayInterfaceAllTrue() {
        MockInterface[] mock = new MockInterface[] {new MockInterface(true), new MockInterface(true)};
        assertTrue(SchemaValidationHelper.isValidArray(mock));
    }

    @Test
    void testIsValidArrayInterfaceAllFalse() {
        MockInterface[] mock = new MockInterface[] {new MockInterface(false), new MockInterface(false)};
        assertFalse(SchemaValidationHelper.isValidArray(mock));
    }

    @Test
    void testIsValidArrayInterfaceMixedNull() {
        MockInterface[] mock = new MockInterface[] { new MockInterface(true), null};
        assertFalse(SchemaValidationHelper.isValidArray(mock));
    }

    @Test
    void testIsValidArrayInterfaceMixedFalse() {
        MockInterface[] mock = new MockInterface[] { new MockInterface(true), new MockInterface(false)};
        assertFalse(SchemaValidationHelper.isValidArray(mock));
    }

    @Test
    void testIsValidArrayStringNull() {
        String[] array = null;
        assertFalse(SchemaValidationHelper.isValidArray(array));
    }

    @Test
    void testIsValidArrayStringEmpty() {
        String[] array = new String[] {};
        assertFalse(SchemaValidationHelper.isValidArray(array));
    }

    @Test
    void testIsValidArrayStringAllNonEmpty() {
        String[] array = new String[] {"1", "test"};
        assertTrue(SchemaValidationHelper.isValidArray(array));
    }

    @Test
    void testIsValidArrayStringAllEmpty() {
        String[] array = new String[] {"", ""};
        assertFalse(SchemaValidationHelper.isValidArray(array));
    }

    @Test
    void testIsValidArrayStringMixedNull() {
        String[] array = new String[] {"test", null};
        assertFalse(SchemaValidationHelper.isValidArray(array));
    }

    @Test
    void testIsValidArrayStringMixedEmpty() {
        String[] array = new String[] {"", "test"};
        assertFalse(SchemaValidationHelper.isValidArray(array));
    }

    @Test
    void testIsValidOptionalArrayStringNull() {
        String[] array = null;
        assertTrue(SchemaValidationHelper.isValidOptionalArray(array));
    }

    @Test
    void testIsValidOptionalArrayStringEmpty() {
        String[] array = new String[] {};
        assertFalse(SchemaValidationHelper.isValidOptionalArray(array));
    }

    @Test
    void testIsValidOptionalArrayStringAllNonEmpty() {
        String[] array = new String[] {"1", "test"};
        assertTrue(SchemaValidationHelper.isValidOptionalArray(array));
    }

    @Test
    void testIsValidOptionalArrayStringAllEmpty() {
        String[] array = new String[] {"", ""};
        assertFalse(SchemaValidationHelper.isValidOptionalArray(array));
    }

    @Test
    void testIsValidOptionalArrayStringMixedNull() {
        String[] array = new String[] {"test", null};
        assertFalse(SchemaValidationHelper.isValidOptionalArray(array));
    }

    @Test
    void testIsValidOptionalArrayStringMixedEmpty() {
        String[] array = new String[] {"", "test"};
        assertFalse(SchemaValidationHelper.isValidOptionalArray(array));
    }

    @Test
    void testIsValidRequiredStringNull() {
        String str = null;
        assertFalse(SchemaValidationHelper.isValidRequired(str));
    }

    @Test
    void testIsValidRequiredStringEmpty() {
        String str = "";
        assertFalse(SchemaValidationHelper.isValidRequired(str));
    }

    @Test
    void testIsValidRequiredStringNonEmpty() {
        String str = "test";
        assertTrue(SchemaValidationHelper.isValidRequired(str));
    }

    @Test
    void testIsValidRequiredIntegerNull() {
        Integer integer = null;
        assertFalse(SchemaValidationHelper.isValidRequired(integer));
    }

    @Test
    void testIsValidRequiredIntegerSmallerZero() {
        Integer integer = - new Random().nextInt(Integer.MAX_VALUE) - 1;
        assertFalse(SchemaValidationHelper.isValidRequired(integer));
    }

    @Test
    void testIsValidRequiredIntegerGreaterOrEqualZero() {
        Integer integer = new Random().nextInt(Integer.MAX_VALUE);
        assertTrue(SchemaValidationHelper.isValidRequired(integer));
    }

    @Test
    void testIsValidRequiredBooleanNull() {
        Boolean bool = null;
        assertFalse(SchemaValidationHelper.isValidRequired(bool));
    }

    @Test
    void testIsValidRequiredBooleanNotNull() {
        Boolean bool = new Random().nextBoolean();
        assertTrue(SchemaValidationHelper.isValidRequired(bool));
    }

    @Test
    void testIsValidOptionalStringNull() {
        String str = null;
        assertTrue(SchemaValidationHelper.isValidOptional(str));
    }

    @Test
    void testIsValidOptionalStringEmpty() {
        String str = "";
        assertFalse(SchemaValidationHelper.isValidOptional(str));
    }

    @Test
    void testIsValidOptionalStringNonEmpty() {
        String str = "test";
        assertTrue(SchemaValidationHelper.isValidOptional(str));
    }

    @Test
    void testIsValidOptionalIntegerNull() {
        Integer integer = null;
        assertTrue(SchemaValidationHelper.isValidOptional(integer));
    }

    @Test
    void testIsValidOptionalIntegerSmallerZero() {
        Integer integer = - new Random().nextInt(Integer.MAX_VALUE) - 1;
        assertFalse(SchemaValidationHelper.isValidRequired(integer));
    }

    @Test
    void testIsValidOptionalIntegerGreaterOrEqualZero() {
        Integer integer = new Random().nextInt(Integer.MAX_VALUE);
        assertTrue(SchemaValidationHelper.isValidRequired(integer));
    }

    private class MockInterface implements SchemaValidation {
        private boolean result;

        MockInterface(boolean result) {
            this.result = result;
        }

        @Override
        public boolean isValid() {
            return result;
        }
    }
}

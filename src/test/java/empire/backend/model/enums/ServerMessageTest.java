/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.enums;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ServerMessageTest {
    @Test
    void testEnumHasResetMailSent() {
        assertNotNull(ServerMessage.valueOf("RESET_MAIL_SENT"));
    }

    @Test
    void testEnumHasValidationMailSent() {
        assertNotNull(ServerMessage.valueOf("VALIDATION_MAIL_SENT"));
    }

    @Test
    void testEnumHasPasswordUpdated() {
        assertNotNull(ServerMessage.valueOf("PASSWORD_UPDATED"));
    }

    @Test
    void testEnumHasLoginSuccess() {
        assertNotNull(ServerMessage.valueOf("LOGIN_SUCCESS"));
    }

    @Test
    void testEnumHasRenewSuccess() {
        assertNotNull(ServerMessage.valueOf("RENEW_SUCCESS"));
    }

    @Test
    void testEnumHasUserUpdated() {
        assertNotNull(ServerMessage.valueOf("USER_UPDATED"));
    }

    @Test
    void testEnumHasUserDeleted() {
        assertNotNull(ServerMessage.valueOf("USER_DELETED"));
    }

    @Test
    void testEnumHasLanguageAdded() {
        assertNotNull(ServerMessage.valueOf("LANGUAGE_ADDED"));
    }

    @Test
    void testEnumHasLanguageUpdated() {
        assertNotNull(ServerMessage.valueOf("LANGUAGE_UPDATED"));
    }

    @Test
    void testEnumHasLanguageRemoved() {
        assertNotNull(ServerMessage.valueOf("LANGUAGE_REMOVED"));
    }

    @Test
    void testEnumHasMailValidated() {
        assertNotNull(ServerMessage.valueOf("MAIL_VALIDATED"));
    }

    @Test
    void testEnumHasAllInfoUpdated() {
        assertNotNull(ServerMessage.valueOf("ALL_INFO_UPDATED"));
    }

    @Test
    void testEnumHasAllInfoDeleted() {
        assertNotNull(ServerMessage.valueOf("ALL_INFO_DELETED"));
    }

    @Test
    void testEnumHasInfoAdded() {
        assertNotNull(ServerMessage.valueOf("INFO_ADDED"));
    }

    @Test
    void testEnumHasInfoUpdated() {
        assertNotNull(ServerMessage.valueOf("INFO_UPDATED"));
    }

    @Test
    void testEnumHasInfoDeleted() {
        assertNotNull(ServerMessage.valueOf("INFO_DELETED"));
    }

    @Test
    void testEnumHasTypeUpdated() {
        assertNotNull(ServerMessage.valueOf("TYPE_UPDATED"));
    }

    @Test
    void testEnumHasTypeDeleted() {
        assertNotNull(ServerMessage.valueOf("TYPE_DELETED"));
    }

    @Test
    void testEnumHasOnlineUpdated() {
        assertNotNull(ServerMessage.valueOf("ONLINE_UPDATED"));
    }

    @Test
    void testEnumHasRankUpdated() {
        assertNotNull(ServerMessage.valueOf("RANK_UPDATED"));
    }

    @Test
    void testEnumHasRankDeleted() {
        assertNotNull(ServerMessage.valueOf("RANK_DELETED"));
    }

    @Test
    void testEnumHasRankAssigned() {
        assertNotNull(ServerMessage.valueOf("RANK_ASSIGNED"));
    }

    @Test
    void testEnumHasRankReset() {
        assertNotNull(ServerMessage.valueOf("RANK_RESET"));
    }

    @Test
    void testEnumHasRoleUpdated() {
        assertNotNull(ServerMessage.valueOf("ROLE_UPDATED"));
    }

    @Test
    void testEnumHasRoleDeleted() {
        assertNotNull(ServerMessage.valueOf("ROLE_DELETED"));
    }

    @Test
    void testEnumHasRoleAssigned() {
        assertNotNull(ServerMessage.valueOf("ROLE_ASSIGNED"));
    }

    @Test
    void testEnumHasRoleRemoved() {
        assertNotNull(ServerMessage.valueOf("ROLE_REMOVED"));
    }

    @Test
    void testEnumHasAllRolesRemoved() {
        assertNotNull(ServerMessage.valueOf("ALL_ROLES_REMOVED"));
    }

    @Test
    void testEnumHasRegistered() {
        assertNotNull(ServerMessage.valueOf("REGISTERED"));
    }

    @Test
    void testEnumHasNotRegistered() {
        assertNotNull(ServerMessage.valueOf("NOT_REGISTERED"));
    }

    @Test
    void testEnumHasManagementRoleUpdated() {
        assertNotNull(ServerMessage.valueOf("MANAGEMENT_ROLE_UPDATED"));
    }

    @Test
    void testEnumHasSquadUpdated() {
        assertNotNull(ServerMessage.valueOf("SQUAD_UPDATED"));
    }

    @Test
    void testEnumHasSquadDeleted() {
        assertNotNull(ServerMessage.valueOf("SQUAD_DELETED"));
    }

    @Test
    void testEnumHasSquadAssigned() {
        assertNotNull(ServerMessage.valueOf("SQUAD_ASSIGNED"));
    }

    @Test
    void testEnumHasSquadRemoved() {
        assertNotNull(ServerMessage.valueOf("SQUAD_REMOVED"));
    }

    @Test
    void testEnumHasSquadPositionUpdated() {
        assertNotNull(ServerMessage.valueOf("SQUAD_POSITION_UPDATED"));
    }

    @Test
    void testEnumHasCorporationUpdated() {
        assertNotNull(ServerMessage.valueOf("CORPORATION_UPDATED"));
    }

    @Test
    void testEnumHasCorporationDeleted() {
        assertNotNull(ServerMessage.valueOf("CORPORATION_DELETED"));
    }

    @Test
    void testEnumHasLegateUpdated() {
        assertNotNull(ServerMessage.valueOf("LEGATE_UPDATED"));
    }

    @Test
    void testEnumHasLegateDeleted() {
        assertNotNull(ServerMessage.valueOf("LEGATE_DELETED"));
    }

    @Test
    void testEnumHasUserCreated() {
        assertNotNull(ServerMessage.valueOf("USER_CREATED"));
    }

    @Test
    void testEnumHasTypeCreated() {
        assertNotNull(ServerMessage.valueOf("TYPE_CREATED"));
    }

    @Test
    void testEnumHasRankCreated() {
        assertNotNull(ServerMessage.valueOf("RANK_CREATED"));
    }

    @Test
    void testEnumHasRoleCreated() {
        assertNotNull(ServerMessage.valueOf("ROLE_CREATED"));
    }

    @Test
    void testEnumHasSquadCreated() {
        assertNotNull(ServerMessage.valueOf("SQUAD_CREATED"));
    }

    @Test
    void testEnumHasCorporationCreated() {
        assertNotNull(ServerMessage.valueOf("CORPORATION_CREATED"));
    }

    @Test
    void testEnumHasLegateCreated() {
        assertNotNull(ServerMessage.valueOf("LEGATE_CREATED"));
    }

    @Test
    void testEnumHasQueryParamWrong() {
        assertNotNull(ServerMessage.valueOf("QUERY_PARAM_WRONG"));
    }

    @Test
    void testEnumHasPathParamWrong() {
        assertNotNull(ServerMessage.valueOf("PATH_PARAM_WRONG"));
    }

    @Test
    void testEnumHasRequestBodyWrong() {
        assertNotNull(ServerMessage.valueOf("REQUEST_BODY_WRONG"));
    }

    @Test
    void testEnumHasInvalidEntity() {
        assertNotNull(ServerMessage.valueOf("INVALID_ENTITY"));
    }

    @Test
    void testEnumHasInvalidTableAlias() {
        assertNotNull(ServerMessage.valueOf("INVALID_TABLE_ALIAS"));
    }

    @Test
    void testEnumHasAccessDenied() {
        assertNotNull(ServerMessage.valueOf("ACCESS_DENIED"));
    }

    @Test
    void testEnumHasJWTMissing() {
        assertNotNull(ServerMessage.valueOf("JWT_MISSING"));
    }

    @Test
    void testEnumHasJWTInvalid() {
        assertNotNull(ServerMessage.valueOf("JWT_INVALID"));
    }

    @Test
    void testEnumHasRoleMissing() {
        assertNotNull(ServerMessage.valueOf("ROLE_MISSING"));
    }

    @Test
    void testEnumHasAuthIncorrect() {
        assertNotNull(ServerMessage.valueOf("AUTH_INCORRECT"));
    }

    @Test
    void testEnumHasImageNotFound() {
        assertNotNull(ServerMessage.valueOf("IMAGE_NOT_FOUND"));
    }

    @Test
    void testEnumHasUserNotFound() {
        assertNotNull(ServerMessage.valueOf("USER_NOT_FOUND"));
    }

    @Test
    void testEnumHasTypeNotFound() {
        assertNotNull(ServerMessage.valueOf("TYPE_NOT_FOUND"));
    }

    @Test
    void testEnumHasRankNotFound() {
        assertNotNull(ServerMessage.valueOf("RANK_NOT_FOUND"));
    }

    @Test
    void testEnumHasRoleNotFound() {
        assertNotNull(ServerMessage.valueOf("ROLE_NOT_FOUND"));
    }

    @Test
    void testEnumHasSquadNotFound() {
        assertNotNull(ServerMessage.valueOf("SQUAD_NOT_FOUND"));
    }

    @Test
    void testEnumHasCorporationNotFound() {
        assertNotNull(ServerMessage.valueOf("CORPORATION_NOT_FOUND"));
    }

    @Test
    void testEnumHasLegateNotFound() {
        assertNotNull(ServerMessage.valueOf("LEGATE_NOT_FOUND"));
    }

    @Test
    void testEnumHasUserOrLanguageNotFound() {
        assertNotNull(ServerMessage.valueOf("USER_OR_LANGUAGE_NOT_FOUND"));
    }

    @Test
    void testEnumHasInfoTypeAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("INFO_TYPE_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasUserAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("USER_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasRankAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("RANK_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasRoleAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("ROLE_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasSquadAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("SQUAD_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasCorporationAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("CORPORATION_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasUserAlreadyLegate() {
        assertNotNull(ServerMessage.valueOf("USER_ALREADY_LEGATE"));
    }

    @Test
    void testEnumHasLanguageAlreadyExists() {
        assertNotNull(ServerMessage.valueOf("LANGUAGE_ALREADY_EXISTS"));
    }

    @Test
    void testEnumHasDatabaseError() {
        assertNotNull(ServerMessage.valueOf("DATABASE_ERROR"));
    }

    @Test
    void testEnumHasMailSendError() {
        assertNotNull(ServerMessage.valueOf("MAIL_SEND_ERROR"));
    }

    @Test
    void testEnumHasColorConversionError() {
        assertNotNull(ServerMessage.valueOf("COLOR_CONVERSION_ERROR"));
    }

    @Test
    void testEnumHasJWTError() {
        assertNotNull(ServerMessage.valueOf("JWT_ERROR"));
    }

    @Test
    void testEnumHasSeventySevenMembers() {
        assertEquals(78, ServerMessage.values().length);
    }

    @Test
    void testEnumGetCode200() {
        assertEquals(200, ServerMessage.RESET_MAIL_SENT.getCode());
        assertEquals(200, ServerMessage.VALIDATION_MAIL_SENT.getCode());
        assertEquals(200, ServerMessage.PASSWORD_UPDATED.getCode());
        assertEquals(200, ServerMessage.LOGIN_SUCCESS.getCode());
        assertEquals(200, ServerMessage.RENEW_SUCCESS.getCode());
        assertEquals(200, ServerMessage.USER_UPDATED.getCode());
        assertEquals(200, ServerMessage.USER_DELETED.getCode());
        assertEquals(200, ServerMessage.LANGUAGE_ADDED.getCode());
        assertEquals(200, ServerMessage.LANGUAGE_UPDATED.getCode());
        assertEquals(200, ServerMessage.LANGUAGE_REMOVED.getCode());
        assertEquals(200, ServerMessage.MAIL_VALIDATED.getCode());
        assertEquals(200, ServerMessage.ALL_INFO_UPDATED.getCode());
        assertEquals(200, ServerMessage.ALL_INFO_DELETED.getCode());
        assertEquals(200, ServerMessage.INFO_ADDED.getCode());
        assertEquals(200, ServerMessage.INFO_UPDATED.getCode());
        assertEquals(200, ServerMessage.INFO_DELETED.getCode());
        assertEquals(200, ServerMessage.TYPE_UPDATED.getCode());
        assertEquals(200, ServerMessage.TYPE_DELETED.getCode());
        assertEquals(200, ServerMessage.ONLINE_UPDATED.getCode());
        assertEquals(200, ServerMessage.RANK_UPDATED.getCode());
        assertEquals(200, ServerMessage.RANK_DELETED.getCode());
        assertEquals(200, ServerMessage.RANK_ASSIGNED.getCode());
        assertEquals(200, ServerMessage.RANK_RESET.getCode());
        assertEquals(200, ServerMessage.ROLE_UPDATED.getCode());
        assertEquals(200, ServerMessage.ROLE_DELETED.getCode());
        assertEquals(200, ServerMessage.ROLE_ASSIGNED.getCode());
        assertEquals(200, ServerMessage.ROLE_REMOVED.getCode());
        assertEquals(200, ServerMessage.ALL_ROLES_REMOVED.getCode());
        assertEquals(200, ServerMessage.REGISTERED.getCode());
        assertEquals(200, ServerMessage.NOT_REGISTERED.getCode());
        assertEquals(200, ServerMessage.MANAGEMENT_ROLE_UPDATED.getCode());
        assertEquals(200, ServerMessage.SQUAD_UPDATED.getCode());
        assertEquals(200, ServerMessage.SQUAD_DELETED.getCode());
        assertEquals(200, ServerMessage.SQUAD_ASSIGNED.getCode());
        assertEquals(200, ServerMessage.SQUAD_REMOVED.getCode());
        assertEquals(200, ServerMessage.SQUAD_POSITION_UPDATED.getCode());
        assertEquals(200, ServerMessage.CORPORATION_UPDATED.getCode());
        assertEquals(200, ServerMessage.CORPORATION_DELETED.getCode());
        assertEquals(200, ServerMessage.LEGATE_UPDATED.getCode());
        assertEquals(200, ServerMessage.LEGATE_DELETED.getCode());
    }

    @Test
    void testEnumGetCode201() {
        assertEquals(201, ServerMessage.USER_CREATED.getCode());
        assertEquals(201, ServerMessage.TYPE_CREATED.getCode());
        assertEquals(201, ServerMessage.RANK_CREATED.getCode());
        assertEquals(201, ServerMessage.ROLE_CREATED.getCode());
        assertEquals(201, ServerMessage.SQUAD_CREATED.getCode());
        assertEquals(201, ServerMessage.CORPORATION_CREATED.getCode());
        assertEquals(201, ServerMessage.LEGATE_CREATED.getCode());
    }

    @Test
    void testEnumGetCode400() {
        assertEquals(400, ServerMessage.QUERY_PARAM_WRONG.getCode());
        assertEquals(400, ServerMessage.PATH_PARAM_WRONG.getCode());
        assertEquals(400, ServerMessage.REQUEST_BODY_WRONG.getCode());
        assertEquals(400, ServerMessage.INVALID_ENTITY.getCode());
        assertEquals(400, ServerMessage.INVALID_TABLE_ALIAS.getCode());
    }

    @Test
    void testEnumGetCode401() {
        assertEquals(401, ServerMessage.ACCESS_DENIED.getCode());
        assertEquals(401, ServerMessage.JWT_MISSING.getCode());
        assertEquals(401, ServerMessage.JWT_INVALID.getCode());

    }

    @Test
    void testEnumGetCode403() {
        assertEquals(403, ServerMessage.ROLE_MISSING.getCode());
        assertEquals(403, ServerMessage.AUTH_INCORRECT.getCode());
    }

    @Test
    void testEnumGetCode404() {
        assertEquals(404, ServerMessage.IMAGE_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.USER_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.TYPE_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.RANK_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.ROLE_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.SQUAD_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.CORPORATION_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.LEGATE_NOT_FOUND.getCode());
        assertEquals(404, ServerMessage.USER_OR_LANGUAGE_NOT_FOUND.getCode());
    }

    @Test
    void testEnumGetCode409() {
        assertEquals(409, ServerMessage.INFO_TYPE_ALREADY_EXISTS.getCode());
        assertEquals(409, ServerMessage.USER_ALREADY_EXISTS.getCode());
        assertEquals(409, ServerMessage.RANK_ALREADY_EXISTS.getCode());
        assertEquals(409, ServerMessage.ROLE_ALREADY_EXISTS.getCode());
        assertEquals(409, ServerMessage.SQUAD_ALREADY_EXISTS.getCode());
        assertEquals(409, ServerMessage.CORPORATION_ALREADY_EXISTS.getCode());
        assertEquals(409, ServerMessage.USER_ALREADY_LEGATE.getCode());
        assertEquals(409, ServerMessage.LANGUAGE_ALREADY_EXISTS.getCode());
    }

    @Test
    void testEnumGetCode500() {
        assertEquals(500, ServerMessage.DATABASE_ERROR.getCode());
        assertEquals(500, ServerMessage.MAIL_SEND_ERROR.getCode());
        assertEquals(500, ServerMessage.COLOR_CONVERSION_ERROR.getCode());
        assertEquals(500, ServerMessage.JWT_ERROR.getCode());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.role;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleListTest {
    @Test
    void testGetterAndConstructor() {
        String[] expectedArray = {"role1", "role2"};
        RoleList list = new RoleList(expectedArray);

        assertArrayEquals(expectedArray, list.getRoles());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        RoleList list = new RoleList(null);

        assertNull(list.getRoles());
    }

    @Test
    void testIsValid() {
        String[] expectedArray = {"role1", "role2"};
        RoleList list = new RoleList(expectedArray);

        assertTrue(list.isValid());
    }

    @Test
    void testIsValidRolesNull() {
        RoleList list = new RoleList(null);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidRolesEmpty() {
        String[] expectedArray = {};
        RoleList list = new RoleList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidRoleEmpty() {
        String[] expectedArray = {"role1", ""};
        RoleList list = new RoleList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidRoleNull() {
        String[] expectedArray = {"role1", null};
        RoleList list = new RoleList(expectedArray);

        assertFalse(list.isValid());
    }
}

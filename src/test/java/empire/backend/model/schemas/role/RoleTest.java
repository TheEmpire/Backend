/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.role;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleTest {
    private Boolean expectedWrite;
    private Boolean expectedRead;
    private Boolean expectedCreate;
    private Boolean expectedDelete;

    @BeforeEach
    void setup() {
        expectedWrite = expectedCreate = true;
        expectedRead = expectedDelete = false;
    }

    @Test
    void testGetterAndConstructor() {
        Role role = new Role(expectedRead, expectedWrite, expectedCreate, expectedDelete);

        assertEquals(expectedRead, role.getRead());
        assertEquals(expectedWrite, role.getWrite());
        assertEquals(expectedCreate, role.getCreate());
        assertEquals(expectedDelete, role.getDelete());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        Role role = new Role(null, null, null, null);

        assertNull(role.getRead());
        assertNull(role.getWrite());
        assertNull(role.getCreate());
        assertNull(role.getDelete());
    }

    @Test
    void testIsValid() {
        Role role = new Role(expectedRead, expectedWrite, expectedCreate, expectedDelete);

        assertTrue(role.isValid());
    }

    @Test
    void testIsValidAllNull() {
        Role role = new Role(null, null, null, null);

        assertFalse(role.isValid());
    }

    @Test
    void testIsValidReadNull() {
        Role role = new Role(null, expectedWrite, expectedCreate, expectedDelete);

        assertFalse(role.isValid());
    }

    @Test
    void testIsValidWriteNull() {
        Role role = new Role(expectedRead, null, expectedCreate, expectedDelete);

        assertFalse(role.isValid());
    }

    @Test
    void testIsValidCreateNull() {
        Role role = new Role(expectedRead, expectedWrite, null, expectedDelete);

        assertFalse(role.isValid());
    }

    @Test
    void testIsValidDeleteNull() {
        Role role = new Role(expectedRead, expectedWrite, expectedCreate, null);

        assertFalse(role.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class RegistrationTest {
    @Test
    void testGetterAndConstructor() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertEquals(expectedImperialId, registration.getImperialId());
        assertEquals(expectedPassword, registration.getPassword());
        assertEquals(expectedName, registration.getName());
        assertEquals(expectedMail, registration.getMail());
        assertEquals(expectedTimezone, registration.getTimezone());
        assertArrayEquals(expectedLanguage, registration.getLanguages());
        assertEquals(expectedDateOfBirth, registration.getDateOfBirth());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        Registration registration = new Registration(null, null, null, null, null, null, null);

        assertNull(registration.getImperialId());
        assertNull(registration.getPassword());
        assertNull(registration.getName());
        assertNull(registration.getMail());
        assertNull(registration.getTimezone());
        assertNull(registration.getLanguages());
        assertNull(registration.getDateOfBirth());
    }

    @Test
    void testIsValid() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertTrue(registration.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(null, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidPasswordNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, null, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidMailNull() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, null, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidTimezoneNull() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, null, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidLanguagesNull() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, null, expectedDateOfBirth);

        assertTrue(registration.isValid());
    }

    @Test
    void testIsValidNameNull() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, null, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertTrue(registration.isValid());
    }

    @Test
    void testIsValidDateOfBirthNull() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, null);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedImperialId = "";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidMailEmpty() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidMailNotAMail() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "notA.mail";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidMailWithInjectionMail() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "mail@mail.com\nCC:mail2@mail.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidTimezoneEmpty() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);
        assertFalse(registration.isValid());
    }


    @Test
    void testIsValidLanguagesEmpty() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidLanguageEmpty() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", ""};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }

    @Test
    void testIsValidLanguageNull() {
        String expectedImperialId = "userId";
        String expectedPassword = "password";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1, Germany";
        String[] expectedLanguage = {"language1", null};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        Registration registration = new Registration(expectedImperialId, expectedPassword, expectedMail, expectedName, expectedTimezone, expectedLanguage, expectedDateOfBirth);

        assertFalse(registration.isValid());
    }
}

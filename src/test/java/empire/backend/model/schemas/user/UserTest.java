/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import empire.backend.model.enums.UserState;
import empire.backend.model.internal.UserShare;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class UserTest {
    static Stream<Arguments> userShares() {
        return Stream.of(
                arguments(new UserShare(true, true, true)),
                arguments(new UserShare(true, false, true)),
                arguments(new UserShare(true, true, false)),
                arguments(new UserShare(false, true, true)),
                arguments(new UserShare(false, false, true)),
                arguments(new UserShare(false, true, false)),
                arguments(new UserShare(false, false, false))
        );
    }

    @Test
    void testGetterAndConstructor() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertEquals(expectedImperialId, user.getImperialId());
        assertEquals(expectedName, user.getName());
        assertEquals(expectedMail, user.getMail());
        assertEquals(expectedTimezone, user.getTimezone());
        assertArrayEquals(expectedLanguage, user.getLanguage());
        assertEquals(expectedDateOfBirth, user.getDateOfBirth());
        assertEquals(expectedDateOfAccess, user.getDateOfAccess());
        assertEquals(expectedState, user.getState());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        User user = new User(null, null, null, null, null, null, null, null);

        assertNull(user.getImperialId());
        assertNull(user.getName());
        assertNull(user.getMail());
        assertNull(user.getTimezone());
        assertNull(user.getLanguage());
        assertNull(user.getDateOfBirth());
        assertNull(user.getDateOfAccess());
        assertNull(user.getState());
    }

    @Test
    void testIsValid() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(null, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidTimezoneNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, null, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidDateOfAccessNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, null, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidStateNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, null);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidLanguagesNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, null, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidMailNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, null, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidNameNull() {
        String expectedImperialId = "userId";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, null, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidDateOfBirthNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, null, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedImperialId = "";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidMailEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidMailNotAMail() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "notA.mail";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidTimezoneEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidLanguagesEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidLanguageEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", ""};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidLanguageNull() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", null};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @ParameterizedTest
    @MethodSource("userShares")
    void testResetNotShared(UserShare share) {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        User user = new User(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);
        user = user.resetNotShared(share);

        assertEquals(expectedImperialId, user.getImperialId());
        assertEquals(expectedTimezone, user.getTimezone());
        assertEquals(expectedDateOfAccess, user.getDateOfAccess());
        assertEquals(expectedLanguage, user.getLanguage());
        assertEquals(expectedState, user.getState());
        if (share.isMail())
            assertEquals(expectedMail, user.getMail());
        else
            assertNull(user.getMail());
        if (share.isName())
            assertEquals(expectedName, user.getName());
        else
            assertNull(user.getName());
        if (share.isDateOfBirth())
            assertEquals(expectedDateOfBirth, user.getDateOfBirth());
        else
            assertNull(user.getDateOfBirth());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import empire.backend.model.enums.UserState;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class UserUpdateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertEquals(expectedImperialId, user.getImperialId());
        assertEquals(expectedName, user.getName());
        assertEquals(expectedMail, user.getMail());
        assertEquals(expectedTimezone, user.getTimezone());
        assertEquals(expectedDateOfBirth, user.getDateOfBirth());
        assertEquals(expectedState, user.getState());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        UserUpdate user = new UserUpdate(null, null, null, null, null, null);

        assertNull(user.getImperialId());
        assertNull(user.getName());
        assertNull(user.getMail());
        assertNull(user.getTimezone());
        assertNull(user.getDateOfBirth());
        assertNull(user.getState());
    }

    @Test
    void testIsValid() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(null, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidOnlyImperialId() {
        String expectedImperialId = "userId";
        UserUpdate user = new UserUpdate(expectedImperialId, null, null, null, null, null);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidImperialIdAndName() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, null, null, null, null);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdAndMail() {
        String expectedImperialId = "userId";
        String expectedMail = "test@test.com";
        UserUpdate user = new UserUpdate(expectedImperialId, null, expectedMail, null, null, null);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdAndInvalidMail() {
        String expectedImperialId = "userId";
        String expectedMail = "invalid.mail";
        UserUpdate user = new UserUpdate(expectedImperialId, null, expectedMail, null, null, null);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidImperialIdAndTimezone() {
        String expectedImperialId = "userId";
        String expectedTimezone = "UTC +1, Germany";
        UserUpdate user = new UserUpdate(expectedImperialId, null, null, expectedTimezone, null, null);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdAndState() {
        String expectedImperialId = "userId";
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, null, null, null, null, expectedState);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdAndDateOfBirth() {
        String expectedImperialId = "userId";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserUpdate user = new UserUpdate(expectedImperialId, null, null, null, expectedDateOfBirth, null);

        assertTrue(user.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedImperialId = "";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidMailEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "";
        String expectedTimezone = "UTC +1";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testIsValidTimezoneEmpty() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "";
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        UserState expectedState = UserState.ACTIVE;
        UserUpdate user = new UserUpdate(expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedDateOfBirth, expectedState);

        assertFalse(user.isValid());
    }

    @Test
    void testGetSetAttributesNonNull() {
        UserUpdate user = new UserUpdate("userId", "name", "mail", "timezone", LocalDate.now(), UserState.ACTIVE);

        Map<String, Object> map = user.getSetAttributes();
        assertEquals(5, map.size());
    }

    @Test
    void testGetSetAttributesSomeNull() {
        UserUpdate user = new UserUpdate("userId", "name", null, "timezone", LocalDate.now(), null);

        Map<String, Object> map = user.getSetAttributes();
        assertEquals(3, map.size());
    }

    @Test
    void testGetSetAttributesAllNull() {
        UserUpdate user = new UserUpdate("userId", null, null, null, null, null);

        Map<String, Object> map = user.getSetAttributes();
        assertEquals(0, map.size());
    }

}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.user;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserListTest {
    @Test
    void testGetterAndConstructor() {
        String[] expectedArray = {"user1", "user2"};
        UserList list = new UserList(expectedArray);

        assertArrayEquals(expectedArray, list.getUsers());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        UserList list = new UserList(null);

        assertNull(list.getUsers());
    }

    @Test
    void testIsValid() {
        String[] expectedArray = {"user1", "user2"};
        UserList list = new UserList(expectedArray);

        assertTrue(list.isValid());
    }

    @Test
    void testIsValidUsersNull() {
        UserList list = new UserList(null);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidUsersEmpty() {
        String[] expectedArray = {};
        UserList list = new UserList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidUserEmpty() {
        String[] expectedArray = {"user1", ""};
        UserList list = new UserList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidUserNull() {
        String[] expectedArray = {"user1", null};
        UserList list = new UserList(expectedArray);

        assertFalse(list.isValid());
    }
}

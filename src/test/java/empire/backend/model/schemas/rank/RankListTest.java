/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.rank;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RankListTest {
    @Test
    void testGetterAndConstructor() {
        String[] expectedArray = {"rank1", "rank2"};
        RankList list = new RankList(expectedArray);

        assertArrayEquals(expectedArray, list.getRanks());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        RankList list = new RankList(null);

        assertNull(list.getRanks());
    }

    @Test
    void testIsValid() {
        String[] expectedArray = {"rank1", "rank2"};
        RankList list = new RankList(expectedArray);

        assertTrue(list.isValid());
    }

    @Test
    void testIsValidRanksNull() {
        RankList list = new RankList(null);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidRanksEmpty() {
        String[] expectedArray = {};
        RankList list = new RankList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidRankEmpty() {
        String[] expectedArray = {"rank1", ""};
        RankList list = new RankList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidRankNull() {
        String[] expectedArray = {"rank1", null};
        RankList list = new RankList(expectedArray);

        assertFalse(list.isValid());
    }
}

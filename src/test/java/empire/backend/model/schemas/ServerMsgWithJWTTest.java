/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServerMsgWithJWTTest {
    @Test
    void testGetterAndConstructor() {
        String expectedMsg = "msg";
        String expectedToken = "val";
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(expectedMsg, expectedToken);

        assertEquals(expectedMsg, msgWithJWT.getMsg());
        assertEquals(expectedToken, msgWithJWT.getToken());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(null, null);

        assertNull(msgWithJWT.getMsg());
        assertNull(msgWithJWT.getToken());
    }

    @Test
    void testIsValid() {
        String expectedMsg = "msg";
        String expectedToken = "token";
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(expectedMsg, expectedToken);

        assertTrue(msgWithJWT.isValid());
    }

    @Test
    void testIsValidMsgNull() {
        String expectedToken = "token";
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(null, expectedToken);

        assertFalse(msgWithJWT.isValid());
    }

    @Test
    void testIsValidTokenNull() {
        String expectedMsg = "msg";
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(expectedMsg, null);

        assertFalse(msgWithJWT.isValid());
    }

    @Test
    void testIsValidMsgEmpty() {
        String expectedMsg = "";
        String expectedToken = "token";
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(expectedMsg, expectedToken);

        assertFalse(msgWithJWT.isValid());
    }

    @Test
    void testIsValidTokenEmpty() {
        String expectedMsg = "msg";
        String expectedToken = "";
        ServerMsgWithJWT msgWithJWT = new ServerMsgWithJWT(expectedMsg, expectedToken);

        assertFalse(msgWithJWT.isValid());
    }
}

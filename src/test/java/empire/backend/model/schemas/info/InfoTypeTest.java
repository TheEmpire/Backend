/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.info;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfoTypeTest {
    @Test
    void testGetterAndConstructor() {
        String expectedName = "name";
        Integer expectedId = 1;
        Integer expectedRole = 3;
        InfoType info = new InfoType(expectedId, expectedName, expectedRole);

        assertEquals(expectedId, info.getId());
        assertEquals(expectedName, info.getName());
        assertEquals(expectedRole, info.getRole());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        InfoType info = new InfoType(null, null, null);

        assertNull(info.getId());
        assertNull(info.getName());
        assertNull(info.getRole());
    }

    @Test
    void testIsValid() {
        String expectedName = "name";
        Integer expectedId = 1;
        Integer expectedRole = 3;
        InfoType info = new InfoType(expectedId, expectedName, expectedRole);

        assertTrue(info.isValid());
    }

    @Test
    void testIsValidIdNull() {
        String expectedName = "name";
        Integer expectedRole = 3;
        InfoType info = new InfoType(null, expectedName, expectedRole);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidNameNull() {
        Integer expectedId = 1;
        Integer expectedRole = 3;
        InfoType info = new InfoType(expectedId, null, expectedRole);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidRoleNull() {
        Integer expectedId = 1;
        String expectedName = "name";
        InfoType info = new InfoType(expectedId, expectedName, null);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedName = "";
        Integer expectedId = 1;
        Integer expectedRole = 3;
        InfoType info = new InfoType(expectedId, expectedName, expectedRole);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidIdSmallerZero() {
        String expectedName = "name";
        Integer expectedId = -1;
        Integer expectedRole = 3;
        InfoType info = new InfoType(expectedId, expectedName, expectedRole);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidRoleSmallerZero() {
        String expectedName = "name";
        Integer expectedId = 1;
        Integer expectedRole = -1;
        InfoType info = new InfoType(expectedId, expectedName, expectedRole);

        assertFalse(info.isValid());
    }
}

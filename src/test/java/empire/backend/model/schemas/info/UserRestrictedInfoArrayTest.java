/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.info;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserRestrictedInfoArrayTest {
    @Test
    void testGetterAndConstructor() {
        UserRestrictedInfo[] expectedArray = {new UserRestrictedInfo("type1", "val1"), new UserRestrictedInfo("type2", "val2")};
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(expectedArray);

        assertArrayEquals(expectedArray, array.getInformation());
    }

    @Test
    void testGetterAndConstructorWithPartialNull() {
        UserRestrictedInfo[] expectedArray = {null, new UserRestrictedInfo("type2", "val2")};
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(expectedArray);

        assertArrayEquals(expectedArray, array.getInformation());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(null);

        assertNull(array.getInformation());
    }

    @Test
    void testIsValid() {
        UserRestrictedInfo[] expectedArray = {new UserRestrictedInfo("type1", "val1"), new UserRestrictedInfo("type2", "val2")};
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(expectedArray);

        assertTrue(array.isValid());
    }

    @Test
    void testIsValidValuesNull() {
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(null);

        assertFalse(array.isValid());
    }

    @Test
    void testIsValidValuesPartialNull() {
        UserRestrictedInfo[] expectedArray = {null, new UserRestrictedInfo("type2", "val2")};
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(expectedArray);

        assertFalse(array.isValid());
    }

    @Test
    void testIsValidValuesEmpty() {
        UserRestrictedInfo[] expectedArray = {};
        UserRestrictedInfoArray array = new UserRestrictedInfoArray(expectedArray);

        assertFalse(array.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.info;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfoTypeArrayTest {
    @Test
    void testGetterAndConstructor() {
        InfoType[] expectedArray = {new InfoType(1, "name1", 2), new InfoType(2, "name2", 3)};
        InfoTypeArray array = new InfoTypeArray(expectedArray);

        assertArrayEquals(expectedArray, array.getTypes());
    }

    @Test
    void testGetterAndConstructorWithPartialNull() {
        InfoType[] expectedArray = {null, new InfoType(2, "name2", 1)};
        InfoTypeArray array = new InfoTypeArray(expectedArray);

        assertArrayEquals(expectedArray, array.getTypes());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        InfoTypeArray array = new InfoTypeArray(null);

        assertNull(array.getTypes());
    }

    @Test
    void testIsValid() {
        InfoType[] expectedArray = {new InfoType(1, "name1", 2), new InfoType(2, "name2", 3)};
        InfoTypeArray array = new InfoTypeArray(expectedArray);

        assertTrue(array.isValid());
    }

    @Test
    void testIsValidValuesNull() {
        InfoTypeArray array = new InfoTypeArray(null);

        assertFalse(array.isValid());
    }

    @Test
    void testIsValidValuesPartialNull() {
        InfoType[] expectedArray = {null, new InfoType(2, "name2", 1)};
        InfoTypeArray array = new InfoTypeArray(expectedArray);

        assertFalse(array.isValid());
    }

    @Test
    void testIsValidValuesEmpty() {
        InfoType[] expectedArray = {};
        InfoTypeArray array = new InfoTypeArray(expectedArray);

        assertFalse(array.isValid());
    }
}

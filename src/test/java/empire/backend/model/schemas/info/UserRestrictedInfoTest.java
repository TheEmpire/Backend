/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.info;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserRestrictedInfoTest {
    @Test
    void testGetterAndConstructor() {
        String expectedType = "type";
        String expectedValue = "val";
        UserRestrictedInfo info = new UserRestrictedInfo(expectedType, expectedValue);

        assertEquals(expectedType, info.getType());
        assertEquals(expectedValue, info.getValue());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        UserRestrictedInfo info = new UserRestrictedInfo(null, null);

        assertNull(info.getType());
        assertNull(info.getValue());
    }

    @Test
    void testIsValid() {
        String expectedType = "type";
        String expectedValue = "val";
        UserRestrictedInfo info = new UserRestrictedInfo(expectedType, expectedValue);

        assertTrue(info.isValid());
    }

    @Test
    void testIsValidKeyNull() {
        String expectedValue = "val";
        UserRestrictedInfo info = new UserRestrictedInfo(null, expectedValue);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidValueNull() {
        String expectedType = "type";
        UserRestrictedInfo info = new UserRestrictedInfo(expectedType, null);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidKeyEmpty() {
        String expectedType = "";
        String expectedValue = "val";
        UserRestrictedInfo info = new UserRestrictedInfo(expectedType, expectedValue);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidValueEmpty() {
        String expectedType = "type";
        String expectedValue = "";
        UserRestrictedInfo info = new UserRestrictedInfo(expectedType, expectedValue);

        assertFalse(info.isValid());
    }
}

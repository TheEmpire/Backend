/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.info;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InfoTypeCreateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedType = "type";
        Integer expectedRole = 1;
        InfoTypeCreate info = new InfoTypeCreate(expectedType, expectedRole);

        assertEquals(expectedType, info.getType());
        assertEquals(expectedRole, info.getRole());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        InfoTypeCreate info = new InfoTypeCreate(null, null);

        assertNull(info.getType());
        assertNull(info.getRole());
    }

    @Test
    void testIsValid() {
        String expectedType = "type";
        Integer expectedRole = 1;
        InfoTypeCreate info = new InfoTypeCreate(expectedType, expectedRole);

        assertTrue(info.isValid());
    }

    @Test
    void testIsValidTypeNull() {
        Integer expectedRole = 1;
        InfoTypeCreate info = new InfoTypeCreate(null, expectedRole);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidRoleNull() {
        String expectedType = "type";
        InfoTypeCreate info = new InfoTypeCreate(expectedType, null);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidTypeEmpty() {
        String expectedType = "";
        Integer expectedRole = 1;
        InfoTypeCreate info = new InfoTypeCreate(expectedType, expectedRole);

        assertFalse(info.isValid());
    }

    @Test
    void testIsValidRoleSmallerZeroEmpty() {
        String expectedType = "type";
        Integer expectedRole = -1;
        InfoTypeCreate info = new InfoTypeCreate(expectedType, expectedRole);

        assertFalse(info.isValid());
    }
}

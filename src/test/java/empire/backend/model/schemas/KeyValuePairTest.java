/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KeyValuePairTest {
    @Test
    void testGetterAndConstructor() {
        String expectedKey = "key";
        String expectedValue = "val";
        KeyValuePair pair = new KeyValuePair(expectedKey, expectedValue);

        assertEquals(expectedKey, pair.getKey());
        assertEquals(expectedValue, pair.getValue());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        KeyValuePair pair = new KeyValuePair(null, null);

        assertNull(pair.getKey());
        assertNull(pair.getValue());
    }

    @Test
    void testIsValid() {
        String expectedKey = "key";
        String expectedValue = "val";
        KeyValuePair pair = new KeyValuePair(expectedKey, expectedValue);

        assertTrue(pair.isValid());
    }

    @Test
    void testIsValidKeyNull() {
        String expectedValue = "val";
        KeyValuePair pair = new KeyValuePair(null, expectedValue);

        assertFalse(pair.isValid());
    }

    @Test
    void testIsValidValueNull() {
        String expectedKey = "key";
        KeyValuePair pair = new KeyValuePair(expectedKey, null);

        assertFalse(pair.isValid());
    }

    @Test
    void testIsValidKeyEmpty() {
        String expectedKey = "";
        String expectedValue = "val";
        KeyValuePair pair = new KeyValuePair(expectedKey, expectedValue);

        assertFalse(pair.isValid());
    }

    @Test
    void testIsValidValueEmpty() {
        String expectedKey = "key";
        String expectedValue = "";
        KeyValuePair pair = new KeyValuePair(expectedKey, expectedValue);

        assertFalse(pair.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordUpdateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedId = "userId";
        String expectedOldPwd = "oldPassword";
        String expectedNewPwd = "newPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, expectedNewPwd, expectedId);

        assertEquals(expectedId, passwordUpdate.getImperialId());
        assertEquals(expectedOldPwd, passwordUpdate.getOldPassword());
        assertEquals(expectedNewPwd, passwordUpdate.getNewPassword());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        PasswordUpdate passwordUpdate = new PasswordUpdate(null, null, null);

        assertNull(passwordUpdate.getImperialId());
        assertNull(passwordUpdate.getOldPassword());
        assertNull(passwordUpdate.getNewPassword());
    }

    @Test
    void testIsValid() {
        String expectedId = "userId";
        String expectedOldPwd = "oldPassword";
        String expectedNewPwd = "newPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, expectedNewPwd, expectedId);

        assertTrue(passwordUpdate.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        String expectedOldPwd = "oldPassword";
        String expectedNewPwd = "newPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, expectedNewPwd, null);

        assertFalse(passwordUpdate.isValid());
    }

    @Test
    void testIsValidOldPasswordNull() {
        String expectedId = "userId";
        String expectedNewPwd = "newPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(null, expectedNewPwd, expectedId);

        assertFalse(passwordUpdate.isValid());
    }

    @Test
    void testIsValidNewPasswordNull() {
        String expectedId = "userId";
        String expectedOldPwd = "oldPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, null, expectedId);

        assertFalse(passwordUpdate.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedId = "";
        String expectedOldPwd = "oldPassword";
        String expectedNewPwd = "newPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, expectedNewPwd, expectedId);

        assertFalse(passwordUpdate.isValid());
    }

    @Test
    void testIsValidOldPasswordEmpty() {
        String expectedId = "userId";
        String expectedOldPwd = "";
        String expectedNewPwd = "newPassword";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, expectedNewPwd, expectedId);

        assertFalse(passwordUpdate.isValid());
    }

    @Test
    void testIsValidNewPasswordEmpty() {
        String expectedId = "userId";
        String expectedOldPwd = "oldPassword";
        String expectedNewPwd = "";
        PasswordUpdate passwordUpdate = new PasswordUpdate(expectedOldPwd, expectedNewPwd, expectedId);

        assertFalse(passwordUpdate.isValid());
    }
}

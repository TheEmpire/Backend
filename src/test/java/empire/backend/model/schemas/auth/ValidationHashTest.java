/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidationHashTest {
    @Test
    void testGetterAndConstructor() {
        String expectedHash = "hash";
        ValidationHash validationHash = new ValidationHash(expectedHash);

        assertEquals(expectedHash, validationHash.getHash());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        ValidationHash validationHash = new ValidationHash(null);

        assertNull(validationHash.getHash());
    }

    @Test
    void testIsValid() {
        String expectedHash = "hash";
        ValidationHash validationHash = new ValidationHash(expectedHash);

        assertTrue(validationHash.isValid());
    }

    @Test
    void testIsValidHashNull() {
        ValidationHash validationHash = new ValidationHash(null);

        assertFalse(validationHash.isValid());
    }

    @Test
    void testIsValidPasswordEmpty() {
        String expectedHash = "";
        ValidationHash validationHash = new ValidationHash(expectedHash);

        assertFalse(validationHash.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordResetTest {    @Test
void testGetterAndConstructor() {
    String expectedHashOne = "hash1";
    String expectedHashTwo = "hash2";
    String expectedId = "userId";
    String expectedPwd = "password";
    PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, expectedPwd);

    assertEquals(expectedId, passwordReset.getImperialId());
    assertEquals(expectedPwd, passwordReset.getNewPassword());
    assertEquals(expectedHashOne, passwordReset.getHashOne());
    assertEquals(expectedHashTwo, passwordReset.getHashTwo());
}

    @Test
    void testGetterAndConstructorWithNull() {
        PasswordReset passwordReset = new PasswordReset(null, null, null, null);

        assertNull(passwordReset.getImperialId());
        assertNull(passwordReset.getNewPassword());
        assertNull(passwordReset.getHashOne());
        assertNull(passwordReset.getHashTwo());
    }

    @Test
    void testIsValid() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        String expectedId = "userId";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, expectedPwd);

        assertTrue(passwordReset.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, null, expectedPwd);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidPasswordNull() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        String expectedId = "userId";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, null);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidHashOneNull() {
        String expectedHashTwo = "hash2";
        String expectedId = "userId";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(null, expectedHashTwo, expectedId, expectedPwd);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidHashTwoNull() {
        String expectedHashOne = "hash1";
        String expectedId = "userId";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, null, expectedId, expectedPwd);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        String expectedId = "";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, expectedPwd);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidPasswordEmpty() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "hash2";
        String expectedId = "userId";
        String expectedPwd = "";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, expectedPwd);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidHashOneEmpty() {
        String expectedHashOne = "";
        String expectedHashTwo = "hash2";
        String expectedId = "userId";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, expectedPwd);

        assertFalse(passwordReset.isValid());
    }

    @Test
    void testIsValidHashTwoEmpty() {
        String expectedHashOne = "hash1";
        String expectedHashTwo = "";
        String expectedId = "userId";
        String expectedPwd = "password";
        PasswordReset passwordReset = new PasswordReset(expectedHashOne, expectedHashTwo, expectedId, expectedPwd);

        assertFalse(passwordReset.isValid());
    }
}

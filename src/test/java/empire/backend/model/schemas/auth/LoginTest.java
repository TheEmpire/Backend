/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LoginTest {
    @Test
    void testGetterAndConstructor() {
        String expectedId = "userId";
        String expectedPwd = "password";
        Login login = new Login(expectedId, expectedPwd);

        assertEquals(expectedId, login.getImperialId());
        assertEquals(expectedPwd, login.getPassword());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        Login login = new Login(null, null);

        assertNull(login.getImperialId());
        assertNull(login.getPassword());
    }

    @Test
    void testIsValid() {
        String expectedId = "userId";
        String expectedPwd = "password";
        Login login = new Login(expectedId, expectedPwd);

        assertTrue(login.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        String expectedPwd = "password";
        Login login = new Login(null, expectedPwd);

        assertFalse(login.isValid());
    }

    @Test
    void testIsValidPasswordNull() {
        String expectedId = "userId";
        Login login = new Login(expectedId, null);

        assertFalse(login.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedId = "";
        String expectedPwd = "password";
        Login login = new Login(expectedId, expectedPwd);

        assertFalse(login.isValid());
    }

    @Test
    void testIsValidPasswordEmpty() {
        String expectedId = "userId";
        String expectedPwd = "";
        Login login = new Login(expectedId, expectedPwd);

        assertFalse(login.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.squad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquadListTest {
    @Test
    void testGetterAndConstructor() {
        String[] expectedArray = {"squad1", "squad2"};
        SquadList list = new SquadList(expectedArray);

        assertArrayEquals(expectedArray, list.getSquads());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        SquadList list = new SquadList(null);

        assertNull(list.getSquads());
    }

    @Test
    void testIsValid() {
        String[] expectedArray = {"squad1", "squad2"};
        SquadList list = new SquadList(expectedArray);

        assertTrue(list.isValid());
    }

    @Test
    void testIsValidSquadsNull() {
        SquadList list = new SquadList(null);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidSquadsEmpty() {
        String[] expectedArray = {};
        SquadList list = new SquadList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidSquadEmpty() {
        String[] expectedArray = {"squad1", ""};
        SquadList list = new SquadList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidSquadNull() {
        String[] expectedArray = {"squad1", null};
        SquadList list = new SquadList(expectedArray);

        assertFalse(list.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.squad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquadPositionTest {
    @Test
    void testGetterAndConstructor() {
        Integer expectedPos = 1;
        SquadPosition pos = new SquadPosition(expectedPos);

        assertEquals(expectedPos, pos.getPosition());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        SquadPosition pos = new SquadPosition(null);

        assertNull(pos.getPosition());
    }

    @Test
    void testIsValid() {
        Integer expectedPos = 1;
        SquadPosition pos = new SquadPosition(expectedPos);

        assertTrue(pos.isValid());
    }

    @Test
    void testIsValidPositionNull() {
        SquadPosition pos = new SquadPosition(null);

        assertFalse(pos.isValid());
    }

    @Test
    void testIsValidIdSmallerZeroEmpty() {
        Integer expectedPos = -1;
        SquadPosition pos = new SquadPosition(expectedPos);

        assertFalse(pos.isValid());
    }
}

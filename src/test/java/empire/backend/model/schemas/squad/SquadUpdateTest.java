/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.squad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquadUpdateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        SquadUpdate squad = new SquadUpdate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertEquals(expectedMaxMembers, squad.getMaxMembers());
        assertEquals(expectedLeadingSquad, squad.getLeadingSquad());
        assertEquals(expectedName, squad.getName());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        SquadUpdate squad = new SquadUpdate(null, null, null);

        assertNull(squad.getMaxMembers());
        assertNull(squad.getLeadingSquad());
        assertNull(squad.getName());
    }

    @Test
    void testIsValidAll() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        SquadUpdate squad = new SquadUpdate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidLeadingSquad() {
        String expectedLeadingSquad = "leader";
        SquadUpdate squad = new SquadUpdate(null, null, expectedLeadingSquad);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidMaxMembers() {
        Integer expectedMaxMembers = 5;
        SquadUpdate squad = new SquadUpdate(null, expectedMaxMembers, null);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidName() {
        String expectedName = "name";
        SquadUpdate squad = new SquadUpdate(expectedName, null, null);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedName = "";
        SquadUpdate squad = new SquadUpdate(expectedName, null, null);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidLeadingSquadEmpty() {
        String expectedLeadingSquad = "";
        SquadUpdate squad = new SquadUpdate(null, null, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidMaxMembersSmallerZeroEmpty() {
        Integer expectedMaxMembers = -1;
        SquadUpdate squad = new SquadUpdate(null, expectedMaxMembers, null);

        assertFalse(squad.isValid());
    }
}

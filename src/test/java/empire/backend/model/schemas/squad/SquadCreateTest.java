/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.squad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquadCreateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        SquadCreate squad = new SquadCreate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertEquals(expectedMaxMembers, squad.getMaxMembers());
        assertEquals(expectedLeadingSquad, squad.getLeadingSquad());
        assertEquals(expectedName, squad.getName());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        SquadCreate squad = new SquadCreate(null, null, null);

        assertNull(squad.getMaxMembers());
        assertNull(squad.getLeadingSquad());
        assertNull(squad.getName());
    }

    @Test
    void testIsValid() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        SquadCreate squad = new SquadCreate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidNameNull() {
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        SquadCreate squad = new SquadCreate(null, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidLeadingSquadNull() {
        String expectedName = "name";
        Integer expectedMaxMembers = 5;
        SquadCreate squad = new SquadCreate(expectedName, expectedMaxMembers, null);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidMaxMembersNull() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        SquadCreate squad = new SquadCreate(expectedName, null, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedName = "";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        SquadCreate squad = new SquadCreate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidLeadingSquadEmpty() {
        String expectedName = "name";
        String expectedLeadingSquad = "";
        Integer expectedMaxMembers = 5;
        SquadCreate squad = new SquadCreate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidMaxMembersSmallerZeroEmpty() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = -1;
        SquadCreate squad = new SquadCreate(expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }
}

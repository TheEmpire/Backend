/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.squad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquadTest {
    @Test
    void testGetterAndConstructor() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedId = 1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertEquals(expectedId, squad.getId());
        assertEquals(expectedMaxMembers, squad.getMaxMembers());
        assertEquals(expectedLeadingSquad, squad.getLeadingSquad());
        assertEquals(expectedName, squad.getName());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        Squad squad = new Squad(null, null, null, null);

        assertNull(squad.getId());
        assertNull(squad.getMaxMembers());
        assertNull(squad.getLeadingSquad());
        assertNull(squad.getName());
    }

    @Test
    void testIsValid() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedId = 1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidNameNull() {
        String expectedLeadingSquad = "leader";
        Integer expectedId = 1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, null, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidIdNull() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(null, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidLeadingSquadNull() {
        String expectedName = "name";
        Integer expectedId = 1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, null);

        assertTrue(squad.isValid());
    }

    @Test
    void testIsValidMaxMembersNull() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedId = 1;
        Squad squad = new Squad(expectedId, expectedName, null, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedName = "";
        String expectedLeadingSquad = "leader";
        Integer expectedId = 1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidLeadingSquadEmpty() {
        String expectedName = "name";
        String expectedLeadingSquad = "";
        Integer expectedId = 1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }


    @Test
    void testIsValidIdSmallerZeroEmpty() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedId = -1;
        Integer expectedMaxMembers = 5;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }

    @Test
    void testIsValidMaxMembersSmallerZeroEmpty() {
        String expectedName = "name";
        String expectedLeadingSquad = "leader";
        Integer expectedId = 1;
        Integer expectedMaxMembers = -1;
        Squad squad = new Squad(expectedId, expectedName, expectedMaxMembers, expectedLeadingSquad);

        assertFalse(squad.isValid());
    }
}

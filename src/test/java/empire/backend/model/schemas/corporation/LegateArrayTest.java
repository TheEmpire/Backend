/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LegateArrayTest {
    @Test
    void testGetterAndConstructor() {
        Legate[] expectedArray = {new Legate(1, 2, "user1"), new Legate(3, 4, "user2")};
        LegateArray array = new LegateArray(expectedArray);

        assertArrayEquals(expectedArray, array.getLegates());
    }

    @Test
    void testGetterAndConstructorWithPartialNull() {
        Legate[] expectedArray = {null, new Legate(3, 4, "user2")};
        LegateArray array = new LegateArray(expectedArray);

        assertArrayEquals(expectedArray, array.getLegates());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        LegateArray array = new LegateArray(null);

        assertNull(array.getLegates());
    }

    @Test
    void testIsValid() {
        Legate[] expectedArray = {new Legate(1, 2, "user1"), new Legate(3, 4, "user2")};
        LegateArray array = new LegateArray(expectedArray);

        assertTrue(array.isValid());
    }

    @Test
    void testIsValidValuesNull() {
        LegateArray array = new LegateArray(null);

        assertFalse(array.isValid());
    }

    @Test
    void testIsValidValuesPartialNull() {
        Legate[] expectedArray = {null, new Legate(3, 4, "user2")};
        LegateArray array = new LegateArray(expectedArray);

        assertFalse(array.isValid());
    }

    @Test
    void testIsValidValuesEmpty() {
        Legate[] expectedArray = {};
        LegateArray array = new LegateArray(expectedArray);

        assertFalse(array.isValid());
    }
}

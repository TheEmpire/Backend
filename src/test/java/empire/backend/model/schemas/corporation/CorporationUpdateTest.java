/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CorporationUpdateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        CorporationUpdate corporationUpdate = new CorporationUpdate(expectedName, expectedDescription);

        assertEquals(expectedName, corporationUpdate.getName());
        assertEquals(expectedDescription, corporationUpdate.getDescription());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        CorporationUpdate corporationUpdate = new CorporationUpdate(null, null);

        assertNull(corporationUpdate.getName());
        assertNull(corporationUpdate.getDescription());
    }

    @Test
    void testIsValid() {
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        CorporationUpdate corporationUpdate = new CorporationUpdate(expectedName, expectedDescription);

        assertTrue(corporationUpdate.isValid());
    }

    @Test
    void testIsValidNameNull() {
        String expectedDescription = "sampleDescription";
        CorporationUpdate corporationUpdate = new CorporationUpdate(null, expectedDescription);

        assertTrue(corporationUpdate.isValid());
    }

    @Test
    void testIsValidDescriptionNull() {
        String expectedName = "name";
        CorporationUpdate corporationUpdate = new CorporationUpdate(expectedName, null);

        assertTrue(corporationUpdate.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedName = "";
        String expectedDescription = "sampleDescription";
        CorporationUpdate corporationUpdate = new CorporationUpdate(expectedName, expectedDescription);

        assertTrue(corporationUpdate.isValid());
    }

    @Test
    void testIsValidDescriptionEmpty() {
        String expectedName = "name";
        String expectedDescription = "";
        CorporationUpdate corporationUpdate = new CorporationUpdate(expectedName, expectedDescription);

        assertTrue(corporationUpdate.isValid());
    }

    @Test
    void testIsValidBothNull() {
        CorporationUpdate corporationUpdate = new CorporationUpdate(null, null);

        assertFalse(corporationUpdate.isValid());
    }

    @Test
    void testIsValidBothEmpty() {
        String expectedName = "";
        String expectedDescription = "";
        CorporationUpdate corporationUpdate = new CorporationUpdate(expectedName, expectedDescription);

        assertFalse(corporationUpdate.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CorporationCreateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        CorporationCreate corporationCreate = new CorporationCreate(expectedName, expectedDescription);

        assertEquals(expectedName, corporationCreate.getName());
        assertEquals(expectedDescription, corporationCreate.getDescription());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        CorporationCreate corporationCreate = new CorporationCreate(null, null);

        assertNull(corporationCreate.getName());
        assertNull(corporationCreate.getDescription());
    }

    @Test
    void testIsValid() {
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        CorporationCreate corporationCreate = new CorporationCreate(expectedName, expectedDescription);

        assertTrue(corporationCreate.isValid());
    }

    @Test
    void testIsValidNameNull() {
        String expectedDescription = "sampleDescription";
        CorporationCreate corporationCreate = new CorporationCreate(null, expectedDescription);

        assertFalse(corporationCreate.isValid());
    }

    @Test
    void testIsValidDescriptionNull() {
        String expectedName = "name";
        CorporationCreate corporationCreate = new CorporationCreate(expectedName, null);

        assertFalse(corporationCreate.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        String expectedName = "";
        String expectedDescription = "sampleDescription";
        CorporationCreate corporationCreate = new CorporationCreate(expectedName, expectedDescription);

        assertFalse(corporationCreate.isValid());
    }

    @Test
    void testIsValidDescriptionEmpty() {
        String expectedName = "name";
        String expectedDescription = "";
        CorporationCreate corporationCreate = new CorporationCreate(expectedName, expectedDescription);

        assertFalse(corporationCreate.isValid());
    }
}

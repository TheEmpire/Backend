/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CorporationTest {
    @Test
    void testGetterAndConstructor() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertEquals(expectedId, corp.getId());
        assertEquals(expectedName, corp.getName());
        assertEquals(expectedDescription, corp.getDescription());
        assertArrayEquals(expectedLegate, corp.getLegate());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        Corporation corp = new Corporation(null, null, null, null);

        assertNull(corp.getId());
        assertNull(corp.getName());
        assertNull(corp.getDescription());
        assertNull(corp.getLegate());
    }

    @Test
    void testIsValid() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertTrue(corp.isValid());
    }

    @Test
    void testIsValidLegatesNull() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, null);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidIdNull() {
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(null, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidNameNull() {
        Integer expectedId = 1;
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, null, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidDescriptionNull() {
        Integer expectedId = 1;
        String expectedName = "name";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, expectedName, null, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidIdSmallerZero() {
        Integer expectedId = -1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidNameEmpty() {
        Integer expectedId = 1;
        String expectedName = "";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidDescriptionEmpty() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "";
        String[] expectedLegate = {"legate1", "legate2"};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidLegatesEmpty() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidLegateEmpty() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", ""};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }

    @Test
    void testIsValidLegateNull() {
        Integer expectedId = 1;
        String expectedName = "name";
        String expectedDescription = "sampleDescription";
        String[] expectedLegate = {"legate1", null};
        Corporation corp = new Corporation(expectedId, expectedName, expectedDescription, expectedLegate);

        assertFalse(corp.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CorporationListTest {
    @Test
    void testGetterAndConstructor() {
        String[] expectedArray = {"corp1", "corp2"};
        CorporationList list = new CorporationList(expectedArray);

        assertArrayEquals(expectedArray, list.getCorporations());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        CorporationList list = new CorporationList(null);

        assertNull(list.getCorporations());
    }

    @Test
    void testIsValid() {
        String[] expectedArray = {"corp1", "corp2"};
        CorporationList list = new CorporationList(expectedArray);

        assertTrue(list.isValid());
    }

    @Test
    void testIsValidCorporationsNull() {
        CorporationList list = new CorporationList(null);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidCorporationsEmpty() {
        String[] expectedArray = {};
        CorporationList list = new CorporationList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidCorporationEmpty() {
        String[] expectedArray = {"corp1", ""};
        CorporationList list = new CorporationList(expectedArray);

        assertFalse(list.isValid());
    }

    @Test
    void testIsValidCorporationNull() {
        String[] expectedArray = {"corp1", null};
        CorporationList list = new CorporationList(expectedArray);

        assertFalse(list.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas.corporation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LegateTest {
    @Test
    void testGetterAndConstructor() {
        String expectedImperialId = "userId";
        Integer expectedId = 1;
        Integer expectedUserId = 2;
        Legate legate = new Legate(expectedId, expectedUserId, expectedImperialId);

        assertEquals(expectedId, legate.getId());
        assertEquals(expectedUserId, legate.getUserId());
        assertEquals(expectedImperialId, legate.getImperialId());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        Legate legate = new Legate(null, null, null);

        assertNull(legate.getId());
        assertNull(legate.getUserId());
        assertNull(legate.getImperialId());
    }

    @Test
    void testIsValid() {
        String expectedImperialId = "userId";
        Integer expectedId = 1;
        Integer expectedUserId = 2;
        Legate legate = new Legate(expectedId, expectedUserId, expectedImperialId);

        assertTrue(legate.isValid());
    }

    @Test
    void testIsValidIdNull() {
        String expectedImperialId = "userId";
        Integer expectedUserId = 2;
        Legate legate = new Legate(null, expectedUserId, expectedImperialId);

        assertFalse(legate.isValid());
    }

    @Test
    void testIsValidImperialIdNull() {
        Integer expectedId = 1;
        Integer expectedUserId = 2;
        Legate legate = new Legate(expectedId, expectedUserId, null);

        assertFalse(legate.isValid());
    }

    @Test
    void testIsValidUserIdNull() {
        String expectedImperialId = "userId";
        Integer expectedId = 1;
        Legate legate = new Legate(expectedId, null, expectedImperialId);

        assertTrue(legate.isValid());
    }

    @Test
    void testIsValidImperialIdEmpty() {
        String expectedImperialId = "";
        Integer expectedId = 1;
        Integer expectedUserId = 2;
        Legate legate = new Legate(expectedId, expectedUserId, expectedImperialId);

        assertFalse(legate.isValid());
    }

    @Test
    void testIsValidIdSmallerZeroEmpty() {
        String expectedImperialId = "userId";
        Integer expectedId = -1;
        Integer expectedUserId = 2;
        Legate legate = new Legate(expectedId, expectedUserId, expectedImperialId);

        assertFalse(legate.isValid());
    }

    @Test
    void testIsValidUserIdSmallerZeroEmpty() {
        String expectedImperialId = "userId";
        Integer expectedId = 1;
        Integer expectedUserId = -1;
        Legate legate = new Legate(expectedId, expectedUserId, expectedImperialId);

        assertFalse(legate.isValid());
    }
}

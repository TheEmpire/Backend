/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UpdateValuesTest {
    @Test
    void testGetterAndConstructor() {
        KeyValuePair[] pair = {new KeyValuePair("key", "val"), new KeyValuePair("key2", "val2")};
        UpdateValues val = new UpdateValues(pair);

        assertArrayEquals(pair, val.getValues());
    }

    @Test
    void testGetterAndConstructorWithPartialNull() {
        KeyValuePair[] pair = {null, new KeyValuePair("key2", "val2")};
        UpdateValues val = new UpdateValues(pair);

        assertArrayEquals(pair, val.getValues());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        UpdateValues val = new UpdateValues(null);

        assertNull(val.getValues());
    }

    @Test
    void testIsValid() {
        KeyValuePair[] pair = {new KeyValuePair("key", "val"), new KeyValuePair("key2", "val2")};
        UpdateValues val = new UpdateValues(pair);

        assertTrue(val.isValid());
    }

    @Test
    void testIsValidValuesNull() {
        UpdateValues val = new UpdateValues(null);

        assertFalse(val.isValid());
    }

    @Test
    void testIsValidValuesPartialNull() {
        KeyValuePair[] pair = {null, new KeyValuePair("key2", "val2")};
        UpdateValues val = new UpdateValues(pair);

        assertFalse(val.isValid());
    }

    @Test
    void testIsValidValuesEmpty() {
        KeyValuePair[] pair = {};
        UpdateValues val = new UpdateValues(pair);

        assertFalse(val.isValid());
    }
}

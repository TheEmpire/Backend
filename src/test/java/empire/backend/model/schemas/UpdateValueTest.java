/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.schemas;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UpdateValueTest {
    @Test
    void testGetterAndConstructor() {
        String expectedVal = "val";
        UpdateValue val = new UpdateValue(expectedVal);

        assertEquals(expectedVal, val.getValue());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        UpdateValue val = new UpdateValue(null);

        assertNull(val.getValue());
    }

    @Test
    void testIsValid() {
        String expectedVal = "val";
        UpdateValue val = new UpdateValue(expectedVal);

        assertTrue(val.isValid());
    }

    @Test
    void testIsValidValNull() {
        UpdateValue val = new UpdateValue(null);

        assertFalse(val.isValid());
    }

    @Test
    void testIsValidValEmpty() {
        String expectedVal = "";
        UpdateValue val = new UpdateValue(expectedVal);

        assertFalse(val.isValid());
    }
}

/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.internal;

import empire.backend.model.enums.UserState;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class UserWithIdTest {
    @Test
    void testGetterAndConstructor() {
        int expectedId = 1;
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        UserWithId user = new UserWithId(expectedId, expectedImperialId, expectedName, expectedMail, expectedTimezone, expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState);

        assertEquals(expectedId, user.getId());
        assertEquals(expectedImperialId, user.getImperialId());
        assertEquals(expectedName, user.getName());
        assertEquals(expectedMail, user.getMail());
        assertEquals(expectedTimezone, user.getTimezone());
        assertArrayEquals(expectedLanguage, user.getLanguage());
        assertEquals(expectedDateOfBirth, user.getDateOfBirth());
        assertEquals(expectedDateOfAccess, user.getDateOfAccess());
        assertEquals(expectedState, user.getState());
        assertTrue(user.exists());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        int expectedId = 1;
        UserWithId user = new UserWithId(expectedId,null, null, null, null, null, null, null, null);

        assertEquals(expectedId, user.getId());
        assertNull(user.getImperialId());
        assertNull(user.getName());
        assertNull(user.getMail());
        assertNull(user.getTimezone());
        assertNull(user.getLanguage());
        assertNull(user.getDateOfBirth());
        assertNull(user.getDateOfAccess());
        assertNull(user.getState());
        assertTrue(user.exists());
    }

    @Test
    void testGetterAndConstructorWithoutParams() {
        UserWithId user = new UserWithId();
        assertNull(user.getId());
        assertNull(user.getImperialId());
        assertNull(user.getName());
        assertNull(user.getMail());
        assertNull(user.getTimezone());
        assertNull(user.getLanguage());
        assertNull(user.getDateOfBirth());
        assertNull(user.getDateOfAccess());
        assertNull(user.getState());
        assertFalse(user.exists());
    }

    @Test
    void testPrepareForExport() {
        String expectedImperialId = "userId";
        String expectedName = "name";
        String expectedMail = "test@test.com";
        String expectedTimezone = "UTC +1";
        String[] expectedLanguage = {"language1", "language2"};
        LocalDate expectedDateOfBirth = LocalDate.now().minusYears(10);
        LocalDate expectedDateOfAccess = LocalDate.now().minusDays(5);
        UserState expectedState = UserState.ACTIVE;
        UserWithId user = new UserWithId(1, expectedImperialId, expectedName, expectedMail, expectedTimezone,
                expectedLanguage, expectedDateOfAccess, expectedDateOfBirth, expectedState).prepareForExport();

        assertNull(user.getId());
        assertEquals(expectedImperialId, user.getImperialId());
        assertEquals(expectedName, user.getName());
        assertEquals(expectedMail, user.getMail());
        assertEquals(expectedTimezone, user.getTimezone());
        assertArrayEquals(expectedLanguage, user.getLanguage());
        assertEquals(expectedDateOfBirth, user.getDateOfBirth());
        assertEquals(expectedDateOfAccess, user.getDateOfAccess());
        assertEquals(expectedState, user.getState());
        assertNull(user.exists());
    }

}

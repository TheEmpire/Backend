package empire.backend.model.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserShareTest {
    @Test
    void testGetterAndConstructor() {
        UserShare user = new UserShare(true, false, false);

        assertTrue(user.isMail());
        assertFalse(user.isDateOfBirth());
        assertFalse(user.isName());
    }
}

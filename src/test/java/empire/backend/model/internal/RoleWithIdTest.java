/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.internal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleWithIdTest {
    private Boolean expectedWrite;
    private Boolean expectedRead;
    private Boolean expectedCreate;
    private Boolean expectedDelete;
    private Integer expectedId;

    @BeforeEach
    void setup() {
        expectedWrite = expectedCreate = true;
        expectedRead = expectedDelete = false;
        expectedId = 1;
    }

    @Test
    void testGetterAndConstructor() {
        RoleWithId role = new RoleWithId(expectedId, expectedRead, expectedWrite, expectedCreate, expectedDelete);

        assertEquals(expectedRead, role.getRead());
        assertEquals(expectedWrite, role.getWrite());
        assertEquals(expectedCreate, role.getCreate());
        assertEquals(expectedDelete, role.getDelete());
        assertEquals(expectedId, role.getId());
        assertTrue(role.exists());
    }

    @Test
    void testGetterAndConstructorTwo() {
        RoleWithId role = new RoleWithId();

        assertNull(role.getRead());
        assertNull(role.getWrite());
        assertNull(role.getCreate());
        assertNull(role.getDelete());
        assertNull(role.getId());
        assertFalse(role.exists());
    }

    @Test
    void testGetterAndConstructorWithNull() {
        RoleWithId role = new RoleWithId(null, null, null, null, null);

        assertNull(role.getRead());
        assertNull(role.getWrite());
        assertNull(role.getCreate());
        assertNull(role.getDelete());
        assertNull(role.getId());
        assertTrue(role.exists());
    }

    @Test
    void testIsValidWriteNull() {
        RoleWithId role = new RoleWithId(null, expectedRead, expectedWrite, expectedCreate, expectedDelete);

        assertTrue(role.isValid());
    }
}

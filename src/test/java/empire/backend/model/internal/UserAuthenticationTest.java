/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.model.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserAuthenticationTest {
    @Test
    void testGetterAndConstructor() {
        String expectedPwd = "password";
        String expectedMail = "mail@valid.com";
        String expectedSalt = "salt";
        boolean expectedVerified = true;
        UserAuthentication auth = new UserAuthentication(expectedPwd, expectedMail, expectedSalt, expectedVerified);

        assertEquals(expectedPwd, auth.getPassword());
        assertEquals(expectedMail, auth.getMail());
        assertEquals(expectedSalt, auth.getSalt());
        assertEquals(expectedVerified, auth.isVerified());
        assertFalse(auth.isEmpty());
    }

    @Test
    void testGetterAndConstructorWithoutParam() {
        UserAuthentication auth = new UserAuthentication();

        assertNull(auth.getPassword());
        assertNull(auth.getMail());
        assertNull(auth.getSalt());
        assertNull(auth.isVerified());
        assertTrue(auth.isEmpty());
    }


    @Test
    void testGetterAndConstructorWithNull() {
        UserAuthentication auth = new UserAuthentication(null, null, null, null);

        assertNull(auth.getPassword());
        assertNull(auth.getMail());
        assertNull(auth.getSalt());
        assertNull(auth.isVerified());
    }
}

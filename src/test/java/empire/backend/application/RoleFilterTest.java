/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.application;

import empire.backend.InitMocks;
import empire.backend.database.RoleQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.internal.RoleFilterId;
import empire.backend.model.internal.RoleWithId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class RoleFilterTest extends InitMocks {
    private List<RoleWithId> roles;
    @Mock
    private ContainerRequestContext context;
    @Mock
    private SecurityContextImpl securityContext;
    @Mock
    private ResourceInfo resourceInfo;
    @Mock
    private JWTHelper jwtHelper;
    @Mock
    private ResponseBuilder builder;
    @Mock
    private RoleQueries roleQueries;
    @Mock
    private CustomGson gson;
    @InjectMocks
    private RoleFilter roleFilter;

    @BeforeEach
    void setup() {
        when(jwtHelper.getName(securityContext)).thenReturn("user");

        roles = new LinkedList<>();
        when(roleQueries.getRolesForUser("user")).thenReturn(roles);
    }

    @Test
    void testFilterNotAnnotated() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("notAnnotated"));

        roleFilter.filter(context);

        verify(builder, times(0)).create(any());
    }

    @Test
    void testFilterAnnotatedDatabaseError() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotated"));

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testFilterAnnotatedUserDoesNotHaveRole() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotated"));
        when(roleQueries.userHasRoleForTable("Test", roles)).thenReturn(new RoleWithId());

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedUserDoesNotHaveRight() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotated"));
        when(roleQueries.userHasRoleForTable("Test", roles)).thenReturn(
                new RoleWithId(1, false, false, false, false));
        when(context.getMethod()).thenReturn("GET", "PUT", "POST", "DELETE");

        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);

        verify(builder, times(4)).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedUserDoesHaveRight() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotated"));
        when(roleQueries.userHasRoleForTable("Test", roles)).thenReturn(
                new RoleWithId(1, true, false, false, false),
                new RoleWithId(1, false, true, false, false),
                new RoleWithId(1, false, false, true, false),
                new RoleWithId(1, false, false, false, true));
        when(context.getMethod()).thenReturn("GET", "PUT", "POST", "DELETE");

        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);

        verify(builder, times(0)).create(any());
    }

    @Test
    void testFilterAnnotatedSelfInPathSameUser() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupPath("user");

        roleFilter.filter(context);

        verify(builder, times(0)).create(any());
    }

    @Test
    void testFilterAnnotatedSelfInPathNotSameUser() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupPath("user2");

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testFilterAnnotatedSelfInPathNotSameUserDoesNotHaveRole() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupPath("user2");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(new RoleWithId());

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfInPathNotSameUserDoesNotHaveRight() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupPath("user2");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(
                new RoleWithId(1, false, false, false, false));
        when(context.getMethod()).thenReturn("GET", "PUT", "POST", "DELETE");

        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);

        verify(builder, times(4)).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfInPathNotSameUserDoesHaveRight() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupPath("user2");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(
                new RoleWithId(1, true, false, false, false),
                new RoleWithId(1, false, true, false, false),
                new RoleWithId(1, false, false, true, false),
                new RoleWithId(1, false, false, false, true));
        when(context.getMethod()).thenReturn("GET", "PUT", "POST", "DELETE");

        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);

        verify(builder, times(0)).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfInBodySameUser() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupBody("user");

        roleFilter.filter(context);

        verify(builder, times(0)).create(any());
    }

    @Test
    void testFilterAnnotatedSelfInBodyNotSameUser() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupBody("user2");

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testFilterAnnotatedSelfInBodyNotSameUserDoesNotHaveRole() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupBody("user2");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(new RoleWithId());

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfInBodyNotSameUserDoesNotHaveRight() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupBody("user2");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(
                new RoleWithId(1, false, false, false, false));
        when(context.getMethod()).thenReturn("GET", "PUT", "POST", "DELETE");

        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);

        verify(builder, times(4)).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfInBodyNotSameUserDoesHaveRight() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupBody("user2");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(
                new RoleWithId(1, true, false, false, false),
                new RoleWithId(1, false, true, false, false),
                new RoleWithId(1, false, false, true, false),
                new RoleWithId(1, false, false, false, true));
        when(context.getMethod()).thenReturn("GET", "PUT", "POST", "DELETE");

        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);
        roleFilter.filter(context);

        verify(builder, times(0)).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfEmptyUser() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        setupPath("");
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(new RoleWithId());

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.ROLE_MISSING);
    }

    @Test
    void testFilterAnnotatedSelfNotInPathAndBody() throws NoSuchMethodException, IOException {
        when(resourceInfo.getResourceMethod()).thenReturn(mockClass.class.getDeclaredMethod("annotatedSelf"));
        UriInfo info = mock(UriInfo.class);
        MultivaluedMap<String, String> map = new MultivaluedHashMap<>();
        when(info.getPathParameters()).thenReturn(map);
        when(context.getUriInfo()).thenReturn(info);
        when(roleQueries.userHasRoleForTable("Test2", roles)).thenReturn(new RoleWithId());

        roleFilter.filter(context);

        verify(builder).create(ServerMessage.ROLE_MISSING);
    }


    private void setupPath(String user) {
        UriInfo info = mock(UriInfo.class);
        MultivaluedMap<String, String> map = new MultivaluedHashMap<>();
        map.putSingle("imperialId", user);
        when(info.getPathParameters()).thenReturn(map);
        when(context.getUriInfo()).thenReturn(info);
    }

    private void setupBody(String user) {
        UriInfo info = mock(UriInfo.class);
        MultivaluedMap<String, String> map = new MultivaluedHashMap<>();
        when(info.getPathParameters()).thenReturn(map);
        when(context.getUriInfo()).thenReturn(info);
        when(context.getEntityStream()).thenReturn(new ByteArrayInputStream(("{\n\"imperialId\": \"" + user + "\"\n}").getBytes()));
        when(gson.fromJson("{\n\"imperialId\": \"" + user + "\"\n}", RoleFilterId.class)).thenReturn(new RoleFilterId(user));
    }

    private static class mockClass {
        void notAnnotated() {
        }

        @WithRole(table = "Test")
        void annotated() {
        }

        @WithRole(table = "Test2", self = true)
        void annotatedSelf() {
        }
    }
}

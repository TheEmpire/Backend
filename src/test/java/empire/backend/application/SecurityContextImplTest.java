/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.application;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SecurityContextImplTest {
    @Test
    void testConstructorAndGetter() {
        String expectedToken = "token";
        String expectedAuthScheme = "Bearer";
        String randomString = String.valueOf(new Random().nextInt(255));
        SecurityContextImpl impl = new SecurityContextImpl(expectedToken, true, expectedAuthScheme);

        assertEquals(expectedToken, impl.getUserPrincipal().getName());
        assertTrue(impl.isUserInRole(randomString));
        assertTrue(impl.isSecure());
        assertEquals(expectedAuthScheme, impl.getAuthenticationScheme());
    }

    @Test
    void testConstructorAndGetterNull() {
        String randomString = String.valueOf(new Random().nextInt(255));
        SecurityContextImpl impl = new SecurityContextImpl(null, false, null);

        assertNull(impl.getUserPrincipal().getName());
        assertTrue(impl.isUserInRole(randomString));
        assertFalse(impl.isSecure());
        assertNull(impl.getAuthenticationScheme());
    }
}

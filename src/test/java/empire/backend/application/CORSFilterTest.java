/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.application;

import empire.backend.InitMocks;
import org.jboss.resteasy.specimpl.MultivaluedMapImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.MultivaluedMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class CORSFilterTest extends InitMocks {

    @Mock private ContainerRequestContext requestContext;
    @Mock private ContainerResponseContext responseContext;
    @InjectMocks private CORSFilter corsFilter;

    @Test
    void testFilter() {
        MultivaluedMap<String, Object> headers = new MultivaluedMapImpl<>();
        when(responseContext.getHeaders()).thenReturn(headers);

        corsFilter.filter(requestContext, responseContext);
        assertTrue(headers.containsKey("Access-Control-Allow-Origin"));
        assertEquals("*", headers.getFirst("Access-Control-Allow-Origin"));
        assertTrue(headers.containsKey("Access-Control-Allow-Headers"));
        assertEquals("origin, content-type, accept, authorization", headers.getFirst("Access-Control-Allow-Headers"));
        assertTrue(headers.containsKey("Access-Control-Allow-Credentials"));
        assertEquals("true", headers.getFirst("Access-Control-Allow-Credentials"));
        assertTrue(headers.containsKey("Access-Control-Allow-Methods"));
        assertEquals("GET, POST, PUT, DELETE, OPTIONS", headers.getFirst("Access-Control-Allow-Methods"));
    }
}

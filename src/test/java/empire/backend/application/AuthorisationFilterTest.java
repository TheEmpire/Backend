/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.application;

import empire.backend.InitMocks;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AuthorisationFilterTest extends InitMocks {
    @Mock private ContainerRequestContext context;
    @Mock private JWTHelper jwtHelper;
    @Mock private ResponseBuilder builder;
    @InjectMocks
    private AuthorisationFilter authorisationFilter;

    @Test
    void testFilterJWTMissing() {
        when(context.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn("bla");

        authorisationFilter.filter(context);
        verify(builder).create(ServerMessage.JWT_MISSING);
    }

    @Test
    void testFilterHeaderNull() {
        when(context.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn(null);

        authorisationFilter.filter(context);
        verify(builder).create(ServerMessage.JWT_MISSING);
    }

    @Test
    void testFilterJWTInvalid() {
        when(context.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn("Bearer token");
        when(jwtHelper.verifyJWT("token")).thenReturn(false);

        authorisationFilter.filter(context);
        verify(builder).create(ServerMessage.JWT_INVALID);
    }

    @Test
    void testFilter() {
        when(context.getHeaderString(HttpHeaders.AUTHORIZATION)).thenReturn("Bearer token");
        when(jwtHelper.verifyJWT("token")).thenReturn(true);
        when(context.getSecurityContext()).thenReturn(new SecurityContextImpl("old", true, "Bla"));
        ArgumentCaptor<SecurityContextImpl> argument = ArgumentCaptor.forClass(SecurityContextImpl.class);

        authorisationFilter.filter(context);
        verify(context).setSecurityContext(argument.capture());
        assertEquals("token", argument.getValue().getUserPrincipal().getName());
        assertEquals(true, argument.getValue().isSecure());
        assertEquals("Bearer", argument.getValue().getAuthenticationScheme());
    }
}

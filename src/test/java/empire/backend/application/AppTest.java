package empire.backend.application;

import org.junit.jupiter.api.Test;

import javax.ws.rs.ApplicationPath;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AppTest {
    @Test
    void testConstructionAndAnnotation() {
        App app = new App();
        ApplicationPath anno = App.class.getDeclaredAnnotation(ApplicationPath.class);

        assertNotNull(app);
        assertNotNull(anno);
        assertEquals("/", anno.value());
    }
}

package empire.backend.application;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.InjectionPoint;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LoggerProducerTest {
    @Test
    void testProducing() {
        InjectionPoint point = mock(InjectionPoint.class);
        Bean bean = mock(Bean.class);
        when(point.getBean()).thenReturn(bean);
        when(bean.getBeanClass()).thenReturn(LoggerProducerTest.class);
        Logger logger = new LoggerProducer().produceLogger(point);
        assertNotNull(logger);
        assertEquals("empire.backend.application.LoggerProducerTest", logger.getName());
    }
}

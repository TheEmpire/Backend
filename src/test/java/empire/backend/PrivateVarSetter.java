package empire.backend;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.fail;

public class PrivateVarSetter {
    public static void setPrivateVar(String value, String varname, Object classToUse) {
        try {
            Class<?> secretClass = classToUse.getClass();
            Field secret = secretClass.getDeclaredField(varname);
            secret.setAccessible(true);
            secret.set(classToUse, value);
            secret.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            fail("Could not set private variable, Reflection did not work");
        }
    }
}

package empire.backend.database;

import empire.backend.InitMocks;
import empire.backend.helper.CustomLogger;
import empire.backend.model.internal.RoleWithId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;

import javax.security.auth.login.CredentialNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

class RoleQueriesTest extends InitMocks {
    @Mock
    DbSetup dbSetup;
    @Mock
    CustomLogger logger;
    @InjectMocks
    RoleQueries queries;

    @BeforeEach
    void setupConnection() throws SQLException, ClassNotFoundException {
        ConnectionHelper helper = new ConnectionHelper();
        setPrivateVar("mysql", "host", helper);
        setPrivateVar("testUser", "user", helper);
        setPrivateVar("secretPassword", "password", helper);
        setPrivateVar("empire", "database", helper);
        Connection conn = helper.loadConnection();
        when(dbSetup.getConnection()).thenReturn(conn);
    }

    @Test
    void testUserHasRoleForTableRolesNull() {
        assertNull(queries.userHasRoleForTable("info", null));
        verify(dbSetup, times(0)).getConnection();
        verify(dbSetup, times(0)).close(any(), any(), any());
    }

    @Test
    void testUserHasRoleForTableRoleMissing() {
        RoleWithId role1 = new RoleWithId(1, true, true, false, false);
        RoleWithId role2 = new RoleWithId(3, true, true, false, false);
        RoleWithId role3 = new RoleWithId(4, true, true, false, false);
        List<RoleWithId> roles = new LinkedList<>();
        roles.add(role1);
        roles.add(role2);
        roles.add(role3);
        RoleWithId actual = queries.userHasRoleForTable("info", roles);

        assertFalse(actual.exists());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUserHasRoleForTableNoEntry() {
        RoleWithId role1 = new RoleWithId(1, true, true, false, false);
        RoleWithId role2 = new RoleWithId(3, true, true, false, false);
        RoleWithId role3 = new RoleWithId(4, true, true, false, false);
        List<RoleWithId> roles = new LinkedList<>();
        roles.add(role1);
        roles.add(role2);
        roles.add(role3);

        RoleWithId role = queries.userHasRoleForTable("notExisting", roles);
        assertNotNull(role);
        assertFalse(role.exists());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUserHasRoleForTableException() throws SQLException {
        Connection conn = mock(Connection.class);
        when(dbSetup.getConnection()).thenReturn(conn);
        SQLException e = new SQLException("Test");
        when(conn.prepareStatement(anyString())).thenThrow(e);
        RoleWithId role1 = new RoleWithId(1, true, true, false, false);
        RoleWithId role2 = new RoleWithId(3, true, true, false, false);
        RoleWithId role3 = new RoleWithId(4, true, true, false, false);
        List<RoleWithId> roles = new LinkedList<>();
        roles.add(role1);
        roles.add(role2);
        roles.add(role3);

        assertNull(queries.userHasRoleForTable("info", roles));
        verify(logger).log("Error on querying role for user and table", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUserHasRoleForTable() {
        RoleWithId role1 = new RoleWithId(1, true, true, false, false);
        RoleWithId role2 = new RoleWithId(2, true, true, false, false);
        RoleWithId role3 = new RoleWithId(4, true, true, false, false);
        List<RoleWithId> roles = new LinkedList<>();
        roles.add(role1);
        roles.add(role2);
        roles.add(role3);

        RoleWithId actual = queries.userHasRoleForTable("info", roles);

        assertTrue(actual.exists());
        assertEquals(role2, actual);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetRolesForUser() {
        RoleWithId role1 = new RoleWithId(0, true, true, true, true);
        RoleWithId role2 = new RoleWithId(1, true, true, false, false);

        List<RoleWithId> actual = queries.getRolesForUser("testUser");

        assertThat(actual, containsInAnyOrder(samePropertyValuesAs(role1), samePropertyValuesAs(role2)));
        assertEquals(2, actual.size());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetRolesForUserTwo() {
        RoleWithId role = new RoleWithId(2, true, false, false, false);

        List<RoleWithId> actual = queries.getRolesForUser("testUser2");

        assertThat(actual, contains(samePropertyValuesAs(role)));
        assertEquals(1, actual.size());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetRolesForUserWithoutRoles() {
        List<RoleWithId> actual = queries.getRolesForUser("testUser3");

        assertEquals(0, actual.size());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetRolesForUserNonExisting() {
        List<RoleWithId> actual = queries.getRolesForUser("NonExisting");

        assertEquals(0, actual.size());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetRolesForUserNull() {
        List<RoleWithId> actual = queries.getRolesForUser(null);

        assertEquals(0, actual.size());
        verify(dbSetup).close(any(), any(), any());
    }


    @Test
    void testGetRolesForUserException() throws SQLException {
        Connection conn = mock(Connection.class);
        when(dbSetup.getConnection()).thenReturn(conn);
        SQLException e = new SQLException("Test");
        when(conn.prepareStatement(anyString())).thenThrow(e);

        assertNull(queries.getRolesForUser("userID"));
        verify(logger).log("Error on querying roles for user", e);
        verify(dbSetup).close(any(), any(), any());
    }
}

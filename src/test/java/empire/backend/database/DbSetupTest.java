/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.database;

import empire.backend.ConsoleErrorMock;
import empire.backend.InitMocks;
import empire.backend.helper.CustomLogger;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class DbSetupTest extends InitMocks {
    @Mock
    ResultSet resultSet;
    @Mock
    PreparedStatement preparedStatement;
    @Mock
    Connection connection;
    @Mock
    ConnectionHelper helper;
    @Mock
    CustomLogger logger;
    @InjectMocks
    DbSetup dbSetup;

    @Test
    void testGetConnectionCredsValid() throws SQLException, ClassNotFoundException {
        when(helper.loadConnection()).thenReturn(null);

        dbSetup.getConnection();

        verify(logger, times(0)).log(any(), any());
    }

    @Test
    void testGetConnectionCredsValidDriverNotFound() throws SQLException, ClassNotFoundException {
        ClassNotFoundException e = new ClassNotFoundException();
        when(helper.loadConnection()).thenThrow(e);

        dbSetup.getConnection();

        verify(logger).log("JDBC Driver class not found", e);
    }

    @Test
    void testGetConnectionCredsValidConnectionCannotBeEstablished() throws SQLException, ClassNotFoundException {
        SQLException e = new SQLException("Test");
        when(helper.loadConnection()).thenThrow(e);

        dbSetup.getConnection();

        verify(logger).log("Error on getting database connection", e);
    }

    @Test
    void testCloseAllNonNull() throws SQLException {
        dbSetup.close(resultSet, preparedStatement, connection);
        verify(resultSet).close();
        verify(preparedStatement).close();
        verify(connection).close();
    }

    @Test
    void testCloseResultSetNull() throws SQLException{
        dbSetup.close(null, preparedStatement, connection);
        verify(resultSet, times(0)).close();
        verify(preparedStatement).close();
        verify(connection).close();
    }

    @Test
    void testClosePreparedStatementNull() throws SQLException{
        dbSetup.close(resultSet, null, connection);
        verify(resultSet).close();
        verify(preparedStatement, times(0)).close();
        verify(connection).close();
    }

    @Test
    void testCloseConnectionNull() throws SQLException{
        dbSetup.close(resultSet, preparedStatement, null);
        verify(resultSet).close();
        verify(preparedStatement).close();
        verify(connection, times(0)).close();
    }

    @Test
    void testCloseResultSetThrowsException() throws SQLException{
        SQLException e = new SQLException();
        doThrow(e).when(resultSet).close();

        dbSetup.close(resultSet, preparedStatement, connection);

        verify(logger).log("Error on closing result set", e);
    }

    @Test
    void testClosePreparedStatementThrowsException() throws SQLException{
        SQLException e = new SQLException();
        doThrow(e).when(preparedStatement).close();

        dbSetup.close(resultSet, preparedStatement, connection);

        verify(logger).log("Error on closing result set", e);
    }

    @Test
    void testCloseConnectionThrowsException() throws SQLException{
        SQLException e = new SQLException();
        doThrow(e).when(connection).close();

        dbSetup.close(resultSet, preparedStatement, connection);

        verify(logger).log("Error on closing result set", e);
    }
}

package empire.backend.database;

import empire.backend.InitMocks;
import empire.backend.helper.CustomLogger;
import empire.backend.model.enums.UserState;
import empire.backend.model.internal.UserAuthentication;
import empire.backend.model.internal.UserShare;
import empire.backend.model.internal.UserWithId;
import empire.backend.model.schemas.user.Registration;
import empire.backend.model.schemas.user.UserUpdate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.stream.Stream;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.Mockito.*;

class UserQueriesTest extends InitMocks {
    @Mock
    DbSetup dbSetup;
    @Mock
    CustomLogger logger;
    @InjectMocks
    UserQueries queries;

    static Stream<Arguments> getUsers() {
        return Stream.of(
                arguments(new UserWithId(0, "testUser", null, "mail@test.com", "Germany UTC+02", new String[]{},
                                LocalDate.of(2019, 9, 20), LocalDate.of(1970, 1, 1), UserState.ACTIVE),
                        "testUser"),
                arguments(new UserWithId(1, "testUser2", "Sample User", "mail2@test.com", "Japan UTC+09", new String[]{},
                                LocalDate.of(2019, 2, 20), LocalDate.of(1990, 3, 3), UserState.SUSPENDED),
                        "testUser2"),
                arguments(new UserWithId(2, "testUser3", "Sample", "mail3@test.com", "England UTC+00", new String[]{},
                                LocalDate.of(2018, 2, 20), LocalDate.of(2010, 12, 3), UserState.TERMINATED),
                        "testUser3"),
                arguments(new UserWithId(3, "testUser4", "User With Language", "mail.4@test.com", "Germany UTC+02", new String[]{"English", "German", "Random"},
                                LocalDate.of(2017, 1, 31), LocalDate.of(1977, 1, 25), UserState.INACTIVE),
                        "testUser4")
        );
    }

    static Stream<Arguments> isVerified() {
        return Stream.of(
                arguments("testUser", false),
                arguments("testUser2", true),
                arguments("testUser5", null),
                arguments("testUser4", true),
                arguments(null, null)
        );
    }

    static Stream<Arguments> userExists() {
        return Stream.of(
                arguments("testUser", true),
                arguments("nonExistent", false),
                arguments(null, false)
        );
    }

    static Stream<Arguments> userCredentials() {
        return Stream.of(
                arguments("testUser", new UserAuthentication("hashed", "mail@test.com", "salt0", false)),
                arguments("testUser2", new UserAuthentication("hashed2", "mail2@test.com", "salt2", true)),
                arguments("testUser5", new UserAuthentication()),
                arguments("testUser4", new UserAuthentication("hashed4", "mail.4@test.com", "salt4", true)),
                arguments("nonExistent", new UserAuthentication()),
                arguments(null, new UserAuthentication())
        );
    }

    static Stream<Arguments> idForMail() {
        return Stream.of(
                arguments("testUser", "mail@test.com"),
                arguments("testUser2", "mail2@test.com"),
                arguments("testUser3", "mail3@test.com"),
                arguments("testUser4", "mail.4@test.com"),
                arguments(null, "nonExistent@mail.com"),
                arguments(null, null)
        );
    }

    static Stream<Arguments> createUser() {
        return Stream.of(
                arguments(true, new Registration("testUser15", "unnecessary", "mail@test.s.com",
                                null, "England UTC+00", null, LocalDate.of(2000, 5, 25)),
                        "hashT", "saltT"),
                arguments(true, new Registration("testUser16", "unnecessary", "mail@test.s.com",
                                null, "England UTC+00", new String[]{}, LocalDate.of(1999, 5, 2)),
                        "hashT", "saltT"),
                arguments(true, new Registration("testUser17", null, "mail@test.s.com",
                        null, "England UTC+00", new String[]{"English", "German", "Test"},
                        LocalDate.of(1950, 1, 5)), "hashT", "saltT"),
                arguments(true, new Registration("testUser8", "unnecessary", "mail@test.s.com",
                        "Test User", "England UTC+00", new String[]{},
                        LocalDate.of(1950, 1, 5)), "hashT", "saltT"),
                arguments(true, new Registration("testUser9", "unnecessary", "mail@sample.com",
                                null, "England UTC+00", new String[]{}, null),
                        "hashT2", "saltT2"),
                arguments(false, new Registration("testUser4", "unnecessary", "mail@sample.com",
                                null, "England UTC+00", null, null),
                        "hashT2", "saltT2"),
                arguments(null, new Registration("testUser10", "unnecessary", "mail@test.s.com",
                                null, "England UTC+00", null, LocalDate.of(2000, 5, 25)),
                        null, null),
                arguments(null, new Registration("testUser11", "unnecessary", "mail@test.s.com",
                                null, "England UTC+00", null, LocalDate.of(2000, 5, 25)),
                        "hashT2", null),
                arguments(null, new Registration("testUser12", "unnecessary", "mail@test.s.com",
                                null, "England UTC+00", null, LocalDate.of(2000, 5, 25)),
                        null, "saltT2"),
                arguments(null, new Registration("testUser13", "unnecessary", null,
                                null, "England UTC+00", null, LocalDate.of(2000, 5, 25)),
                        "hashT2", "saltT2"),
                arguments(null, new Registration("testUser14", "unnecessary", "mail@sample.com",
                                null, null, null, LocalDate.of(2000, 5, 25)),
                        "hashT2", "saltT2"),
                arguments(null, new Registration(null, "unnecessary", "mail@sample.com",
                                null, "England UTC+00", null, LocalDate.of(2000, 5, 25)),
                        "hashT2", "saltT2")
        );
    }

    static Stream<Arguments> updateUserWithoutMail() {
        return Stream.of(
                arguments(true, new UserUpdate("testUser_Update1", "User Test", null, null, null, null),
                        new UserWithId(6, "testUser_Update1", "User Test", "mail@mail.com", "Germany UTC+02", new String[]{},
                                LocalDate.of(2019, 9, 20), LocalDate.of(1999, 1, 1), UserState.ACTIVE)),
                arguments(true, new UserUpdate("testUser_Update2", null, null, "Japan UTC+8", LocalDate.of(1989, 1, 20), UserState.INACTIVE),
                        new UserWithId(7, "testUser_Update2", "User", "mail@mail.com", "Japan UTC+8", new String[]{},
                                LocalDate.of(2019, 9, 20), LocalDate.of(1989, 1, 20), UserState.INACTIVE)),
                arguments(false, new UserUpdate("nonExistent", null, null, "Japan UTC+8", LocalDate.of(1989, 1, 20), UserState.INACTIVE),
                        null)
        );
    }

    static Stream<Arguments> updateUserWithMail() {
        return Stream.of(
                arguments(true, new UserUpdate("testUser_Update3", null, "mail2@valid.com", null, null, null),
                        new UserWithId(8, "testUser_Update3", "User", "mail2@valid.com", "Germany UTC+02", new String[]{},
                                LocalDate.of(2019, 9, 20), LocalDate.of(1999, 1, 1), UserState.ACTIVE)),
                arguments(true, new UserUpdate("testUser_Update4", "User Test", "mail2@valid.com", "Japan UTC+8", LocalDate.of(1989, 1, 20), UserState.INACTIVE),
                        new UserWithId(9, "testUser_Update4", "User Test", "mail2@valid.com", "Japan UTC+8", new String[]{},
                                LocalDate.of(2019, 9, 20), LocalDate.of(1989, 1, 20), UserState.INACTIVE)),
                arguments(false, new UserUpdate("nonExistent", "User Test", "mail2@valid.com", "Japan UTC+8", LocalDate.of(1989, 1, 20), UserState.INACTIVE),
                        null)
        );
    }

    @BeforeEach
    void setupConnection() throws SQLException, ClassNotFoundException {
        ConnectionHelper helper = new ConnectionHelper();
        setPrivateVar("mysql", "host", helper);
        setPrivateVar("testUser", "user", helper);
        setPrivateVar("secretPassword", "password", helper);
        setPrivateVar("empire", "database", helper);
        Connection conn = helper.loadConnection();
        when(dbSetup.getConnection()).thenReturn(conn);
    }

    @Test
    void testGetUserUserNull() {
        UserWithId user = queries.getUser(null);
        assertNotNull(user);
        assertFalse(user.exists());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetUserUserNonExistent() {
        UserWithId user = queries.getUser("nonExistent");

        assertNotNull(user);
        assertFalse(user.exists());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetUserException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.getUser("testUser"));
        verify(logger).log("Error on querying user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("getUsers")
    void testGetUser(UserWithId expected, String user) {
        UserWithId actual = queries.getUser(user);

        assertThat(expected, samePropertyValuesAs(actual));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testIsVerifiedUserNonExistent() {
        assertNull(queries.isVerified("nonExistent"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testIsVerifiedException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.isVerified("testUser"));
        verify(logger).log("Error on querying user verified state", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("isVerified")
    void testIsVerified(String imperialId, Boolean expected) {
        assertEquals(expected, queries.isVerified(imperialId));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testSetVerifiedUserNonExistent() {
        assertFalse(queries.setVerified(500, true));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testSetVerifiedException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.setVerified(1, true));
        verify(logger).log("Error on setting user verified state", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testSetVerifiedUserAlreadyVerified() {
        assertTrue(queries.setVerified(1, true));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testSetVerifiedUserAlreadyNotVerified() {
        assertTrue(queries.setVerified(0, false));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testSetVerifiedUser() {
        assertTrue(queries.setVerified(2, true));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUserExistsException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.userExists("testUser"));
        verify(logger).log("Error on querying whether the user exists", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("userExists")
    void testUserExists(String imperialId, Boolean expected) {
        assertEquals(expected, queries.userExists(imperialId));
    }

    @Test
    void testGetUserCredentialsException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.getUserCredentials("testUser"));
        verify(logger).log("Error on querying user credentials", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("userCredentials")
    void testGetUserCredentials(String imperialId, UserAuthentication expected) {
        assertThat(expected, samePropertyValuesAs(queries.getUserCredentials(imperialId)));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetIdForMailException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.getIdForMail("mail@test.com"));
        verify(logger).log("Error on querying user id with mail", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("idForMail")
    void testGetIdForMail(String expected, String mail) {
        assertEquals(expected, queries.getIdForMail(mail));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateUserException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.createUser(mock(Registration.class), "hashed", "salt"));
        verify(logger).log("Error on creating user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateUserRollbackException() throws SQLException {
        Connection connection = mock(Connection.class);
        when(dbSetup.getConnection()).thenReturn(connection);
        SQLException e = new SQLException();
        when(connection.prepareStatement(anyString())).thenThrow(e);
        doThrow(e).when(connection).rollback();

        assertNull(queries.createUser(null, "hashed", "salt"));
        verify(logger).log("Error on creating user rollback", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateUserRollbackConnectionNull() {
        when(dbSetup.getConnection()).thenReturn(null);

        assertNull(queries.createUser(null, "hashed", "salt"));
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("createUser")
    void testCreateUser(Boolean expected, Registration data, String hash, String salt) {
        assertEquals(expected, queries.createUser(data, hash, salt));

        if (expected == null) {
            assertFalse(queries.userExists(data.getImperialId()));
        } else if (expected) {
            String[] languages = data.getLanguages();
            if (languages == null)
                languages = new String[]{};
            assertThat(new UserWithId(0, data.getImperialId(), data.getName(), data.getMail(), data.getTimezone(),
                            languages, LocalDate.now(), data.getDateOfBirth(), UserState.ACTIVE),
                    samePropertyValuesAs(queries.getUser(data.getImperialId()), "id"));
            assertThat(new UserAuthentication(hash, data.getMail(), salt, false),
                    samePropertyValuesAs(queries.getUserCredentials(data.getImperialId())));

        }
        verify(dbSetup, times(expected == null ? 2 : expected ? 3 : 1)).close(any(), any(), any());
    }

    @Test
    void testUpdatePasswordException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.updatePassword(1, "hashedNew"));
        verify(logger).log("Error on setting password", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdatePasswordUnknownUser() {
        assertFalse(queries.updatePassword(20, "hashedNew"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdatePassword() {
        assertTrue(queries.updatePassword(1, "hashedNew"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetUserShareException() throws SQLException {
        SQLException e = prepareException();

        UserShare actual = queries.getUserShare("unknown");

        assertNull(actual);
        verify(logger).log("Error on querying user share properties", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetUserShareUnknownUser() {
        UserShare actual = queries.getUserShare("unknown");

        assertFalse(actual.isName());
        assertFalse(actual.isDateOfBirth());
        assertFalse(actual.isMail());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetUserShare() {
        UserShare actual = queries.getUserShare("testUser2");

        assertTrue(actual.isName());
        assertTrue(actual.isDateOfBirth());
        assertFalse(actual.isMail());
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteUserException() throws SQLException {
        SQLException e = prepareException();

        Boolean actual = queries.deleteUser("testUser5");

        assertNull(actual);
        verify(logger).log("Error on deleting user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteUser() {
        Boolean actual = queries.deleteUser("testUser5");
        Boolean actualTwo = queries.deleteUser("testUser5");

        assertTrue(actual, "first time should delete user");
        assertFalse(actualTwo, "second time should be not successful");
        verify(dbSetup, times(2)).close(any(), any(), any());
    }

    @Test
    void testUpdateUserException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.updateUser(new UserUpdate("testUser7", "name", null, null, null, null)));
        verify(logger).log("Error on updating user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateUserNoValuesToUpdate() {
        assertFalse(queries.updateUser(new UserUpdate("testUser7", null, null, null, null, null)));
        verify(dbSetup).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("updateUserWithoutMail")
    void testUpdateUserWithoutMail(boolean result, UserUpdate update, UserWithId expected) {
        String imperialId = update.getImperialId();
        UserAuthentication auth = queries.getUserCredentials(imperialId);
        assertEquals(result, queries.updateUser(update));
        if (expected != null) {
            assertThat(auth, samePropertyValuesAs(queries.getUserCredentials(imperialId)));
            assertThat(expected, samePropertyValuesAs(queries.getUser(imperialId)));
            verify(dbSetup, times(4)).close(any(), any(), any());
        } else
            verify(dbSetup, times(2)).close(any(), any(), any());
    }

    @ParameterizedTest
    @MethodSource("updateUserWithMail")
    void testUpdateUserWithMail(boolean result, UserUpdate update, UserWithId expected) {
        assertEquals(result, queries.updateUser(update));
        if (expected != null) {
            UserAuthentication auth = queries.getUserCredentials(update.getImperialId());
            assertFalse(auth.isVerified());
            assertThat(expected, samePropertyValuesAs(queries.getUser(update.getImperialId())));
            verify(dbSetup, times(3)).close(any(), any(), any());
        } else
            verify(dbSetup, times(1)).close(any(), any(), any());
    }

    @Test
    void testCreateLanguageException() throws SQLException {
        SQLException e = prepareException();

        assertEquals(-1, queries.createLanguage("testUser6", "language"));
        verify(logger).log("Error on adding language to user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateLanguageExceptionWithMessage() throws SQLException {
        Connection conn = mock(Connection.class);
        when(dbSetup.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenThrow(new SQLException("SampleMessage"));
        assertEquals(-1, queries.createLanguage("testUser6", "language"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateLanguageUserNotFound() {
        assertEquals(1, queries.createLanguage("unknownUser", "language"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateLanguageLanguageAlreadyExists() {
        assertEquals(2, queries.createLanguage("testUser6", "Sample"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testCreateLanguage() {
        assertEquals(0, queries.createLanguage("testUser6", "testLang"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateLanguageException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.updateLanguage("testUser6", "English", "German"));
        verify(logger).log("Error on updating language for user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateLanguageUserNotFound() {
        assertFalse(queries.updateLanguage("unknownUser", "English", "German"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateLanguageLanguageNotFound() {
        assertFalse(queries.updateLanguage("testUser6", "nonExistent", "German"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateLanguage() {
        assertTrue(queries.updateLanguage("testUser6", "English", "German"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteLanguageException() throws SQLException {
        SQLException e = prepareException();

        assertNull(queries.deleteLanguage("testUser6", "Test"));
        verify(logger).log("Error on deleting language for user", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteLanguageUserNotFound() {
        assertFalse(queries.deleteLanguage("unknownUser", "Test"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteLanguageLanguageNotFound() {
        assertFalse(queries.deleteLanguage("testUser6", "nonExistent"));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteLanguage() {
        assertTrue(queries.deleteLanguage("testUser6", "Test"));
        verify(dbSetup).close(any(), any(), any());
    }

    private SQLException prepareException() throws SQLException {
        SQLException e = new SQLException();
        Connection conn = mock(Connection.class);
        when(dbSetup.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenThrow(e);
        return e;
    }
}

package empire.backend.database;

import empire.backend.InitMocks;
import empire.backend.helper.CustomLogger;
import empire.backend.model.schemas.info.InfoType;
import empire.backend.model.schemas.info.InfoTypeArray;
import empire.backend.model.schemas.info.InfoTypeCreate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;

import javax.security.auth.login.CredentialNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class InfoTypeQueriesTest extends InitMocks {
    @Mock
    DbSetup dbSetup;
    @Mock
    CustomLogger logger;
    @InjectMocks
    InfoTypeQueries queries;

    @BeforeEach
    void setupConnection() throws SQLException, ClassNotFoundException {
        ConnectionHelper helper = new ConnectionHelper();
        setPrivateVar("mysql", "host", helper);
        setPrivateVar("testUser", "user", helper);
        setPrivateVar("secretPassword", "password", helper);
        setPrivateVar("empire", "database", helper);
        Connection conn = helper.loadConnection();
        when(dbSetup.getConnection()).thenReturn(conn);
    }

    @Test
    void testGetTypes() {
        InfoType type2 = new InfoType(2, "Test2", 1);
        InfoType type3 = new InfoType(4, "Test3", 2);
        InfoType type4 = new InfoType(5, "Test4", 2);

        InfoTypeArray actual = queries.getTypes();

        assertThat(actual.getTypes(), hasItemInArray(samePropertyValuesAs(type2)));
        assertThat(actual.getTypes(), hasItemInArray(samePropertyValuesAs(type3)));
        assertThat(actual.getTypes(), hasItemInArray(samePropertyValuesAs(type4)));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testGetTypesWithException() throws SQLException {
        Connection conn = mock(Connection.class);
        SQLException e = new SQLException("Test");
        when(dbSetup.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenThrow(e);

        assertNull(queries.getTypes());
        verify(logger).log("Error on querying information types", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testInsertTypeWithException() throws SQLException {
        Connection conn = mock(Connection.class);
        SQLException e = new SQLException("Test");
        when(dbSetup.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenThrow(e);

        assertNull(queries.insertType(null));
        verify(logger).log("Error on adding information type", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testInsertTypeAlreadyExistingType() {
        InfoTypeCreate type = new InfoTypeCreate("Test2", 2);

        assertFalse(queries.insertType(type));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testInsertType() {
        InfoTypeCreate type = new InfoTypeCreate("Test5", 2);
        InfoType expected = new InfoType(7, "Test5", 2);

        assertTrue(queries.insertType(type));
        InfoTypeArray actual = queries.getTypes();
        assertThat(actual.getTypes(), hasItemInArray(samePropertyValuesAs(expected, "id")));
        verify(dbSetup, times(2)).close(any(), any(), any());
    }

    @Test
    void testUpdateTypeWithException() throws SQLException {
        Connection conn = mock(Connection.class);
        SQLException e = new SQLException("Test");
        when(dbSetup.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenThrow(e);

        assertNull(queries.updateType(null, 1));
        verify(logger).log("Error on updating information type", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateTypeNonExistingType() {
        InfoTypeCreate type = new InfoTypeCreate("test", 2);

        assertFalse(queries.updateType(type, 10));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testUpdateType() {
        InfoType expected = new InfoType(1, "test", 2);
        InfoTypeCreate type = new InfoTypeCreate("test", 2);

        assertTrue(queries.updateType(type, 1));
        InfoType actual = queries.getTypes().getTypes()[0];
        assertThat(actual, samePropertyValuesAs(expected));
        verify(dbSetup, times(2)).close(any(), any(), any());
    }

    @Test
    void testDeleteTypeWithException() throws SQLException {
        Connection conn = mock(Connection.class);
        SQLException e = new SQLException("Test");
        when(dbSetup.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenThrow(e);

        assertNull(queries.deleteType(1));
        verify(logger).log("Error on deleting information type", e);
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteTypeNonExistingType() {
        assertFalse(queries.deleteType(10));
        verify(dbSetup).close(any(), any(), any());
    }

    @Test
    void testDeleteType() {
        InfoType expected = new InfoType(6, "Testing", 0);

        assertTrue(queries.deleteType(6));
        InfoTypeArray actual = queries.getTypes();
        assertThat(actual.getTypes(), not(hasItemInArray(samePropertyValuesAs(expected))));
        verify(dbSetup, times(2)).close(any(), any(), any());
    }
}

package empire.backend.database;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConnectionHelperTest {
    @Test
    void testConnectionHelper() throws SQLException, ClassNotFoundException {
        ConnectionHelper helper = new ConnectionHelper();
        setPrivateVar("mysql", "host", helper);
        setPrivateVar("testUser", "user", helper);
        setPrivateVar("secretPassword", "password", helper);
        setPrivateVar("empire", "database", helper);

        Connection connection = helper.loadConnection();

        assertNotNull(connection);
    }
}

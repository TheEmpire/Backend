/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.InitMocks;
import empire.backend.helper.ImageHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ImageServletTest extends InitMocks {
    @Mock private ImageHelper helper;
    @Mock private ResponseBuilder builder;
    @InjectMocks private ImageServlet servlet;

    @BeforeEach
    void setup() {
        InputStream stream = new ByteArrayInputStream("test".getBytes());
        when(helper.readImageFromResource(matches("[a-zA-Z]*\\.(png|svg)"))).thenReturn(stream);
        when(helper.readImageFromResource(matches(".*[0-9]\\.(png|svg)"))).thenReturn(null);
        when(helper.convertPng(any(), anyInt(), anyInt(), anyInt())).thenReturn(stream);
    }

    @Test
    void testGetImageMissingFileExtension() {
        servlet.getImage("logo", "255,255,255");
        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testGetImageInvalidColorFormatNotEnoughColor() {
        servlet.getImage("logo.png", "255,255");
        verify(builder).create(ServerMessage.QUERY_PARAM_WRONG);
    }

    @Test
    void testGetImageInvalidColorFormatTooHighColor() {
        servlet.getImage("logo.png", "255,255,256");
        verify(builder).create(ServerMessage.QUERY_PARAM_WRONG);
    }

    @Test
    void testGetImageInvalidFileExtension() {
        servlet.getImage("logo.xyz", "255,255,255");
        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testGetImageNonExistentImage() {
        servlet.getImage("logo2.png", "255,255,255");
        verify(builder).create(ServerMessage.IMAGE_NOT_FOUND);
    }

    @Test
    void testGetImageLogoNoColorChange() {
        servlet.getImage("logo.png", "255,255,255");
        verify(builder).create(any(InputStream.class), eq("png"));
        verify(helper, times(0)).convertPng(any(), anyInt(), anyInt(), anyInt());
    }

    @Test
    void testGetImageLogoWithColorChange() {
        servlet.getImage("logo.png", "255,0,255");
        verify(builder).create(any(InputStream.class), eq("png"));
        verify(helper).convertPng(any(), eq(255), eq(0), eq(255));
    }

    @Test
    void testGetImageLogoOutlineNoColorChange() {
        servlet.getImage("logoOutline.png", "255,255,255");
        verify(builder).create(any(InputStream.class), eq("png"));
        verify(helper, times(0)).convertPng(any(), anyInt(), anyInt(), anyInt());
    }

    @Test
    void testGetImageLogoOutlineWithColorChange() {
        servlet.getImage("logoOutline.png", "255,0,255");
        verify(builder).create(any(InputStream.class), eq("png"));
        verify(helper).convertPng(any(), eq(255), eq(0), eq(255));
    }

    @Test
    void testGetImageBackgroundWithColorChangeShouldIgnoreColor() {
        servlet.getImage("background.png", "255,0,255");
        verify(builder).create(any(InputStream.class), eq("png"));
        verify(helper, times(0)).convertPng(any(), anyInt(), anyInt(), anyInt());
    }

    @Test
    void testGetImageBackground() {
        servlet.getImage("background.png", "255,255,255");
        verify(builder).create(any(InputStream.class), eq("png"));
        verify(helper, times(0)).convertPng(any(), anyInt(), anyInt(), anyInt());
    }

    @Test
    void testGetImageLoadingWithColorChangeShouldIgnoreColor() {
        servlet.getImage("loading.svg", "255,0,255");
        verify(builder).create(any(InputStream.class), eq("svg"));
        verify(helper, times(0)).convertPng(any(), anyInt(), anyInt(), anyInt());
    }

    @Test
    void testGetImageLoading() {
        servlet.getImage("loading.svg", "255,255,255");
        verify(builder).create(any(InputStream.class), eq("svg"));
        verify(helper, times(0)).convertPng(any(), anyInt(), anyInt(), anyInt());
    }

    @Test
    void testGetImageWithColorChangeErrorInConversion() {
        when(helper.convertPng(any(), anyInt(), anyInt(), anyInt())).thenReturn(null);
        servlet.getImage("logo.png", "255,0,255");
        verify(builder).create(ServerMessage.COLOR_CONVERSION_ERROR);
        verify(helper).convertPng(any(), anyInt(), anyInt(), anyInt());
    }
}

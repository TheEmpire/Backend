package empire.backend.rest;

import empire.backend.InitMocks;
import empire.backend.database.InfoTypeQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.schemas.info.InfoType;
import empire.backend.model.schemas.info.InfoTypeArray;
import empire.backend.model.schemas.info.InfoTypeCreate;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class InfoTypeServletTest extends InitMocks {
    @Mock
    InfoTypeQueries typeQueries;
    @Mock
    ResponseBuilder builder;
    @Mock
    CustomGson gson;
    @InjectMocks
    InfoTypeServlet infoTypeServlet;

    @Test
    void testGetTypes() {
        InfoTypeArray expectedTypes = new InfoTypeArray(new InfoType[]{
                new InfoType(1, "name1", 3), new InfoType(2, "name2", 2)});
        when(typeQueries.getTypes()).thenReturn(expectedTypes);

        infoTypeServlet.getTypes();

        verify(builder).create(expectedTypes);
    }

    @Test
    void testGetTypesArrayNull() {
        infoTypeServlet.getTypes();

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testCreateTypeInvalidJson() {
        when(gson.fromJson("invalid", InfoTypeCreate.class)).thenReturn(new InfoTypeCreate("type", -1));
        infoTypeServlet.createType("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testCreateTypeJsonNull() {
        infoTypeServlet.createType("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testCreateTypeAlreadyExistingType() {
        InfoTypeCreate type = new InfoTypeCreate("type1", 1);

        when(gson.fromJson("valid", InfoTypeCreate.class)).thenReturn(type);
        when(typeQueries.insertType(type)).thenReturn(false);

        infoTypeServlet.createType("valid");

        verify(builder).create(ServerMessage.INFO_TYPE_ALREADY_EXISTS);
    }

    @Test
    void testCreateTypeErrorOnInsert() {
        InfoTypeCreate type = new InfoTypeCreate("type1", 1);

        when(gson.fromJson("valid", InfoTypeCreate.class)).thenReturn(type);
        when(typeQueries.insertType(type)).thenReturn(null);

        infoTypeServlet.createType("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testCreateType() {
        InfoTypeCreate type = new InfoTypeCreate("type1", 1);

        when(gson.fromJson("valid", InfoTypeCreate.class)).thenReturn(type);
        when(typeQueries.insertType(type)).thenReturn(true);

        infoTypeServlet.createType("valid");

        verify(builder).create(ServerMessage.TYPE_CREATED);
    }

    @Test
    void testUpdateTypeInvalidJson() {
        when(gson.fromJson("invalid", InfoTypeCreate.class)).thenReturn(new InfoTypeCreate("type", -1));

        infoTypeServlet.updateType(1, "invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testUpdateTypeJsonNull() {
        infoTypeServlet.updateType(1, "invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testUpdateTypeInvalidPathParam() {
        infoTypeServlet.updateType(-1, "valid");

        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testUpdateTypeNotExistingType() {
        InfoTypeCreate type = new InfoTypeCreate("type", 1);

        when(gson.fromJson("valid", InfoTypeCreate.class)).thenReturn(type);
        when(typeQueries.updateType(type, 1)).thenReturn(false);

        infoTypeServlet.updateType(1, "valid");

        verify(builder).create(ServerMessage.TYPE_NOT_FOUND);
    }

    @Test
    void testUpdateTypeErrorOnUpdate() {
        InfoTypeCreate type = new InfoTypeCreate("type", 1);

        when(gson.fromJson("valid", InfoTypeCreate.class)).thenReturn(type);
        when(typeQueries.updateType(type, 1)).thenReturn(null);

        infoTypeServlet.updateType(1, "valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testUpdateType() {
        InfoTypeCreate type = new InfoTypeCreate("type", 1);

        when(gson.fromJson("valid", InfoTypeCreate.class)).thenReturn(type);
        when(typeQueries.updateType(type, 1)).thenReturn(true);

        infoTypeServlet.updateType(1, "valid");

        verify(builder).create(ServerMessage.TYPE_UPDATED);
    }

    @Test
    void testDeleteTypeInvalidPathParam() {
        infoTypeServlet.deleteType(-1);

        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testDeleteTypeNotExistingType() {
        when(typeQueries.deleteType(1)).thenReturn(false);

        infoTypeServlet.deleteType(1);

        verify(builder).create(ServerMessage.TYPE_NOT_FOUND);
    }

    @Test
    void testDeleteTypeErrorOnDelete() {
        when(typeQueries.deleteType(1)).thenReturn(null);

        infoTypeServlet.deleteType(1);

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testDeleteType() {
        when(typeQueries.deleteType(1)).thenReturn(true);

        infoTypeServlet.deleteType(1);

        verify(builder).create(ServerMessage.TYPE_DELETED);
    }
}

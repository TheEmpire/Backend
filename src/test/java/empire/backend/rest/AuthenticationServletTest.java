/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend.rest;

import empire.backend.InitMocks;
import empire.backend.application.SecurityContextImpl;
import empire.backend.database.UserQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.HashHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.helper.mail.MailHelper;
import empire.backend.model.enums.AvailableLanguage;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.internal.UserAuthentication;
import empire.backend.model.internal.UserWithId;
import empire.backend.model.schemas.auth.Login;
import empire.backend.model.schemas.auth.PasswordReset;
import empire.backend.model.schemas.auth.PasswordUpdate;
import empire.backend.model.schemas.user.Registration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.core.SecurityContext;
import java.time.LocalDate;

import static empire.backend.PrivateVarSetter.setPrivateVar;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class AuthenticationServletTest extends InitMocks {
    @Mock
    private HashHelper encryption;
    @Mock
    private JWTHelper jwtHelper;
    @Mock
    private UserQueries user;
    @Mock
    private CustomGson gson;
    @Mock
    private ResponseBuilder builder;
    @Mock
    private MailHelper mailHelper;
    @InjectMocks
    private AuthenticationServlet servlet;

    @BeforeEach
    void setup() {
        when(encryption.getSalt(anyInt())).thenReturn("userSalt");
        when(encryption.hash("pwd", "fileSaltuserSalt")).thenReturn("userHash");
        setPrivateVar("fileSalt", "saltPwd", servlet);
    }

    @Test
    void testRegisterInvalidJson() {
        when(gson.fromJson("invalid", Registration.class)).thenReturn(
                new Registration("id", "pwd", null, null,
                        "UTC +1, Testingland", null, LocalDate.of(1900, 5, 20)));

        servlet.register("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testRegisterJsonNull() {
        servlet.register("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }
    @Test
    void testRegisterUserAlreadyExists() {
        when(gson.fromJson("valid", Registration.class)).thenReturn(
                new Registration("id", "pwd", "mail@mail.com", null,
                        "UTC +1, Testingland", null, LocalDate.of(1900, 5, 20)));
        when(user.userExists("id")).thenReturn(true);

        servlet.register("valid");

        verify(builder).create(ServerMessage.USER_ALREADY_EXISTS);
    }

    @Test
    void testRegisterDatabaseErrorUserExists() {
        Registration data = new Registration("id", "pwd", "mail@mail.com", null,
                "UTC +1, Testingland", null, LocalDate.of(1900, 5, 20));
        when(gson.fromJson("valid", Registration.class)).thenReturn(data);
        when(user.userExists("id")).thenReturn(null);

        servlet.register("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testRegisterDatabaseError() {
        Registration data = new Registration("id", "pwd", "mail@mail.com", null,
                "UTC +1, Testingland", null, LocalDate.of(1900, 5, 20));
        when(gson.fromJson("valid", Registration.class)).thenReturn(data);
        when(user.userExists("valid")).thenReturn(false);
        when(user.createUser(data, "userHash", "userSalt")).thenReturn(false);

        servlet.register("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testRegisterJWTError() {
        Registration data = new Registration("id", "pwd", "mail@mail.com", null,
                "UTC +1, Testingland", null, LocalDate.of(1900, 5, 20));
        when(gson.fromJson("valid", Registration.class)).thenReturn(data);
        when(user.userExists("id")).thenReturn(false);
        when(user.createUser(data, "userHash", "userSalt")).thenReturn(true);
        when(jwtHelper.createJWT("id", "mail@mail.com", true)).thenReturn(null);

        servlet.register("valid");

        verify(user).createUser(data, "userHash", "userSalt");
        verify(builder).create(ServerMessage.JWT_ERROR);
    }

    @Test
    void testRegister() {
        Registration data = new Registration("id", "pwd", "mail@mail.com", null,
                "UTC +1, Testingland", null, LocalDate.of(1900, 5, 20));
        when(gson.fromJson("valid", Registration.class)).thenReturn(data);
        when(user.userExists("id")).thenReturn(false);
        when(user.createUser(data, "userHash", "userSalt")).thenReturn(true);
        when(jwtHelper.createJWT("id", "mail@mail.com", false)).thenReturn("jwt");

        servlet.register("valid");

        verify(user).createUser(data, "userHash", "userSalt");
        verify(builder).create(ServerMessage.USER_CREATED, "jwt");
    }

    @Test
    void testLoginInvalidJson() {
        when(gson.fromJson("invalid", Login.class)).thenReturn(
                new Login("id", null));

        servlet.login("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testLoginJsonNull() {
        servlet.login("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testLoginInvalidCredentials() {
        when(gson.fromJson("valid", Login.class)).thenReturn(
                new Login("id", "pwd"));
        when(user.getUserCredentials("id")).thenReturn(new UserAuthentication("bla", "mail@mail.com", "userSalt", true));
        when(encryption.verifyUserPassword("pwd", "bla", "fileSaltuserSalt")).thenReturn(false);

        servlet.login("valid");

        verify(builder).create(ServerMessage.ACCESS_DENIED);
    }

    @Test
    void testLoginDatabaseError() {
        when(gson.fromJson("valid", Login.class)).thenReturn(
                new Login("id", "pwd"));
        when(user.getUserCredentials("id")).thenReturn(null);

        servlet.login("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testLoginUserNotFound() {
        when(gson.fromJson("valid", Login.class)).thenReturn(
                new Login("id", "pwd"));
        when(user.getUserCredentials("id")).thenReturn(new UserAuthentication());

        servlet.login("valid");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testLoginJWTError() {
        when(gson.fromJson("valid", Login.class)).thenReturn(
                new Login("id", "pwd"));
        when(user.getUserCredentials("id")).thenReturn(new UserAuthentication("pwd", "mail@mail.com", "userSalt", true));
        when(encryption.verifyUserPassword("pwd", "pwd", "fileSaltuserSalt")).thenReturn(true);
        when(jwtHelper.createJWT("id", "mail@mail.com", true)).thenReturn(null);

        servlet.login("valid");

        verify(builder).create(ServerMessage.JWT_ERROR);
    }

    @Test
    void testLogin() {
        when(gson.fromJson("valid", Login.class)).thenReturn(
                new Login("id", "pwd"));
        when(user.getUserCredentials("id")).thenReturn(new UserAuthentication("pwd", "mail@mail.com", "userSalt", true));
        when(encryption.verifyUserPassword("pwd", "pwd", "fileSaltuserSalt")).thenReturn(true);
        when(jwtHelper.createJWT("id", "mail@mail.com", true)).thenReturn("jwt2");

        servlet.login("valid");

        verify(builder).create(ServerMessage.LOGIN_SUCCESS, "jwt2");
    }

    @Test
    void testRenewTokenJWTError() {
        SecurityContext context = new SecurityContextImpl("token", true, "Bearer");
        when(jwtHelper.renewJWT("token")).thenReturn(null);

        servlet.renewToken(context);

        verify(builder).create(ServerMessage.JWT_ERROR);
    }

    @Test
    void testRenewToken() {
        SecurityContext context = new SecurityContextImpl("token", true, "Bearer");
        when(jwtHelper.renewJWT("token")).thenReturn("newToken");

        servlet.renewToken(context);

        verify(builder).create(ServerMessage.RENEW_SUCCESS, "newToken");
    }

    @Test
    void testChangePasswordInvalidJson() {
        when(gson.fromJson("invalid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", null));
        servlet.changePassword("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testChangePasswordJsonNull() {
        servlet.changePassword("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testChangePasswordUserNotFound() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication());

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testChangePasswordDatabaseErrorOnAuthQuery() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(null);

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testChangePasswordOldPasswordIncorrect() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.verifyUserPassword("oldPassword", "password", "fileSaltuserSalt")).thenReturn(false);

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.AUTH_INCORRECT);
    }

    @Test
    void testChangePasswordDatabaseErrorOnUserQuery() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.verifyUserPassword("oldPassword", "password", "fileSaltuserSalt")).thenReturn(true);
        when(user.getUser("userId")).thenReturn(null);

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testChangePasswordDatabaseErrorOnUpdate() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.verifyUserPassword("oldPassword", "password", "fileSaltuserSalt")).thenReturn(true);
        when(user.getUser("userId")).thenReturn(new UserWithId(1, null, null, "mail", null, null, null, null, null));
        when(encryption.hash("newPassword", "fileSaltuserSalt")).thenReturn("hashed");
        when(user.updatePassword(1, "hashed")).thenReturn(false);

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testChangePasswordJWTError() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.verifyUserPassword("oldPassword", "password", "fileSaltuserSalt")).thenReturn(true);
        when(user.getUser("userId")).thenReturn(new UserWithId(1, null, null, "mail", null, null, null, null, null));
        when(encryption.hash("newPassword", "fileSaltuserSalt")).thenReturn("hashed");
        when(user.updatePassword(1, "hashed")).thenReturn(true);
        when(jwtHelper.createJWT("userId", "mail", false)).thenReturn(null);

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.JWT_ERROR);
    }

    @Test
    void testChangePassword() {
        when(gson.fromJson("valid", PasswordUpdate.class)).thenReturn(
                new PasswordUpdate("oldPassword", "newPassword", "userId"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.verifyUserPassword("oldPassword", "password", "fileSaltuserSalt")).thenReturn(true);
        when(user.getUser("userId")).thenReturn(new UserWithId(1, null, null, "mail", null, null, null, null, null));
        when(encryption.hash("newPassword", "fileSaltuserSalt")).thenReturn("hashed");
        when(user.updatePassword(1, "hashed")).thenReturn(true);
        when(jwtHelper.createJWT("userId", "mail", false)).thenReturn("jwt");

        servlet.changePassword("valid");

        verify(builder).create(ServerMessage.PASSWORD_UPDATED, "jwt");
    }

    @Test
    void testResetPasswordInvalidJson() {
        when(gson.fromJson("invalid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", null));

        servlet.resetPassword("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testResetPasswordJsonNull() {
        servlet.resetPassword("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testResetPasswordDatabaseErrorOnAuthQuery() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(null);

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testResetPasswordUserNotFound() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication());

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testResetPasswordAuthIncorrectHashOne() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hashed1");
        when(encryption.hash("password", "userId")).thenReturn("hashTwo");

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.AUTH_INCORRECT);
    }

    @Test
    void testResetPasswordAuthIncorrectHashTwo() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hashOne");
        when(encryption.hash("password", "userId")).thenReturn("hashed2");

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.AUTH_INCORRECT);
    }

    @Test
    void testResetPasswordDatabaseErrorOnUserQuery() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hashOne");
        when(encryption.hash("password", "userId")).thenReturn("hashTwo");
        when(user.getUser("userId")).thenReturn(null);

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testResetPasswordDatabaseErrorOnUpdate() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hashOne");
        when(encryption.hash("password", "userId")).thenReturn("hashTwo");
        when(user.getUser("userId")).thenReturn(new UserWithId(1, null, null, "mail", null, null, null, null, null));
        when(encryption.hash("newPassword", "fileSaltuserSalt")).thenReturn("hashed");
        when(user.updatePassword(1, "hashed")).thenReturn(false);

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testResetPasswordJWTError() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hashOne");
        when(encryption.hash("password", "userId")).thenReturn("hashTwo");
        when(user.getUser("userId")).thenReturn(new UserWithId(1, null, null, "mail", null, null, null, null, null));
        when(encryption.hash("newPassword", "fileSaltuserSalt")).thenReturn("hashed");
        when(user.updatePassword(1, "hashed")).thenReturn(true);
        when(jwtHelper.createJWT("userId", "mail", false)).thenReturn(null);

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.JWT_ERROR);
    }

    @Test
    void testResetPassword() {
        when(gson.fromJson("valid", PasswordReset.class)).thenReturn(
                new PasswordReset("hashOne", "hashTwo", "userId", "newPassword"));
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hashOne");
        when(encryption.hash("password", "userId")).thenReturn("hashTwo");
        when(user.getUser("userId")).thenReturn(new UserWithId(1, null, null, "mail", null, null, null, null, null));
        when(encryption.hash("newPassword", "fileSaltuserSalt")).thenReturn("hashed");
        when(user.updatePassword(1, "hashed")).thenReturn(true);
        when(jwtHelper.createJWT("userId", "mail", false)).thenReturn("jwt");

        servlet.resetPassword("valid");

        verify(builder).create(ServerMessage.PASSWORD_UPDATED, "jwt");
    }

    @Test
    void testSendResetLanguageWrong() {
        servlet.sendReset("invalid", "id");

        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testSendResetIdentifierNull() {
        servlet.sendReset("EN", null);

        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testSendResetNoUserForMail() {
        when(user.getIdForMail("mail@valid.com")).thenReturn(null);

        servlet.sendReset("EN", "mail@valid.com");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testSendResetNoMailNoUserQuery() {
        when(user.getUserCredentials("userId")).thenReturn(null);

        servlet.sendReset("EN", "userId");

        verify(user, times(0)).getIdForMail(any());
    }

    @Test
    void testSendResetDatabaseErrorOnAuthQuery() {
        when(user.getUserCredentials("userId")).thenReturn(null);

        servlet.sendReset("EN", "userId");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testSendResetUserNotFoundOnAuthQuery() {
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication());

        servlet.sendReset("EN", "userId");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testSendResetMailBodyError() {
        when(user.getUserCredentials("userId")).thenReturn(
                new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hash1");
        when(encryption.hash("password", "userId")).thenReturn("hash2");
        when(mailHelper.getResetMail("userId", "hash1", "hash2", AvailableLanguage.EN))
                .thenReturn(new String[]{});
        servlet.sendReset("EN", "userId");

        verify(builder).create(ServerMessage.MAIL_SEND_ERROR);
    }

    @Test
    void testSendReset() {
        when(user.getUserCredentials("userId")).thenReturn(
                new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hash1");
        when(encryption.hash("password", "userId")).thenReturn("hash2");
        when(mailHelper.getResetMail("userId", "hash1", "hash2", AvailableLanguage.EN))
                .thenReturn(new String[]{"body", "subject"});
        servlet.sendReset("EN", "userId");

        verify(mailHelper).sendMail("body", "mail", "subject", ServerMessage.RESET_MAIL_SENT);
    }

    @Test
    void testSendValidationLanguageWrong() {
        servlet.sendValidation("invalid", "id");

        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testSendValidationIdentifierNull() {
        servlet.sendValidation("EN", null);

        verify(builder).create(ServerMessage.PATH_PARAM_WRONG);
    }

    @Test
    void testSendValidationNoUserForMail() {
        when(user.getIdForMail("mail@valid.com")).thenReturn(null);

        servlet.sendValidation("EN", "mail@valid.com");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testSendValidationNoMailNoUserQuery() {
        when(user.getUserCredentials("userId")).thenReturn(null);

        servlet.sendValidation("EN", "userId");

        verify(user, times(0)).getIdForMail(any());
    }

    @Test
    void testSendValidationDatabaseErrorOnAuthQuery() {
        when(user.getUserCredentials("userId")).thenReturn(null);

        servlet.sendValidation("EN", "userId");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testSendValidationUserNotFoundOnAuthQuery() {
        when(user.getUserCredentials("userId")).thenReturn(new UserAuthentication());

        servlet.sendValidation("EN", "userId");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testSendValidationMailBodyError() {
        when(user.getUserCredentials("userId")).thenReturn(
                new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hash1");
        when(encryption.hash("password", "userId")).thenReturn("hash2");
        when(mailHelper.getValidationMail("mail", "userId", AvailableLanguage.EN))
                .thenReturn(new String[]{});
        servlet.sendValidation("EN", "userId");

        verify(builder).create(ServerMessage.MAIL_SEND_ERROR);
    }

    @Test
    void testSendValidation() {
        when(user.getUserCredentials("userId")).thenReturn(
                new UserAuthentication("password", "mail", "userSalt", false));
        when(encryption.hash("password", "fileSalt")).thenReturn("hash1");
        when(encryption.hash("password", "userId")).thenReturn("hash2");
        when(mailHelper.getValidationMail("mail", "userId", AvailableLanguage.EN))
                .thenReturn(new String[]{"body", "subject"});
        servlet.sendValidation("EN", "userId");

        verify(mailHelper).sendMail("body", "mail", "subject", ServerMessage.VALIDATION_MAIL_SENT);
    }
}

package empire.backend.rest;

import empire.backend.InitMocks;
import empire.backend.database.UserQueries;
import empire.backend.helper.CustomGson;
import empire.backend.helper.JWTHelper;
import empire.backend.helper.HashHelper;
import empire.backend.helper.ResponseBuilder;
import empire.backend.model.enums.ServerMessage;
import empire.backend.model.enums.UserState;
import empire.backend.model.internal.UserShare;
import empire.backend.model.internal.UserWithId;
import empire.backend.model.schemas.auth.ValidationHash;
import empire.backend.model.schemas.UpdateValue;
import empire.backend.model.schemas.user.UserUpdate;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import javax.ws.rs.core.SecurityContext;
import java.time.LocalDate;

import static org.mockito.Mockito.*;

class UserServletTest extends InitMocks {
    @Mock
    CustomGson gson;
    @Mock
    ResponseBuilder builder;
    @Mock
    UserQueries queries;
    @Mock
    JWTHelper jwtHelper;
    @Mock
    HashHelper encryptionHelper;

    @InjectMocks
    UserServlet servlet;

    @Test
    void testUpdateUserInvalidJson() {
        when(gson.fromJson("invalid", UserUpdate.class)).thenReturn(new UserUpdate(null, null, null, null, null, null));

        servlet.updateUser("invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testUpdateUserJsonNull() {
        servlet.updateUser(null);

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testUpdateUserDatabaseError() {
        UserUpdate update = new UserUpdate("testId", "name", null, null, null, null);
        when(gson.fromJson("valid", UserUpdate.class)).thenReturn(update);
        when(queries.updateUser(update)).thenReturn(null);

        servlet.updateUser("valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testUpdateUserUserNotFound() {
        UserUpdate update = new UserUpdate("testId", null, "mail@mail.com", null, null, null);
        when(gson.fromJson("valid", UserUpdate.class)).thenReturn(update);
        when(queries.updateUser(update)).thenReturn(false);

        servlet.updateUser("valid");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testUpdateUser() {
        UserUpdate update = new UserUpdate("testId", null, null, "UTC+1 Germany", null, null);
        when(gson.fromJson("valid", UserUpdate.class)).thenReturn(update);
        when(queries.updateUser(update)).thenReturn(true);

        servlet.updateUser("valid");

        verify(builder).create(ServerMessage.USER_UPDATED);
    }

    @Test
    void testGetUserDatabaseError() {
        SecurityContext context = mock(SecurityContext.class);
        when(jwtHelper.getName(context)).thenReturn("user");
        when(queries.getUser("user")).thenReturn(null);

        servlet.getUser("user", context);

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testGetUserNotFound() {
        SecurityContext context = mock(SecurityContext.class);
        when(jwtHelper.getName(context)).thenReturn("user");
        when(queries.getUser("user")).thenReturn(new UserWithId());

        servlet.getUser("user", context);

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testGetUserSameUser() {
        SecurityContext context = mock(SecurityContext.class);
        UserWithId user = new UserWithId(1, "user", "name", "mail@valid.com", "UTC+0 England",
                null, LocalDate.now(), LocalDate.of(1950, 10, 25), UserState.ACTIVE);
        when(jwtHelper.getName(context)).thenReturn("user");
        when(queries.getUser("user")).thenReturn(user);

        servlet.getUser("user", context);

        verify(builder).create(user.prepareForExport());
    }

    @Test
    void testGetUserDifferentUser() {
        SecurityContext context = mock(SecurityContext.class);
        UserWithId user = new UserWithId(1, "user", "name", "mail@valid.com", "UTC+0 England",
                null, LocalDate.now(), LocalDate.of(1950, 10, 25), UserState.ACTIVE);
        UserShare share = new UserShare(false, false, true);
        when(jwtHelper.getName(context)).thenReturn("user2");
        when(queries.getUser("user")).thenReturn(user);
        when(queries.getUserShare("user")).thenReturn(share);

        servlet.getUser("user", context);

        verify(builder).create(user.prepareForExport().resetNotShared(share));
    }

    @Test
    void testDeleteUserUserNotFound() {
        when(queries.deleteUser("user")).thenReturn(false);

        servlet.deleteUser("user");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testDeleteUserDatabaseError() {
        when(queries.deleteUser("user")).thenReturn(null);

        servlet.deleteUser("user");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testDeleteUser() {
        when(queries.deleteUser("user")).thenReturn(true);

        servlet.deleteUser("user");

        verify(builder).create(ServerMessage.USER_DELETED);
    }

    @Test
    void testAddLanguageUserNotFound() {
        when(queries.createLanguage("user", "lang")).thenReturn(1);

        servlet.addLanguage("user", "lang");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testAddLanguageLanguageAlreadyExists() {
        when(queries.createLanguage("user", "lang")).thenReturn(2);

        servlet.addLanguage("user", "lang");

        verify(builder).create(ServerMessage.LANGUAGE_ALREADY_EXISTS);
    }

    @Test
    void testAddLanguageDatabaseError() {
        when(queries.createLanguage("user", "lang")).thenReturn(-1);

        servlet.addLanguage("user", "lang");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testAddLanguage() {
        when(queries.createLanguage("user", "lang")).thenReturn(0);

        servlet.addLanguage("user", "lang");

        verify(builder).create(ServerMessage.LANGUAGE_ADDED);
    }

    @Test
    void testUpdateLanguageJsonInvalid() {
        when(gson.fromJson("invalid", UpdateValue.class)).thenReturn(new UpdateValue(null));

        servlet.updateLanguage("user", "lang", "invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testUpdateLanguageJsonNull() {
        servlet.updateLanguage("user", "lang", "invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testUpdateLanguageDatabaseError() {
        when(gson.fromJson("valid", UpdateValue.class)).thenReturn(new UpdateValue("lang2"));
        when(queries.updateLanguage("user", "lang", "lang2")).thenReturn(null);

        servlet.updateLanguage("user", "lang", "valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testUpdateLanguageUserOrLanguageNotFound() {
        when(gson.fromJson("valid", UpdateValue.class)).thenReturn(new UpdateValue("lang2"));
        when(queries.updateLanguage("user", "lang", "lang2")).thenReturn(false);

        servlet.updateLanguage("user", "lang", "valid");

        verify(builder).create(ServerMessage.USER_OR_LANGUAGE_NOT_FOUND);
    }

    @Test
    void testUpdateLanguage() {
        when(gson.fromJson("valid", UpdateValue.class)).thenReturn(new UpdateValue("lang2"));
        when(queries.updateLanguage("user", "lang", "lang2")).thenReturn(true);

        servlet.updateLanguage("user", "lang", "valid");

        verify(builder).create(ServerMessage.LANGUAGE_UPDATED);
    }

    @Test
    void testDeleteLanguageUserOrLanguageNotFound() {
        when(queries.deleteLanguage("user", "lang")).thenReturn(false);

        servlet.deleteLanguage("user", "lang");

        verify(builder).create(ServerMessage.USER_OR_LANGUAGE_NOT_FOUND);
    }

    @Test
    void testDeleteLanguageDatabaseError() {
        when(queries.deleteLanguage("user", "lang")).thenReturn(null);

        servlet.deleteLanguage("user", "lang");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testDeleteLanguage() {
        when(queries.deleteLanguage("user", "lang")).thenReturn(true);

        servlet.deleteLanguage("user", "lang");

        verify(builder).create(ServerMessage.LANGUAGE_REMOVED);
    }

    @Test
    void testValidateEmailJsonInvalid() {
        when(gson.fromJson("invalid", ValidationHash.class)).thenReturn(new ValidationHash(null));

        servlet.validateEmail("user", "invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testValidateEmailJsonNull() {
        when(gson.fromJson("invalid", ValidationHash.class)).thenReturn(null);

        servlet.validateEmail("user", "invalid");

        verify(builder).create(ServerMessage.REQUEST_BODY_WRONG);
    }

    @Test
    void testValidateEmailDatabaseErrorUserQuery() {
        when(gson.fromJson("valid", ValidationHash.class)).thenReturn(new ValidationHash("hash"));
        when(queries.getUser("user")).thenReturn(null);

        servlet.validateEmail("user", "valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
        verify(queries, times(0)).setVerified(anyInt(), anyBoolean());
    }

    @Test
    void testValidateEmailDatabaseErrorSetVerifiedWithException() {
        when(gson.fromJson("valid", ValidationHash.class)).thenReturn(new ValidationHash("hash"));
        when(queries.getUser("user")).thenReturn(new UserWithId(1, "user", "name",
                "mail@valid.com", null, null, null, null, UserState.ACTIVE));
        when(encryptionHelper.hash("mail@valid.com", "user")).thenReturn("hash");
        when(queries.setVerified(1, true)).thenReturn(null);

        servlet.validateEmail("user", "valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
        verify(queries).setVerified(anyInt(), anyBoolean());
    }

    @Test
    void testValidateEmailDatabaseErrorSetVerifiedOnUpdate() {
        when(gson.fromJson("valid", ValidationHash.class)).thenReturn(new ValidationHash("hash"));
        when(queries.getUser("user")).thenReturn(new UserWithId(1, "user", "name",
                "mail@valid.com", null, null, null, null, UserState.ACTIVE));
        when(encryptionHelper.hash("mail@valid.com", "user")).thenReturn("hash");
        when(queries.setVerified(1, true)).thenReturn(false);

        servlet.validateEmail("user", "valid");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
        verify(queries).setVerified(anyInt(), anyBoolean());
    }

    @Test
    void testValidateEmailUserNotFound() {
        when(gson.fromJson("valid", ValidationHash.class)).thenReturn(new ValidationHash("hash"));
        when(queries.getUser("user")).thenReturn(new UserWithId());

        servlet.validateEmail("user", "valid");

        verify(builder).create(ServerMessage.USER_NOT_FOUND);
    }

    @Test
    void testValidateEmailAuthIncorrect() {
        when(gson.fromJson("valid", ValidationHash.class)).thenReturn(new ValidationHash("hash"));
        when(queries.getUser("user")).thenReturn(new UserWithId(1, "user", "name",
                "mail@valid.com", null, null, null, null, UserState.ACTIVE));
        when(encryptionHelper.hash("mail@valid.com", "user")).thenReturn("hash2");
        when(queries.setVerified(1, true)).thenReturn(null);

        servlet.validateEmail("user", "valid");

        verify(builder).create(ServerMessage.AUTH_INCORRECT);
        verify(queries, times(0)).setVerified(anyInt(), anyBoolean());
    }

    @Test
    void testValidateEmail() {
        when(gson.fromJson("valid", ValidationHash.class)).thenReturn(new ValidationHash("hash"));
        when(queries.getUser("user")).thenReturn(new UserWithId(1, "user", "name",
                "mail@valid.com", null, null, null, null, UserState.ACTIVE));
        when(encryptionHelper.hash("mail@valid.com", "user")).thenReturn("hash");
        when(queries.setVerified(1, true)).thenReturn(true);

        servlet.validateEmail("user", "valid");

        verify(builder).create(ServerMessage.MAIL_VALIDATED);
        verify(queries).setVerified(anyInt(), anyBoolean());
    }

    @Test
    void testUserExistsNotRegistered() {
        when(queries.userExists("user")).thenReturn(false);

        servlet.userExists("user");

        verify(builder).create(ServerMessage.NOT_REGISTERED);
    }

    @Test
    void testUserExistsDatabaseError() {
        when(queries.userExists("user")).thenReturn(null);

        servlet.userExists("user");

        verify(builder).create(ServerMessage.DATABASE_ERROR);
    }

    @Test
    void testUserExists() {
        when(queries.userExists("user")).thenReturn(true);

        servlet.userExists("user");

        verify(builder).create(ServerMessage.REGISTERED);
    }
}

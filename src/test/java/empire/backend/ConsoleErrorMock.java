/*
 * REST-API for Empire User Management
 * Copyright (C) 2019 Michael Schmitz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package empire.backend;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ConsoleErrorMock {
    private final PrintStream systemError = System.err;

    private ByteArrayOutputStream testError;

    public void setupError() {
        testError = new ByteArrayOutputStream();
        System.setErr(new PrintStream(testError));
    }

    public String getError() {
        return testError.toString();
    }

    public void restoreError() {
        System.setErr(systemError);
    }

}

#!/bin/bash

docker run --rm --name mysqlLocal -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=empire -e MYSQL_USER=testUser -e MYSQL_PASSWORD=secretPassword -p 3306:3306 -d mysql

#!/bin/bash

docker run --rm --name dns-proxy --hostname dns.mageddo -p 5380:5380 -v /var/run/docker.sock:/var/run/docker.sock -v /etc/resolv.conf:/etc/resolv.conf -d defreitas/dns-proxy-server &
docker run --rm --name mysql --hostname mysql -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=empire -e MYSQL_USER=testUser -e MYSQL_PASSWORD=secretPassword -d mysql

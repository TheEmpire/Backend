#!/bin/bash

executeSQL() {
  (mysql -u testUser -psecretPassword -h mysql -D empire  < "$1") 2>&1
}

output=$(executeSQL "$1")

while  [[ $output == *"Can't connect to MySQL server on 'mysql'"* ]]
do
  sleep 5
  echo "retrying"
  output=$(executeSQL "$1")
done

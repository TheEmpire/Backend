-- MariaDB 10.17 Distrib 10.4.8-MariaDB, for Linux (x86_64)
--
-- Database: empire
-- ------------------------------------------------------
USE empireStaging;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `Corporation`
--

DROP TABLE IF EXISTS `Corporation`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Corporation`
(
    `id`          int(11)                                 NOT NULL AUTO_INCREMENT,
    `name`        varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
    `description` text COLLATE utf8mb4_unicode_ci         NOT NULL,
    PRIMARY KEY (`id`),
    KEY `NAME` (`name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `InfoType`
--

DROP TABLE IF EXISTS `InfoType`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InfoType`
(
    `id`         int(11)                                NOT NULL AUTO_INCREMENT,
    `name`       varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    `neededRole` int(11)                                NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `InfoType_name_uindex` (`name`),
    KEY `fk_role_type_idx` (`neededRole`),
    CONSTRAINT `fk_role_type` FOREIGN KEY (`neededRole`) REFERENCES `Role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Language`
--

DROP TABLE IF EXISTS `Language`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Language`
(
    `userId`   int(11)                                NOT NULL,
    `language` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    PRIMARY KEY (`userId`, `language`),
    CONSTRAINT `fk_user_language` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Legate`
--

DROP TABLE IF EXISTS `Legate`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Legate`
(
    `id`            int(11)                                NOT NULL AUTO_INCREMENT,
    `userId`        int(11) DEFAULT NULL,
    `imperialId`    varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
    `corporationId` int(11)                                NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_user_legate` (`userId`),
    KEY `fk_corporation_idx` (`corporationId`),
    CONSTRAINT `fk_corporation_legate` FOREIGN KEY (`corporationId`) REFERENCES `Corporation` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
    CONSTRAINT `fk_user_legate` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Rank`
--

DROP TABLE IF EXISTS `Rank`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rank`
(
    `id`   int(11)                                NOT NULL AUTO_INCREMENT,
    `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Creating data for table `Rank`
--

LOCK TABLES `Rank` WRITE;
/*!40000 ALTER TABLE `Rank`
    DISABLE KEYS */;
INSERT INTO `Rank` (`id`, `name`)
VALUES (0, 'Member');
/*!40000 ALTER TABLE `Rank`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RestrictedUserInfo`
--

DROP TABLE IF EXISTS `RestrictedUserInfo`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RestrictedUserInfo`
(
    `id`     int(11) NOT NULL AUTO_INCREMENT,
    `userId` int(11) NOT NULL,
    `type`   int(11) NOT NULL,
    `value`  text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_type_idx` (`type`),
    KEY `fk_user_idx` (`userId`),
    CONSTRAINT `fk_type_info` FOREIGN KEY (`type`) REFERENCES `InfoType` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
    CONSTRAINT `fk_user_info` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role`
(
    `id`   int(11)                                NOT NULL AUTO_INCREMENT,
    `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Creating data for table `Role`
--

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role`
    DISABLE KEYS */;
INSERT INTO `Role` (`id`, `name`)
VALUES (0, 'Global Admin');
/*!40000 ALTER TABLE `Role`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RolesForTable`
--

DROP TABLE IF EXISTS `RolesForTable`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RolesForTable`
(
    `id`         int(11)                                NOT NULL AUTO_INCREMENT,
    `tableName`  varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    `neededRole` int(11)                                NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_role_table_idx` (`neededRole`),
    CONSTRAINT `fk_role_table` FOREIGN KEY (`neededRole`) REFERENCES `Role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Creating data for table `RolesForTable`
--

LOCK TABLES `RolesForTable` WRITE;
/*!40000 ALTER TABLE `RolesForTable`
    DISABLE KEYS */;
INSERT INTO `RolesForTable` (`id`, `tableName`, `neededRole`)
VALUES (0, 'squad', 0),
       (1, 'table', 0),
       (2, 'role', 0),
       (3, 'info', 0),
       (4, 'corp', 0),
       (5, 'user', 0),
       (6, 'rank', 0);
/*!40000 ALTER TABLE `RolesForTable`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Squad`
--

DROP TABLE IF EXISTS `Squad`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Squad`
(
    `id`           int(11)                                NOT NULL AUTO_INCREMENT,
    `name`         varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
    `maxMembers`   int(11)                                NOT NULL,
    `leadingSquad` int(11) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_leader_idx` (`leadingSquad`),
    CONSTRAINT `fk_leader_squad` FOREIGN KEY (`leadingSquad`) REFERENCES `Squad` (`id`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User`
(
    `id`            int(11)                                             NOT NULL AUTO_INCREMENT,
    `imperialId`    varchar(50) UNIQUE                                  NOT NULL,
    `mail`          varchar(50)                                         NOT NULL,
    `state`         enum ('ACTIVE','INACTIVE','TERMINATED','SUSPENDED') NOT NULL DEFAULT 'ACTIVE',
    `timezone`      varchar(30)                                         NOT NULL,
    `dateOfAccess`  date                                                NOT NULL DEFAULT (CURRENT_DATE),
    `dateOfBirth`   date                                                         DEFAULT NULL,
    `name`          varchar(50)                                                  DEFAULT NULL,
    `corporationId` int(11)                                                      DEFAULT NULL,
    `rankId`        int(11)                                             NOT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_corporation_idx` (`corporationId`),
    KEY `imperialId_idx` (`imperialId`),
    KEY `fk_rank_idx` (`rankId`),
    CONSTRAINT `fk_corporation_user` FOREIGN KEY (`corporationId`) REFERENCES `Corporation` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `fk_rank_user` FOREIGN KEY (`rankId`) REFERENCES `Rank` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserAuth`
--

DROP TABLE IF EXISTS `UserAuth`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserAuth`
(
    `id`       int(11)      NOT NULL,
    `password` varchar(512) NOT NULL,
    `salt`     varchar(256) NOT NULL,
    `verified` tinyint(4)   NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_imperialId_userauth` FOREIGN KEY (`id`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserInSquad`
--

DROP TABLE IF EXISTS `UserInSquad`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInSquad`
(
    `userId`          int(11) NOT NULL,
    `squadId`         int(11) NOT NULL,
    `positionInSquad` int(11) NOT NULL,
    PRIMARY KEY (`userId`, `squadId`),
    KEY `fk_squad_idx` (`squadId`),
    KEY `fk_user_idx` (`userId`),
    CONSTRAINT `fk_squad_for_user` FOREIGN KEY (`squadId`) REFERENCES `Squad` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
    CONSTRAINT `fk_user_in_squad` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserShare`
--

DROP TABLE IF EXISTS `UserShare`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UserShare`
(
    `id`          int(11)    NOT NULL,
    `name`        tinyint(1) NOT NULL DEFAULT '0',
    `mail`        tinyint(1) NOT NULL DEFAULT '0',
    `dateOfBirth` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    CONSTRAINT `UserShare_User_id_fk` FOREIGN KEY (`id`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserWithRole`
--

DROP TABLE IF EXISTS `UserWithRole`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserWithRole`
(
    `userId` int(11)    NOT NULL,
    `roleId` int(11)    NOT NULL,
    `read`   tinyint(4) NOT NULL,
    `write`  tinyint(4) NOT NULL,
    `create` tinyint(4) NOT NULL,
    `delete` tinyint(4) NOT NULL,
    PRIMARY KEY (`userId`, `roleId`),
    KEY `fk_role_idx` (`roleId`),
    KEY `fk_user_idx` (`userId`),
    CONSTRAINT `fk_role_for_user` FOREIGN KEY (`roleId`) REFERENCES `Role` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
    CONSTRAINT `fk_user_has_role` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

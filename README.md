# Empire Backend
This Java Backend utilizes JNDIs for Database configuration and some other parameters, see src/main/webapp/WEB-INF/web.xml

![License](https://img.shields.io/badge/License-GNU%20GPL%20v3-Blue)

| master (Production) | dev (Staging) |
| --- | --- |
| ![Project Version](https://img.shields.io/badge/version-0.1-Brightgreen) | ![Project Version](https://img.shields.io/badge/version-0.1-Brightgreen) | 
| [![pipeline status master](https://img.shields.io/gitlab/pipeline/TheEmpire/Backend?label=build%20master)](https://gitlab.com/TheEmpire/Backend/commits/master) | [![pipeline status](https://img.shields.io/gitlab/pipeline/TheEmpire/Backend/dev?label=build%20dev)](https://gitlab.com/TheEmpire/Backend/commits/dev)
| [![Online State master](https://img.shields.io/website?down_color=red&down_message=offline&up_color=green&label=backend%20production&up_message=online&url=https%3A%2F%2Frest.cloud-in.space%2Fempire)](https://rest.cloud-in.space/empire) |  ![Online state dev](https://img.shields.io/website?down_color=red&down_message=offline&up_color=green&label=backend%20staging&up_message=online&url=https%3A%2F%2Frest.cloud-in.space%2FempireStaging)
| [![Coverage master](https://img.shields.io/sonar/coverage/empireBackend?logo=sonar&server=https%3A%2F%2Fsonar.cloud-in.space&sonarVersion=7.9)](https://sonar.cloud-in.space/dashboard?id=empireBackend) | [![Coverage dev](https://img.shields.io/sonar/coverage/empireStagingBackend?server=https%3A%2F%2Fsonar.cloud-in.space)](https://sonar.cloud-in.space/dashboard?id=empireStagingBackend) 
| [![Tech Debt master](https://img.shields.io/sonar/tech_debt/empireBackend?server=https%3A%2F%2Fsonar.cloud-in.space)](https://sonar.cloud-in.space/dashboard?id=empireBackend) | [![Tech Debt dev](https://img.shields.io/sonar/tech_debt/empireStagingBackend?server=https%3A%2F%2Fsonar.cloud-in.space)](https://sonar.cloud-in.space/dashboard?id=empireStagingBackend) 
| [![Code Violations master](https://img.shields.io/sonar/violations/empireBackend?server=https%3A%2F%2Fsonar.cloud-in.space&format=long)](https://sonar.cloud-in.space/dashboard?id=empireBackend)  | [![Code Violations dev](https://img.shields.io/sonar/violations/empireStagingBackend?server=https%3A%2F%2Fsonar.cloud-in.space&format=long)](https://sonar.cloud-in.space/dashboard?id=empireStagingBackend) 
| [![Test Results master](https://img.shields.io/sonar/tests/empireBackend?compact_message&server=https%3A%2F%2Fsonar.cloud-in.space)](https://sonar.cloud-in.space/dashboard?id=empireBackend) | [![Test Results dev](https://img.shields.io/sonar/tests/empireStagingBackend?compact_message&server=https%3A%2F%2Fsonar.cloud-in.space)](https://sonar.cloud-in.space/dashboard?id=empireStagingBackend)
| [![Quality Gate Status master](https://sonar.cloud-in.space/api/project_badges/measure?project=empireBackend&metric=alert_status)](https://sonar.cloud-in.space/dashboard?id=empireBackend) | [![Quality Gate Status dev](https://sonar.cloud-in.space/api/project_badges/measure?project=empireStagingBackend&metric=alert_status)](https://sonar.cloud-in.space/dashboard?id=empireStagingBackend)
| [![Swagger master](https://img.shields.io/swagger/valid/2.0/https/rest.cloud-in.space/swaggerDefinitions/empire.yaml?label=swagger%20master)](https://swagger.cloud-in.space/?urls.primaryName=Empire%20API) | [![Swagger dev](https://img.shields.io/swagger/valid/2.0/https/rest.cloud-in.space/swaggerDefinitions/empireStaging.yaml?label=swagger%20dev)](https://swagger.cloud-in.space/?urls.primaryName=Empire%20API%20Staging)

## Development Requirements

- Wildfly 18
- Docker for database instance
- JDBC driver for SQL

## Config

### Wildlfy
- Run the server
- Open Managment Console
- Deploy war file on path /empire

### Database
- start the docker service
- run ```ci/start_local_docker.sh```
- run ```ci/fill_database.sh ci/testDatabase.sql```
- to stop run: ```ci/stop_local_docker.sh```
